package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

/**
 * Created by FOLASHELE on 8/11/2015.
 */
public class CashOut_Activity extends BaseActivity {

  private EditText mAmount, mPaycode, mPin;
  private String Amount, Paycode, Pin, pidoMobile;
  private Button cashOut_btn;
  private static final String Tag = "cashout";
  private String cashout_charge_uri;
  private String cashout_uri;
  private Cashout_Charge_Task _task;
  private Cashout_Task _Task;
  private ConnectionChecker connCheck = new ConnectionChecker(CashOut_Activity.this);

  private void Cashout_Confirmation_dialog(String paycode, String charge, String amount) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.cashout_confirmation, null);

    String cashout_charge = charge.trim(); // strips off all unwanted spaces or quotations

    TextView mPaycode = (TextView) v.findViewById(R.id.paycode_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.cashout_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.cashout_charge_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.cashout_total_txt);

    mPaycode.setText(paycode);
    mAmount_txt_view.setText("₦" + amount);
    mTrans_charge.setText("₦" + cashout_charge);

    int mTotal = Integer.parseInt(cashout_charge) + Integer.parseInt(amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(CashOut_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _Task = new Cashout_Task();
          _Task.execute(cashout_uri);
        } else {
          Toast.makeText(CashOut_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.cash_out);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(CashOut_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mAmount = (EditText) findViewById(R.id.cash_amount);
    mPaycode = (EditText) findViewById(R.id.cashout_paycode);
    mPin = (EditText) findViewById(R.id.cashout_pin);

    cashOut_btn = (Button) findViewById(R.id.cashout_btn);
    cashOut_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Amount = mAmount.getText().toString();
        Paycode = mPaycode.getText().toString();
        Pin = mPin.getText().toString();

        if (Amount.length() == 0 && Paycode.length() == 0 && Pin.length() == 0) {
          mAmount.setError("This Field Is Required");
          mPaycode.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (mAmount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Paycode.length() == 0) {
          mPaycode.setError("This Field Is Required");
        } else if (Paycode.length() < 4) {
          mPaycode.setError("4 Digit Number Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digits Number Required");

        } else {

          cashout_charge_uri = "method=cashoutcharge&amount=" + Amount + "&mobile=" + pidoMobile;

          Log.d(Tag, "cashout charge uri is: " + cashout_charge_uri);
          if (connCheck.isAvailable()) {

            _task = new Cashout_Charge_Task();
            _task.execute(cashout_charge_uri);
          } else {
            Toast
                .makeText(CashOut_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
                .show();
          }

        }

      }
    });

  }

  private class Cashout_Charge_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(CashOut_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(CashOut_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Not available";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      Pd.dismissProgress();

      String charge_result = s;

      if (s != "Not available") {
        Encryption encrypt = new Encryption(); // Base64 encoding instance

        cashout_uri =
            "method=cashout&amount=" + Amount + "&mobile=" + encrypt.Base64Encoder(pidoMobile) +
                "&paycode=" + encrypt.Base64Encoder(Paycode) +
                "&pin=" + encrypt.Base64Encoder(Pin) + "&charge=" + charge_result;
        Cashout_Confirmation_dialog(Paycode, charge_result, Amount);
      } else {
        charge_result = "Not available, Please try again later";
        new Dialog(CashOut_Activity.this, charge_result);
        mPaycode.setText("");
        mPin.setText("");
        mAmount.setText("");
      }

    }

  }

  private class Cashout_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(CashOut_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(CashOut_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(CashOut_Activity.this, result); // insert response into dialog
      mPaycode.setText("");
      mPin.setText("");
      mAmount.setText("");
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }
}

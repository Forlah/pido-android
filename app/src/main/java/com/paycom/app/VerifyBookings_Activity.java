package com.paycom.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.Toast;
import com.paycom.adapters.VerifyBookings_RecyclerAdapter;
import com.paycom.model.Booking;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService_TMAS;
import com.paycom.utitlities.Constants;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import org.json.JSONArray;

public class VerifyBookings_Activity extends BaseActivity {

  private EditText mPlateno;
  private Button VerifyBtn;
  private String PlateNo;
  private RecyclerView mRecyclerView;
  private RelativeLayout detailsRelative;
  private ConnectionChecker connChecker = new ConnectionChecker(VerifyBookings_Activity.this);
  private String safeURl;
  LinearLayoutManager mLayoutManager = null;

  VerifyBookings_RecyclerAdapter adapter;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_bookings_);

    mPlateno = (EditText) findViewById(R.id.verify_booking_ref_no);
    detailsRelative = (RelativeLayout) findViewById(R.id.rel_1);
    VerifyBtn = (Button) findViewById(R.id.verify_bookings_btn);

    mRecyclerView = (RecyclerView) findViewById(R.id.bookings_list);

    mRecyclerView.setHasFixedSize(true);

    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(VerifyBookings_Activity.this);
    mRecyclerView.setLayoutManager(mLayoutManager);

    VerifyBtn.setEnabled(false);
    VerifyBtn.setAlpha(0.5f);

    mPlateno.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {

        if (mPlateno.getText().toString().trim().equals("") || mPlateno.getText().length() < 0) {
          VerifyBtn.setEnabled(false);
          VerifyBtn.setAlpha(0.5f);

        } else {
          VerifyBtn.setEnabled(true);
          VerifyBtn.setAlpha(1f);
        }

      }
    });

    VerifyBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        PlateNo = mPlateno.getText().toString();

        if (PlateNo.length() < 9) {
          mPlateno.setError("Invalid Vehicle Plate Number");
        } else {
          try {

            safeURl = Constants.verifyBookingURL + "plateNumber=" + URLEncoder
                .encode(PlateNo.toUpperCase(), "UTF-8");
            if (connChecker.isAvailable()) {

              new BookingVerificationTask().execute();
            } else {

              Toast.makeText(VerifyBookings_Activity.this, "No Connection To The Internet",
                  Toast.LENGTH_LONG).show();
            }

          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
        }
      }
    });

  }

  private class BookingVerificationTask extends AsyncTask<Void, Void, JSONArray> {

    Progressdialog progressdialog;
    String response;
    JSONArray jsonArray;

    ArrayList<Booking> bookingArrayList = new ArrayList<>();

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      progressdialog = new Progressdialog(VerifyBookings_Activity.this, "Please Wait..");
      progressdialog.showProgress();
    }

    @Override
    protected JSONArray doInBackground(Void... params) {

      try {

        jsonArray = new ConnectionToService_TMAS().getJSONfromUrl(safeURl);

      } catch (Exception e) {
        e.printStackTrace();
        response = "Unable to connect to the service at the moment.Please try again later";
      }

      return jsonArray;

    }

    @Override
    protected void onPostExecute(JSONArray jsonArray) {
      super.onPostExecute(jsonArray);

      progressdialog.dismissProgress();

      try {

        for (int i = 0; i < jsonArray.length(); i++) {

          Booking obj = new Booking();

          String date = jsonArray.getJSONObject(i).get("date").toString();
          String name = jsonArray.getJSONObject(i).get("offName").toString();
          String state = jsonArray.getJSONObject(i).get("state").toString();
          String status = jsonArray.getJSONObject(i).get("status").toString();

          obj.setDate(date);
          obj.setName(name);
          obj.setState(state);
          obj.setStatus(status);

          bookingArrayList.add(obj);
        }

        adapter = new VerifyBookings_RecyclerAdapter(VerifyBookings_Activity.this,
            bookingArrayList);
        mRecyclerView.setAdapter(adapter);

      } catch (Exception e) {
        new Dialog(VerifyBookings_Activity.this,
            "Bookings Not Available"); // insert response into dialog
      }
    }
  }
}

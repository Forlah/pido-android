package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class Bet9ja_Activity extends BaseActivity {

  private EditText mCustomerId, mAmount, mPin;
  private String CustomerId, Amount, User_type, pidoMobile, Pin;
  private Button Bet9ja_btn;
  private Spinner mUser_type;
  private ConnectionChecker connChecker = new ConnectionChecker(Bet9ja_Activity.this);
  private get_CustomerName_Task _task;
  private Bet9ja_Task _Task;
  private String bet9ja_uri;

  private void Bet9ja_Confirmation_dialog() {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.bet9ja_confirmation, null);

    // TextView mCustomerName =  (TextView) v.findViewById(R.id.bet9ja_cus_name_txt);
    TextView mCustomerId = (TextView) v.findViewById(R.id.betja_cus_id_txt);
    TextView mUser = (TextView) v.findViewById(R.id.bet9ja_user_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.bet9ja_amount_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.bet9ja_total_txt);

    //mCustomerName.setText(name);
    mCustomerId.setText(CustomerId);
    mUser.setText(User_type);
    mAmount_txt_view.setText("₦" + Amount);

    mtotal.setText("₦" + Amount);

//        final String bet9ja_uri = "method=betnaijafundwallet&amount="+Amount+"&apipass=mypassword&apiuser=abeeb_test&customername="+name
//                +"&customerid="+CustomerId+"&customermobile="+pidoMobile
//                +"&merchant="+CustomerId+"&merchantpaycommobile="+pidoMobile
//                +"&productid=29&pin="+Pin+"&typeofoperation="+User_type;

    Encryption encrypt = new Encryption();

    bet9ja_uri = "method=betnaijafundwallet&amount=" + Amount + "&customerid=" + CustomerId
        + "&customermobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin) + "&typeofoperation=" + User_type;

    AlertDialog.Builder builder = new AlertDialog.Builder(Bet9ja_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (connChecker.isAvailable()) {
          _Task = new Bet9ja_Task();
          _Task.execute(bet9ja_uri);
        } else {
          Toast.makeText(Bet9ja_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_bet9ja);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Bet9ja_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.bet9ja_cus_id);
    mPin = (EditText) findViewById(R.id.bet9ja_pin);
    mAmount = (EditText) findViewById(R.id.bet9ja_amount);

    mUser_type = (Spinner) findViewById(R.id.bet9ja_spinner);
    mUser_type.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        User_type = parent.getSelectedItem().toString();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    Bet9ja_btn = (Button) findViewById(R.id.btn_bet9ja);
    Bet9ja_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomerId = mCustomerId.getText().toString();
        Amount = mAmount.getText().toString();
        Pin = mPin.getText().toString();

        if (CustomerId.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError(" 4 Digit Number Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else {
          Bet9ja_Confirmation_dialog();
        }
      }
    });

  }

  private class get_CustomerName_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(Bet9ja_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Bet9ja_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Not available";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      Pd.dismissProgress();

      // Bet9ja_Confirmation_dialog(s);

    }

  }

  private class Bet9ja_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Bet9ja_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output;
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Bet9ja_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(Bet9ja_Activity.this, result); // insert response into dialog
      mCustomerId.setText("");
      mAmount.setText("");
      mPin.setText("");
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

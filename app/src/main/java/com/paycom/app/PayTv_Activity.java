package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by FOLASHELE on 8/14/2015.
 */
public class PayTv_Activity extends BaseActivity {

  private String actions[] = {"DSTV", "GOTV", "STARTIMES"};

  RecyclerView mPaytv;
  PayTV_RecyclerAdapter mPayTV_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.cable_tv);

    mPayTV_Adapter = new PayTV_RecyclerAdapter(getApplicationContext(), actions);
    mPaytv = (RecyclerView) findViewById(R.id.cable_list);
    mPaytv.setHasFixedSize(true);
    mPaytv.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    mPaytv.setLayoutManager(mLayoutManager);
    mPaytv.setAdapter(mPayTV_Adapter);
  }

  private class PayTV_RecyclerAdapter extends
      RecyclerView.Adapter<PayTV_RecyclerAdapter.DataObjectHolder> {

    String[] types;
    Context mContext;

    private PayTV_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      types = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView paytv_name;
      ImageView paytv_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        paytv_name = (TextView) itemView.findViewById(R.id.cable_type);
        paytv_Image = (ImageView) itemView.findViewById(R.id.cable_tv_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.cable_tv_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
      final int _position = position;
      holder.paytv_name.setText(types[position]);

      if (types[position].equals("DSTV")) {

        holder.paytv_Image.setImageResource(R.mipmap.dstvlogo);
      } else if (types[position].equals("GOTV")) {
        holder.paytv_Image.setImageResource(R.mipmap.gotv);

      } else {
        holder.paytv_Image.setImageResource(R.mipmap.startimes);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if (types[_position].equals("DSTV")) {

            Intent i = new Intent(PayTv_Activity.this, Dstv_Activity.class);
            startActivity(i);
          } else if (types[_position].equals("GOTV")) {
            Intent i = new Intent(PayTv_Activity.this, Gotv_Activity.class);
            startActivity(i);
          } else if (types[_position].equals("STARTIMES")) {

            Intent i = new Intent(PayTv_Activity.this, StarTimes_Activity.class);
            startActivity(i);
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return types.length;
    }

  }

}


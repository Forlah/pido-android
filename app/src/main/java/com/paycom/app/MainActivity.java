package com.paycom.app;

import android.content.Intent;
import android.os.Bundle;
import com.paycom.DBhelper.Reg_DatabaseHandler;

/**
 * Created by sp_developer on 11/28/15.
 */
public class MainActivity extends BaseActivity {

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(MainActivity.this);

    if (rdb.DbExists()) {
      Intent i = new Intent(MainActivity.this, Login_Activity.class);
      startActivity(i);
      finish();
    } else {
      Intent i = new Intent(MainActivity.this, Registration.class);
      startActivity(i);
      finish();
    }
  }

}

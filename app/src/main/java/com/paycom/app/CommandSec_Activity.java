package com.paycom.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.Sch_Fees;

public class CommandSec_Activity extends BaseActivity {

  private EditText mPin;
  private String Pin, pidoMobile;
  private Button CommandSecSch_Btn;
  private ConnectionChecker connChecker = new ConnectionChecker(CommandSec_Activity.this);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_command);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        CommandSec_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mPin = (EditText) findViewById(R.id.comand_pin);

    CommandSecSch_Btn = (Button) findViewById(R.id.btn_command);
    CommandSecSch_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Pin = mPin.getText().toString();
        if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {

          String uri =
              "method=schoolformpayment&commandtype=" + "otherschoool" + "&productid=" + "72"
                  + "&schoolid=" + "1" +
                  "&customerno=" + pidoMobile + "&mobileno=" + pidoMobile + "&pin=" + Pin;

          if (connChecker.isAvailable()) {

            new Sch_Fees(CommandSec_Activity.this, uri);
          } else {
            Toast.makeText(CommandSec_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
          mPin.setText("");

        }
      }
    });
  }

}

package com.paycom.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.model.Data;
import com.paycom.model.DataLab;
import com.paycom.model.User;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class Token_Activity extends AppCompatActivity {

  private EditText mToken, mPassword;
  private String token_txt, password_txt;
  private Button token_btn;
  private String Password, Mobile_no, IMEI, firstname, Surname, Email, Gender;
  private static final String App_Version = "3.0";
  private TokenTask _task;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_token_);

    Data c = DataLab.get(Token_Activity.this).getDetails();
    Surname = c.getSurname();
    firstname = c.getFirstname();
    Password = c.getPassword();
    Mobile_no = c.getMobile_no();
    IMEI = c.getImei();
    Email = c.getEmail();
    Gender = c.getGender();

    mToken = (EditText) findViewById(R.id.token_no);
    mPassword = (EditText) findViewById(R.id.token_pass_txt);

    token_btn = (Button) findViewById(R.id.token_continue_btn);
    token_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        token_txt = mToken.getText().toString();
        password_txt = mPassword.getText().toString();

        if (token_txt.length() == 0 && password_txt.length() == 0) {
          mToken.setError("This Field Is Required");
          mPassword.setError("This Field Is Required");
        } else if (token_txt.length() == 0) {
          mToken.setError("Enter the token sent via text ");
        } else if (password_txt.length() == 0) {
          mPassword.setError("This Field Is Required");
        } else if (password_txt.equals(Password) == false) {
          mPassword.setError("Does not match your initial password");
        } else {
          Encryption encrypt = new Encryption();

          String token_login_uri = "method=login&msisdn=" + encrypt.Base64Encoder(Mobile_no)
              + "&password=" + password_txt
              + "&autcode=" + encrypt.Base64Encoder(IMEI)
              + "&device=" + "android"
              + "&token=" + token_txt
              + "&version=" + App_Version;

          ConnectionChecker isConnected = new ConnectionChecker(Token_Activity.this);
          if (isConnected.isAvailable()) {

            _task = new TokenTask();
            _task.execute(token_login_uri);
            //  new TokenTask().execute(token_login_uri);
          } else {
            Toast
                .makeText(Token_Activity.this, "No Connection To The Internet..", Toast.LENGTH_LONG)
                .show();
          }
        }

      }
    });
  }

  private class TokenTask extends AsyncTask<String, Void, String> {

    Progressdialog Pd;
    String output;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Token_Activity.this, "Please wait a moment");
      Pd.showProgress_Title();
      // Pd = new Progressdialog();
      //Pd.set_showProgreses(Login_Activity.this,"Requesting","Please Wait..");
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Token_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.startsWith("8001")) {

        // successful

        User _user = new User(firstname, Surname, Mobile_no, Password, IMEI, token_txt, Email,
            Gender);

        Reg_DatabaseHandler db = new Reg_DatabaseHandler(Token_Activity.this);
        db.addUser(_user);

        Pd.dismissProgress(); // dismiss progress dialog

        Intent i = new Intent(Token_Activity.this, Login_Activity.class);
        startActivity(i);
        Toast.makeText(Token_Activity.this, "Welcome", Toast.LENGTH_LONG).show();

        finish(); // kill this activity

      } else if (result.startsWith("8000")) {
        Pd.dismissProgress();
        new Dialog(Token_Activity.this, "Error Occurred, Please Try again");
      } else {

        Pd.dismissProgress();
        new Dialog(Token_Activity.this, result);

      }
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

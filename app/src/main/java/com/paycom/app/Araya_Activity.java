package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class Araya_Activity extends BaseActivity {

  private EditText mEnrolleeId, mPin;
  private String EnrolleeId, Pin, pidoMobile;
  private Button ARAYA_Btn;
  private ConnectionChecker connChecker = new ConnectionChecker(Araya_Activity.this);
  private Verify_Araya_Task _task;
  private Araya_Task _Task;
  private String name, araya_amount, plan, trans_charge;

  private void Araya_Confirmation_Dialog(String name, String plan, String amount,
      String trans_charge) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.araya_confirmation, null);

    TextView mEnrollee_name = (TextView) v.findViewById(R.id.enrolle_name_txt);
    TextView mEnrolleId = (TextView) v.findViewById(R.id.enrolle_id_txt);
    TextView mAraya_plan = (TextView) v.findViewById(R.id.araya_plan_txt);
    TextView mTrans = (TextView) v.findViewById(R.id.araya_charge_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.araya_amount_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.araya_total_txt);

    mEnrollee_name.setText(name);
    mEnrolleId.setText(EnrolleeId);
    mAraya_plan.setText(plan);
    mTrans.setText("₦" + trans_charge);
    mAmount_txt_view.setText("₦" + amount);

    int total = Integer.parseInt(amount.trim()) + Integer.parseInt(trans_charge.trim());
    mtotal.setText("₦" + total);

    final String araya_uri = "method=arayapayment&enrolleeid=" + EnrolleeId
        + "&mobile=" + pidoMobile
        + "&amount=" + amount + "&pin=" + Pin + "&productid=38";

    AlertDialog.Builder builder = new AlertDialog.Builder(Araya_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connChecker.isAvailable()) {

          _Task = new Araya_Task();
          _Task.execute(araya_uri);
        } else {
          Toast.makeText(Araya_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_araya);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Araya_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mEnrolleeId = (EditText) findViewById(R.id.araya_enrollee_id);
    mPin = (EditText) findViewById(R.id.araya_pin);
    ARAYA_Btn = (Button) findViewById(R.id.btn_araya);

    ARAYA_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        EnrolleeId = mEnrolleeId.getText().toString();
        Pin = mPin.getText().toString();

        if (mEnrolleeId.length() == 0 && Pin.length() == 0) {
          mEnrolleeId.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (mEnrolleeId.length() == 0) {
          mEnrolleeId.setError("This Field Is Required");
        } else if (mEnrolleeId.length() < 14) {
          mEnrolleeId.setError("Incomplete,Please check your ID");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          String araya_enquiry_uri =
              "method=arayaenquiry&enrolleeid=" + EnrolleeId + "&mobile=" + pidoMobile;

          if (connChecker.isAvailable()) {

            _task = new Verify_Araya_Task();
            _task.execute(araya_enquiry_uri);
          } else {
            Toast.makeText(Araya_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
                .show();
          }
        }
      }

    });

  }

  private class Verify_Araya_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(Araya_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Araya_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      // String name,araya_amount,plan,trans_charge;
      // Log.d(Tag, "output string from server = " + s);
      if (s.contains("|")) {
        try {
          Pd.dismissProgress();
          // if the result contain "|" sign means valid response
          StringTokenizer tokens = new StringTokenizer(s, "|");
          name = tokens.nextToken();
          araya_amount = tokens.nextToken();
          plan = tokens.nextToken();
          trans_charge = tokens.nextToken();

          if (name.startsWith("200")) {

            name = "Unavailable at the moment";
          } else if (araya_amount.equals("200")) {
            araya_amount = "Unavailable at the moment";
          } else if (plan.equals("200")) {
            plan = "Unavailable at the moment";
          } else if (trans_charge.equals("200") || trans_charge.equals("400")) {
            trans_charge = "Unavailable at the moment";
          } else {
            // display a dialog for the user to verify the acc
            Araya_Confirmation_Dialog(name, plan, araya_amount, trans_charge);
          }

        } catch (NoSuchElementException NE) {
          Pd.dismissProgress();
          new Dialog().error_Dialog(Araya_Activity.this, "Please Check Your Id Again", "FAILED");

          mEnrolleeId.setText("");
          mPin.setText("");

        }

      } else {
        // an error occured on the service
        Pd.dismissProgress();

        new Dialog(Araya_Activity.this, s); // insert response into dialog
      }
    }
  }

  private class Araya_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Araya_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Araya_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(Araya_Activity.this, result); // insert response into dialog

      mEnrolleeId.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

package com.paycom.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

/**
 * Created by FOLASHELE on 8/13/2015.
 */
public class ChangePin_Activity extends BaseActivity {

  private EditText mOldpin, mNewPin, mConfirmPin;
  private String OldPin, NewPin, ConfirmPin, pidoMobile;
  private Button ChangePin_btn;
  ChangePin_Task _task;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        ChangePin_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    setContentView(R.layout.change_pin);
    mOldpin = (EditText) findViewById(R.id.old_pin);
    mNewPin = (EditText) findViewById(R.id.new_pin);
    mConfirmPin = (EditText) findViewById(R.id.confirm_pin);

    ChangePin_btn = (Button) findViewById(R.id.btn_changePin);
    ChangePin_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        OldPin = mOldpin.getText().toString();
        NewPin = mNewPin.getText().toString();
        ConfirmPin = mConfirmPin.getText().toString();

        if (OldPin.length() == 0 && NewPin.length() == 0 && ConfirmPin.length() == 0) {
          mOldpin.setError("This Field Is Required");
          mNewPin.setError("This Field Is Required");
          mConfirmPin.setError("This Field Is Required");
        } else if (OldPin.length() == 0) {
          mOldpin.setError("This Field Is Required");
        } else if (OldPin.length() < 4) {
          mOldpin.setError("4 Digit Number Required");
        } else if (NewPin.length() == 0) {
          mNewPin.setError("This Field Is Required");
        } else if (NewPin.length() < 4) {
          mNewPin.setError("4 Digit Number Required");
        } else if (OldPin.equals(NewPin)) {
          mNewPin.setError("New PIN must be different from old PIN");
        } else if (ConfirmPin.length() == 0) {
          mConfirmPin.setError("This Field Is Required");
        } else if (ConfirmPin.equals(NewPin) == false) {
          mConfirmPin.setError("Passwords Do Not Match");
        } else {
          Encryption encrypt = new Encryption(); // encryption instance

          String change_pin_uri = "method=changepin&mobileno=" + encrypt.Base64Encoder(pidoMobile) +
              "&oldpin=" + encrypt.Base64Encoder(OldPin) +
              "&newpin=" + encrypt.Base64Encoder(NewPin) +
              "&confirmpin=" + encrypt.Base64Encoder(ConfirmPin);

          _task = new ChangePin_Task();
          _task.execute(change_pin_uri);
        }
      }
    });
  }

  private class ChangePin_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(ChangePin_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(ChangePin_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog
      if (result.startsWith("8010")) {
        new Dialog(ChangePin_Activity.this,
            "Sorry The PIN You Used is weak, Please try again"); // insert response into dialog

      }

      new Dialog(ChangePin_Activity.this, result.trim()); // insert response into dialog
      mOldpin.setText("");
      mNewPin.setText("");
      mConfirmPin.setText("");
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }
}

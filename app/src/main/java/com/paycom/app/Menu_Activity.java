package com.paycom.app;

import android.content.Context;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.adapters.MyRecyclerViewAdapter;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Menu_Activity extends BaseActivity {

  private RecyclerView mRecyclerView;
  MyRecyclerViewAdapter mMenu_Adapter;
  LinearLayoutManager mLayoutManager = null;
  Context context = this;
  private TextView mBalance, mBalText;
  private String uri, pidoMobile;
  private static final String Tag = "menus";
  private GetBalanceTask _task;
  private ConnectionChecker connCheck = new ConnectionChecker(Menu_Activity.this);
  private SwipeRefreshLayout swipeRefreshLayout;

  String[] listItems = {"Airtime Topup", "Transfer Money", "Cash Out", "Fund PiDO Wallet",
      "Pay Bills", "Change Pin", "PiDO Contact", "Transaction History"};
  int[] images = {R.mipmap.airtime_topup, R.mipmap.send_money, R.mipmap.cash_out,
      R.mipmap.fund_wallet, R.mipmap.pay_bills, R.mipmap.change_pin, R.mipmap.pido_contact,
      R.mipmap.transaction_history};

  //    String[] listItems = {"Airtime Topup","Transfer Money","Cash Out","Cardless Withdrawal","Fund PiDO Wallet","Pay Bills","Change Pin","PiDO Contact","Transaction History"};
//    int [] images = {R.mipmap.airtime_topup, R.mipmap.send_money, R.mipmap.cash_out, R.mipmap.internetpayment, R.mipmap.araya,R.mipmap.pay_bills, R.mipmap.change_pin,R.mipmap.pido_contact,R.mipmap.transaction_history};
  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Menu_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    setContentView(R.layout.main_menu);

    mMenu_Adapter = new MyRecyclerViewAdapter(listItems, context, images);

    uri = "method=balance2&mobile=" + pidoMobile; // account balance

    new GetBalanceTask().execute(uri); // async task to retrieve acc balance from service

    mRecyclerView = (RecyclerView) findViewById(R.id.menu_list);

    mRecyclerView.setHasFixedSize(true);

    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(context);
    // mRecyclerView.setLayoutManager(mLayoutManager);
    mRecyclerView.setLayoutManager(mLayoutManager);
    mRecyclerView.setAdapter(mMenu_Adapter);

    mBalance = (TextView) findViewById(R.id.acc_bal);

    mBalText = (TextView) findViewById(R.id.acc_bal);
    mBalText.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        if (connCheck.isAvailable()) {

          _task = new GetBalanceTask();
          _task.execute(uri);
        } else {

          Toast.makeText(Menu_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }

      }
    });

    swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipeRefresh);
    //swipeRefreshLayout.setColorSchemeColors(R.color.DarkGreen, R.color.Green, R.color.DarkGreen);
    swipeRefreshLayout.setColorScheme(R.color.DarkGreen);
    swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
      @Override
      public void onRefresh() {

        if (connCheck.isAvailable()) {

          _task = new GetBalanceTask();
          mBalance.setText("Please Wait...");
          _task.execute(uri);
        } else {

          Toast.makeText(Menu_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();

        }
      }
    });

  }

  // Asysnc task to get the balance from the connetionToService class and updates the acc bal text ui

  private class GetBalanceTask extends AsyncTask<String, Void, String> {

    String output;

    @Override
    protected String doInBackground(String... params) {
      // String ans =  new ConnectionToService(uri).ServiceResponse();
      try {
        output = new ConnectionToService(params[0]).download_secure_URL(Menu_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "";
      }

    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      if (s.startsWith("No response from the server") || s.startsWith(" ")) {
        mBalance.setText("Not available");
      } else {

        try {
          BigDecimal bd = new BigDecimal(s.trim());
          mBalance.setText(NumberFormat.getCurrencyInstance(new Locale("en", "NG")).format(bd));

        } catch (Exception e) {
          mBalance.setText("Not available");
        }

      }

      swipeRefreshLayout.setRefreshing(false);  // dismiss refreshing layout

    }
  }

  @Override
  public void onBackPressed() {
    AlertDialog.Builder builder = new AlertDialog.Builder(Menu_Activity.this);
    builder.setIcon(R.mipmap.logout);
    builder.setCancelable(false);
    builder.setTitle("Exit PiDO ?");
    builder.setNegativeButton("No ", null);
    builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        Menu_Activity.this.finish();
      }
    });
    builder.show();
    //super.onBackPressed();

  }

  @Override
  protected void onPause() {
        /*Thread timer = new Thread(){
            public void run(){
                try {
                    sleep(180000);

                } catch (InterruptedException e){
                    e.printStackTrace();

                } finally{

                    finish();
                }
            }
        };
        timer.start();*/
    super.onPause();
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import org.json.JSONException;
import org.json.JSONObject;

public class Generate_Token_Activity extends BaseActivity {

  private Spinner accountSpinner;
  private EditText mAmount;
  private TextView mErrorText;
  private Button SubmitBtn;
  private Boolean errorTextVisible = false;
  private ConnectionChecker connChecker = new ConnectionChecker(Generate_Token_Activity.this);

  private String Amount, AccountType, pidoMobile;

  private String createMyJSON() {

    JSONObject object = new JSONObject();
    try {
      object.put("accountNO", pidoMobile.replaceFirst("0", "234"));
      object.put("accountType", AccountType);
      object.put("amount", Amount);
    } catch (JSONException e) {
      e.printStackTrace();
    }

    return object.toString();
  }

  private void Verify_Dialog() {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.cardless_token_confirmation, null);

    TextView amount = (TextView) v.findViewById(R.id.cardless_amount);
    TextView accType = (TextView) v.findViewById(R.id.cardless_acc_type);

    accType.setText(AccountType);
    amount.setText("₦" + Amount);

    AlertDialog.Builder builder = new AlertDialog.Builder(Generate_Token_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connChecker.isAvailable()) {

          new getToken_task().execute();
        } else {

          Toast.makeText(Generate_Token_Activity.this, "No Connection To The Internet",
              Toast.LENGTH_LONG).show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        Generate_Token_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    setContentView(R.layout.activity_generate__token);

    accountSpinner = (Spinner) findViewById(R.id.acc_type_spinner);
    accountSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        AccountType = parent.getSelectedItem().toString();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    mAmount = (EditText) findViewById(R.id.generate_token_amnt);
    mAmount.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {

        if (errorTextVisible) {
          mErrorText.setVisibility(View.GONE);
        }

      }
    });
    mErrorText = (TextView) findViewById(R.id.amount_err);
    SubmitBtn = (Button) findViewById(R.id.btn_generate_token);
    SubmitBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Amount = mAmount.getText().toString().trim();

        if (!TextUtils.isEmpty(mAmount.getText())) {

          if (Integer.parseInt(Amount) > 20000) {
            mErrorText.setText("Withdrawal Amount Should Not Be More Than (NGN)20,000");
            mErrorText.setVisibility(View.VISIBLE);
            errorTextVisible = true;
          } else if (Integer.parseInt(Amount) < 1000) {
            mErrorText.setText("Withdrawal Amount Should Be From (NGN)1,000");
            mErrorText.setVisibility(View.VISIBLE);
            errorTextVisible = true;
          } else {

            Verify_Dialog();  // confirmation dialog
          }

        } else if (TextUtils.isEmpty(mAmount.getText())) {
          mAmount.setError("This Field Is Required");
        }

      }
    });
  }

  private class getToken_task extends AsyncTask<Void, Void, String> {

    Progressdialog progressdialog;
    String response;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      progressdialog = new Progressdialog(Generate_Token_Activity.this, "Please Wait..");
      progressdialog.showProgress();
    }

    @Override
    protected String doInBackground(Void... params) {
      try {
        response = new ConnectionToService().PostURL_with_header(createMyJSON());

      } catch (Exception e) {
        e.printStackTrace();

        response = "Unable to connect to the service at the moment.Please try again later";
      }

      return response;
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      progressdialog.dismissProgress();

      new Dialog(Generate_Token_Activity.this, s); // insert response into dialog

      mAmount.setText("");
    }
  }
}

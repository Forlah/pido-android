package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class SchoolFees_Activity extends BaseActivity {

  String[] Bills = {"Command Secondary School Enrollment", "Unical CES", "Unical Postgraduate",
      "NMS Enrollment Form",
      "CrestField College of Health Technology"};

  RecyclerView mschFees;
  SchoolFees_RecyclerAdapter mSchFee_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_school_fees);

    mSchFee_Adapter = new SchoolFees_RecyclerAdapter(getApplicationContext(), Bills);
    mschFees = (RecyclerView) findViewById(R.id.school_fees_list);
    mschFees.setHasFixedSize(true);
    mschFees.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    // mRecyclerView.setLayoutManager(mLayoutManager);
    mschFees.setLayoutManager(mLayoutManager);
    mschFees.setAdapter(mSchFee_Adapter);
  }

  private class SchoolFees_RecyclerAdapter extends
      RecyclerView.Adapter<SchoolFees_RecyclerAdapter.DataObjectHolder> {

    String[] bills;
    Context mContext;

    private SchoolFees_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      bills = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView fee_name;
      ImageView sch_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        fee_name = (TextView) itemView.findViewById(R.id.fee_name);
        sch_Image = (ImageView) itemView.findViewById(R.id.school_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.school_fees_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
      // final int _position = position;
      holder.fee_name.setText(bills[position]);

      if (bills[position].equals("Command Secondary School Enrollment")) {

        holder.sch_Image.setImageResource(R.mipmap.commandsecschool);
      } else if (bills[position].equals("Unical CES")) {
        //  holder.bill_name.setText(bills[position]);
        holder.sch_Image.setImageResource(R.mipmap.unical);

      } else if (bills[position].equals("Unical Postgraduate")) {
        //  holder.bill_name.setText(bills[position]);
        holder.sch_Image.setImageResource(R.mipmap.unical);

      } else if (bills[position].equals("NMS Enrollment Form")) {
        //  holder.bill_name.setText(bills[position]);
        holder.sch_Image.setImageResource(R.mipmap.nms_logo);

      } else {
        //holder.bill_name.setText(bills[position]);
        // holder.bill_Image.setImageResource(R.mipmap.sendmoney);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if ("Command Secondary School Enrollment".equals(bills[position])) {

            Intent i = new Intent(SchoolFees_Activity.this, CommandSec_Activity.class);
            startActivity(i);

          } else if (bills[position].equals("Unical CES")) {
            Intent i = new Intent(SchoolFees_Activity.this, UnicalCES_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("Unical Postgraduate")) {
            Intent i = new Intent(SchoolFees_Activity.this, UnicalPostgrad_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("NMS Enrollment Form")) {

            Intent i = new Intent(SchoolFees_Activity.this, NMSEnrollment_Activity.class);
            startActivity(i);

          } else {
            Intent i = new Intent(SchoolFees_Activity.this, CrestField_Activity.class);
            startActivity(i);
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return bills.length;
    }

  }

}

package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class CardlessWithdrawal_Activity extends BaseActivity {

  private String actions[] = {"Generate Token", "Check Token Status", "Cancel Token "};

  RecyclerView mRecycler;
  Cardless_RecyclerAdapter mCardless_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cardless_withdrawal);

    mCardless_Adapter = new Cardless_RecyclerAdapter(getApplicationContext(), actions);
    mRecycler = (RecyclerView) findViewById(R.id.card_withdrawal_list);
    mRecycler.setHasFixedSize(true);
    mRecycler.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    mRecycler.setLayoutManager(mLayoutManager);
    mRecycler.setAdapter(mCardless_Adapter);
  }

  private class Cardless_RecyclerAdapter extends
      RecyclerView.Adapter<Cardless_RecyclerAdapter.DataObjectHolder> {

    String[] items;
    Context mContext;

    private Cardless_RecyclerAdapter(Context context, String[] array) {

      mContext = context;
      items = array;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView trans_name;
      ImageView trans_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        trans_name = (TextView) itemView.findViewById(R.id.nameTxt);
        trans_Image = (ImageView) itemView.findViewById(R.id.iconTag);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.transfer_row_adapter, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
      holder.trans_name.setText(items[position]);

      if (items[position].equals("Generate Token")) {

        holder.trans_Image.setImageResource(R.mipmap.logo_large_wwide);
      } else if (items[position].equals("Check Token Status")) {

        holder.trans_Image.setImageResource(R.mipmap.tobank);
      } else {

        holder.trans_Image.setImageResource(R.mipmap.logo_large_wwide);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if (actions[position].equals("Generate Token")) {

            Intent intent = new Intent(CardlessWithdrawal_Activity.this,
                Generate_Token_Activity.class);
            startActivity(intent);
          } else if (actions[position].equals("Check Token Status")) {

            Intent intent = new Intent(CardlessWithdrawal_Activity.this,
                CardlessToken_Status_Activity.class);
            startActivity(intent);

          } else {

            Intent intent = new Intent(CardlessWithdrawal_Activity.this,
                Cancel_Token_Activity.class);
            startActivity(intent);
          }
        }
      });
    }

    @Override
    public int getItemCount() {
      return items.length;
    }

  }

}

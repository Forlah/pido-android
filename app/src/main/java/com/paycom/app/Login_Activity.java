package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.model.User;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.util.ArrayList;

/**
 * Created by FOLASHELE on 8/5/2015.
 */
public class Login_Activity extends AppCompatActivity {

  private Button login_btn;
  private TextView forgotPword;
  private static final String Tag = "lab_data";
  private ConnectionChecker connChecker = new ConnectionChecker(Login_Activity.this);
  private EditText mMobileNo, mPassword;
  private String MobileNO, Password, db_mobileNO, db_password, db_imei, db_token;
  private static final String App_Version = "3.0";
  Login_Task _task;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler reg_Db = new Reg_DatabaseHandler(Login_Activity.this);
    ArrayList<User> user = reg_Db.getAll();

    db_mobileNO = user.get(0).getMobile_no();
    db_password = user.get(0).getPassword();
    db_imei = user.get(0).getImei();
    db_token = user.get(0).getToken();

    if (db_mobileNO != null && db_password != null && db_imei != null && db_token != null) {

      setContentView(R.layout.login);

      mMobileNo = (EditText) findViewById(R.id.login_no);

      mPassword = (EditText) findViewById(R.id.login_password);

      mMobileNo.setText(db_mobileNO);

      login_btn = (Button) findViewById(R.id.login_btn);
      login_btn.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

//                    Intent i = new Intent(Login_Activity.this,Menu_Activity.class);
//                    startActivity(i);
//                    finish();

          MobileNO = mMobileNo.getText().toString();
          Password = mPassword.getText().toString();

          if (MobileNO.length() == 0 && Password.length() == 0) {
            mMobileNo.setError("This Field Is Required");
            mPassword.setError("This Field Is Required");
          } else if (MobileNO.length() == 0) {
            mMobileNo.setError("This Field Is Required");

          } else if (MobileNO.length() < 11) {
            mMobileNo.setError("Ensure 11 digit mobile no");

          } else if (Password.length() == 0) {
            mPassword.setError("This Field Is Required");

          } else if (Password.length() < 6) {
            mPassword.setError("Password must be a minimum of 6 characters");

          } else {
            // If login details is correct

            if (MobileNO.equals(db_mobileNO) && Password.equals(db_password)) {
              Encryption encrypt = new Encryption();

              String login_uri = "method=login&msisdn=" + encrypt.Base64Encoder(MobileNO.trim())
                  + "&password=" + Password.trim() + "&autcode=" + encrypt
                  .Base64Encoder(db_imei.trim()) + "&device=" + "android"
                  + "&token=" + db_token.trim() + "&version=" + App_Version.trim();

              if (connChecker.isAvailable()) {

                _task = new Login_Task();
                _task.execute(login_uri);
              } else {
                Toast.makeText(Login_Activity.this, "No Connection To The Internet",
                    Toast.LENGTH_LONG).show();
              }
            } else {
              new Dialog().error_Dialog(Login_Activity.this, "Invalid Login Credentials", "FAILED");
              mPassword.setText("");
            }
          }

        }
      });

      forgotPword = (TextView) findViewById(R.id.forgot);
      forgotPword.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          Intent i = new Intent(Login_Activity.this, ResetPassword_Activity.class);
          startActivity(i);
          finish();
        }
      });

    } else {
      setContentView(R.layout.login);
      Toast.makeText(Login_Activity.this, "Unable to retrieve your credentials ", Toast.LENGTH_LONG)
          .show();

    }

  }

  private class Login_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;
    String output;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Login_Activity.this, "Please wait a moment");
      Pd.showProgress_Title();

    }

    @Override
    protected String doInBackground(String... params) {
      // String ans =  new ConnectionToService(uri).ServiceResponse();
      try {
        output = new ConnectionToService(params[0]).download_secure_URL(Login_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Please try again after some time";
      }

    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      Pd.dismissProgress();

      if (s.contains("8001") == true) {

        Intent i = new Intent(Login_Activity.this, Menu_Activity.class);
        startActivity(i);
        finish();
      } else if (s.contains("8007") == true) {

        // display a dialog for user to change pin
        AlertDialog.Builder builder = new AlertDialog.Builder(Login_Activity.this);
        builder.setTitle("INFORMATION");
        builder.setMessage("Please Kindly Change your PIN");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            // Intent i = new Intent(Login_Activity.this, PinChange_Activity.class);
            Intent i = new Intent(Login_Activity.this, ChangePin_Activity.class);
            startActivity(i);
          }
        });
        builder.show();

        finish();

      } else {
        new Dialog().error_Dialog(Login_Activity.this, s, "Login Failed");
      }

    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.Application.VolleyAppl;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class KadunaElectricity_Activity extends BaseActivity {

  private EditText mMeterNo, mAmount, mPin;
  private String MeterNo, Amount, Pin, PaymentType, pidoMobile;
  private Button Kaduna_Btn;
  private ConnectionChecker connCheck = new ConnectionChecker(KadunaElectricity_Activity.this);
  VolleyAppl helper = VolleyAppl.getInstance(); // instantiate volley connection application class
  private Spinner mPaymentType;
  private Kaduna_Task _task;

  // volley timeout constants
  /** The default socket timeout in milliseconds */
  /**
   * The default number of retries
   */
  public static final int DEFAULT_MAX_RETRIES = 0;

  /**
   * The default backoff multiplier
   */
  public static final float DEFAULT_BACKOFF_MULT = 1f;

  private void Kaduna_Confirmation_dialog(String name, String pay_ref, String mAddress,
      String charge) throws UnsupportedEncodingException {

    // since the transaction detials of all will be equal , inflate this for all PHCN transaction details dialog

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.kaduna_confirmation, null);
    final String kaduna_url;

    TextView mCustomer_name = (TextView) v.findViewById(R.id.acc_name_txt);
    TextView _mMeterNo_txt = (TextView) v.findViewById(R.id.meter_no_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.amount_txt);
    TextView Address_txt = (TextView) v.findViewById(R.id.address_txt);
    TextView metertype_txt = (TextView) v.findViewById(R.id.meter_type_txt);
    TextView mCharge_txt = (TextView) v.findViewById(R.id.charge_txt);
    TextView mTotal_txt = (TextView) v.findViewById(R.id.total_txt);

    //Encryption encryption = new Encryption();
    JSONObject obj = new JSONObject();
    int newAmount = Integer.parseInt(Amount) * 100;
    try {
      obj.put("accountType", PaymentType);
      obj.put("accountNumber", MeterNo);
      obj.put("amount", String.valueOf(newAmount));
      obj.put("location", mAddress);
      obj.put("payerName", name);
      obj.put("payRef", pay_ref);
      obj.put("channel", "MOBILE");
      obj.put("phoneNumber", pidoMobile);
      obj.put("subAgentUID", pidoMobile);
      obj.put("msisdn", pidoMobile);
      obj.put("pin", Pin);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    kaduna_url =
        "method=kadunaelectricitypayment&jsonString=" + URLEncoder.encode(obj.toString(), "UTF-8");

    mCustomer_name.setText(name);
    _mMeterNo_txt.setText(MeterNo);
    mAmount_txt_view.setText("₦" + Amount);
    Address_txt.setText(mAddress);
    metertype_txt.setText(PaymentType);
    mCharge_txt.setText("₦" + charge);

    int total_amount = Integer.parseInt(Amount) + Integer.parseInt(charge);

    mTotal_txt.setText("₦" + String.valueOf(total_amount));

    AlertDialog.Builder builder = new AlertDialog.Builder(KadunaElectricity_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _task = new Kaduna_Task();
          _task.execute(kaduna_url);

        } else {
          Toast.makeText(KadunaElectricity_Activity.this, "No Connection To The Internet",
              Toast.LENGTH_LONG).show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_kaduna_electricity);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        KadunaElectricity_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mMeterNo = (EditText) findViewById(R.id.kaduna_meter);
    mAmount = (EditText) findViewById(R.id.kaduna_amount);
    mPin = (EditText) findViewById(R.id.kaduna_pin);
    Kaduna_Btn = (Button) findViewById(R.id.btn_kaduna);

    mPaymentType = (Spinner) findViewById(R.id.kaduna_spinner);
    mPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        PaymentType = parent.getSelectedItem().toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    Kaduna_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        MeterNo = mMeterNo.getText().toString().trim();
        Amount = mAmount.getText().toString().trim();
        Pin = mPin.getText().toString().trim();

        if (MeterNo.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mMeterNo.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (MeterNo.length() == 0) {
          mMeterNo.setError("This Field Is Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Integer.parseInt(Amount) < 10) {

          mAmount.setError("Invalid Amount");

        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {

          String phcn_charge_uri =
              "method=kadunaelectricity&meterType=" + PaymentType + "&accountNo=" + MeterNo;

          if (connCheck.isAvailable()) {

            new GetCustomerName().execute(phcn_charge_uri);

          } else {

            Toast.makeText(KadunaElectricity_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }
      }

    });

  }

  //    private void GetCustomerName(String phcn_charge_uri){
//
//        final Progressdialog Pd;
//        Pd = new Progressdialog(KadunaElectricity_Activity.this,"Please Wait..");
//        Pd.showProgress();
//
//        Volley_StringRequest stringRequest = new Volley_StringRequest(Request.Method.POST, phcn_charge_uri, new Response.Listener<String>() {
//            @Override
//            public void onResponse(String response) {
//
//                Pd.dismissProgress();
//
//                String payRef, address, charge, Customer_Name;
//
//                try {
//
//                    JSONObject object = new JSONObject(response.toString());
//
//                    Customer_Name = object.get("name").toString();
//                    payRef = object.get("payRef").toString();
//                    address = object.get("address").toString();
//                    charge = object.get("charge").toString();
//
//                    try {
//
//                        Kaduna_Confirmation_dialog(Customer_Name,payRef, address, charge);
//
//                    } catch (UnsupportedEncodingException e) {
//                        e.printStackTrace();
//                    }
//
//
//                } catch (JSONException e) {
//
//                    e.printStackTrace();
//
//                        // display the dialog with the errors
//                        new Dialog(KadunaElectricity_Activity.this, response.trim());
//
//                }
//
//            }
//
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//
//                Pd.dismissProgress();
//                new Dialog(KadunaElectricity_Activity.this,"Connection Failed.Please Try again later");
//                error.printStackTrace();
//            }
//        });
//
//
//
//        stringRequest.setPriority(Request.Priority.HIGH);
//        RetryPolicy policy = new DefaultRetryPolicy(ConnectionToService.CONNECTION_TIME_OUT, DEFAULT_MAX_RETRIES, DEFAULT_BACKOFF_MULT);
//        stringRequest.setRetryPolicy(policy);
//        helper.add(stringRequest);
//
//    }
//
  private class GetCustomerName extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(KadunaElectricity_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      try {

        System.out.println("kaduna url = " + params[0]);

        ConnectionToService c = new ConnectionToService(params[0]);
        String json_string_result = c.download_secure_URL(KadunaElectricity_Activity.this);

        return json_string_result;

      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      Pd.dismissProgress();

      String payRef, address, charge, Customer_Name;

      try {

        JSONObject object = new JSONObject(s.toString());

        Customer_Name = object.get("name").toString();
        payRef = object.get("payRef").toString();
        address = object.get("address").toString();
        charge = object.get("charge").toString();

        try {

          Kaduna_Confirmation_dialog(Customer_Name, payRef, address, charge);

        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
        }

      } catch (JSONException e) {

        e.printStackTrace();

        // display the dialog with the errors
        new Dialog(KadunaElectricity_Activity.this, s.trim());

      }

    }
  }

  private class Kaduna_Task extends AsyncTask<String, Void, String> {

    String output;
    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(KadunaElectricity_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {

      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(KadunaElectricity_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      Pd.dismissProgress();

      System.out.println("The response = " + s.trim().toString());

      new Dialog(KadunaElectricity_Activity.this,
          s.trim().toString()); // insert response into dialog
      mMeterNo.setText("");
      mAmount.setText("");
      mPin.setText("");

    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class StarTimes_Activity extends BaseActivity {

  private EditText mCustomerId, mPin, mAmount;
  private String CustomerId, Pin, Amount, pidoMobile;
  private Button Startimes_btn;
  private static final String Tag = "startimes";
  private final String startime_trans_charge = "100";
  private ConnectionChecker connCheck = new ConnectionChecker(StarTimes_Activity.this);
  private Startimes_Task _task;

  private void Startimes_Confirmation_dialog() {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.startimes_confirmation, null);

    TextView mId = (TextView) v.findViewById(R.id.startimes_cus_id_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.startimes_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.startimes_trans_txt);
    TextView mTotal = (TextView) v.findViewById(R.id.startimes_total_txt);

    mId.setText(CustomerId);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + startime_trans_charge);

    int total = Integer.parseInt(startime_trans_charge) + Integer.parseInt(Amount);
    mTotal.setText("₦" + total);

    Encryption encrypt = new Encryption(); //
    final String startimes_uri = "method=billspayment&amount=" + Amount
        + "&customerid=" + encrypt.Base64Encoder(CustomerId)
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&productid=32&charge=" + startime_trans_charge;

    AlertDialog.Builder builder = new AlertDialog.Builder(StarTimes_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _task = new Startimes_Task();
          _task.execute(startimes_uri);
        } else {
          Toast
              .makeText(StarTimes_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.activity_star_times_);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        StarTimes_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.startimes_cus_id);
    mPin = (EditText) findViewById(R.id.startimes_pin);
    mAmount = (EditText) findViewById(R.id.startimes_amount);

    Startimes_btn = (Button) findViewById(R.id.btn_startimes);
    Startimes_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomerId = mCustomerId.getText().toString();
        Pin = mPin.getText().toString();
        Amount = mAmount.getText().toString();

        if (CustomerId.length() == 0 && Pin.length() == 0 && Amount.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (Amount.length() < 3) {
          mAmount.setError("Amount Not Valid");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError(" 4 Digit Number Required");
        } else {

          String startime_charge_url =
              "method=billscharge&amount=" + Amount + "&mobile=" + pidoMobile;
          Log.d(Tag, "Startimes charge url is: " + startime_charge_url);
          Startimes_Confirmation_dialog();
        }
      }
    });
  }

  private class VerifyStartimes_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(StarTimes_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(StarTimes_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      if (s.contentEquals("Unable to connect to the service at the moment.Please try again later")
          || s.startsWith(" ")) {

        Pd.dismissProgress();
        // an error occured on the service
        // display the dialog with the errors
        new Dialog(StarTimes_Activity.this, s);

      } else {
        Pd.dismissProgress();
        //  trans_charge = s;
        // display a dialog for the user to verify the transaction
        //  Startimes_Confirmation_dialog(CustomerId,trans_charge, Amount);
      }
    }
  }

  private class Startimes_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(StarTimes_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(StarTimes_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(StarTimes_Activity.this, result); // insert response into dialog
      mCustomerId.setText("");
      mPin.setText("");
      mAmount.setText("");

    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

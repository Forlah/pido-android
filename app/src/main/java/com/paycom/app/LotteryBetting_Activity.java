package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class LotteryBetting_Activity extends BaseActivity {

  private String actions[] = {"Bet9ja"};

  RecyclerView mBet;
  Lottery_RecyclerAdapter mLottery_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_lottery_betting);

    mLottery_Adapter = new Lottery_RecyclerAdapter(getApplicationContext(), actions);
    mBet = (RecyclerView) findViewById(R.id.lottery_list);
    mBet.setHasFixedSize(true);
    mBet.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    mBet.setLayoutManager(mLayoutManager);
    mBet.setAdapter(mLottery_Adapter);
  }

  private class Lottery_RecyclerAdapter extends
      RecyclerView.Adapter<Lottery_RecyclerAdapter.DataObjectHolder> {

    String[] types;
    Context mContext;

    private Lottery_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      types = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView bet_name;
      ImageView bet_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        bet_name = (TextView) itemView.findViewById(R.id.bet_name);
        bet_Image = (ImageView) itemView.findViewById(R.id.bet_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.lottery_betting_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
      holder.bet_name.setText(types[position]);
      holder.bet_Image.setImageResource(R.mipmap.bet9ja);

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          Intent i = new Intent(LotteryBetting_Activity.this, Bet9ja_Activity.class);
          startActivity(i);

        }
      });

    }

    @Override
    public int getItemCount() {
      return types.length;
    }

  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.DatabaseHandler;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.StringTokenizer;

/**
 * Created by FOLASHELE on 8/10/2015.
 */
public class TransferToBank_Activity extends BaseActivity {

  private EditText mAcc_No, mAmount, mRecipient_Phone, mRemark, mPin;
  private String Acc_No, Amount, Recipient_Phone, Remark, Bank, Pin;
  private Button Transfer_to_bank_btn;
  private Spinner mBank;
  private String verify_uri, Acc_Name;
  private String transfer_task_uri;
  private ImageView PickUpContact;
  Encryption encrypt;
  private String pidoMobile;
  private static final String Tag = "transferTobank";
  private ConnectionChecker connChecker = new ConnectionChecker(TransferToBank_Activity.this);
  private VerifyAcc_Task _task;
  private TransferToBank_Task _Task;
  ArrayAdapter<String> adapter;

  private String[] naija_banks = {"Access", "Citi", "Diamond", "Ecobank", "Enterprise",
      "FCMB", "Fidelity", "FirstBank", "GTB",
      "Heritage", "JAIZ", "KeyStone", "MainStreet",
      "Skye", "Stanbic", "Standard", "Sterling",
      "UBA", "Union", "Unity", "Wema", "Zenith"};

  private void Verify_Dialog(String name, String charge, String acc_no, String bank, String amount,
      String recipient_no) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.transfer_to_bank_confirmation, null);

    TextView mAccount_name = (TextView) v.findViewById(R.id.acc_name_txt);
    TextView mBank_Name = (TextView) v.findViewById(R.id.bank_txt);
    TextView mAccount_no = (TextView) v.findViewById(R.id.acc_no_txt);
    TextView mRecipeint = (TextView) v.findViewById(R.id.receipient_no_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.charge_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.total_txt);

    mAccount_name.setText(name);
    mBank_Name.setText(bank);
    mAccount_no.setText(acc_no);
    mRecipeint.setText(recipient_no);
    mAmount_txt_view.setText("₦" + amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = Integer.parseInt(charge) + Integer.parseInt(amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(TransferToBank_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connChecker.isAvailable()) {
          _Task = new TransferToBank_Task();
          _Task.execute(transfer_task_uri);
        } else {
          Toast.makeText(TransferToBank_Activity.this, "No Connection To The Internet",
              Toast.LENGTH_LONG).show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  private String getPhoneNumber(Uri paramUri) {
    String id = "";
    String no = "";
    Cursor cursor = getContentResolver().query(paramUri, null, null, null, null);

    while (cursor.moveToNext()) {
      id = cursor.getString(cursor.getColumnIndex("_id"));
      if ("1".equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("has_phone_number")))) {
        Cursor cursorNo = getContentResolver()
            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + id,
                null, null);
        while (cursorNo.moveToNext()) {
          if (cursorNo.getInt(cursorNo.getColumnIndex("data2")) == 2) {
            no = no.concat(cursorNo.getString(cursorNo.getColumnIndex("data1")));
            break;
          }
        }
        cursorNo.close();
      }
    }
    cursor.close();
    return no;
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.transfer_to_bank);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        TransferToBank_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    adapter = new ArrayAdapter<>(TransferToBank_Activity.this, android.R.layout.simple_spinner_item,
        naija_banks);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    adapter.notifyDataSetChanged();

    encrypt = new Encryption();

    mAcc_No = (EditText) findViewById(R.id.acc_no);
    mAmount = (EditText) findViewById(R.id.trans_to_bank_amount);
    mRecipient_Phone = (EditText) findViewById(R.id.recipient_no);
    mRemark = (EditText) findViewById(R.id.to_bank_remark);
    mPin = (EditText) findViewById(R.id.to_bank_pin);
    mBank = (Spinner) findViewById(R.id.banks_spinner);
    mBank.setAdapter(adapter);
    mBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Bank = parent.getSelectedItem().toString();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    String No = getIntent().getStringExtra("no perform bank transfer");
    String acc = getIntent().getStringExtra("acc_no  to perform bank transfer");
    String bnk = getIntent().getStringExtra("bank_name from contact");

    if (No != null && acc != null && bnk != null) {

      mBank.setSelection(adapter.getPosition(bnk));
      mAcc_No.setText(acc);
      mRecipient_Phone.setText(No);

    }

    PickUpContact = (ImageView) findViewById(R.id.trans_bnk_Contact);

    PickUpContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent localIntent = new Intent("android.intent.action.PICK",
            ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(localIntent, 1);
      }
    });

    Transfer_to_bank_btn = (Button) findViewById(R.id.btn_to_bank);
    Transfer_to_bank_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Acc_No = mAcc_No.getText().toString();
        Amount = mAmount.getText().toString();
        Recipient_Phone = mRecipient_Phone.getText().toString();
        Remark = mRemark.getText().toString();
        Pin = mPin.getText().toString();

        if (Acc_No.length() == 0 && Amount.length() == 0 && Recipient_Phone.length() == 0
            && Pin.length() == 0) {

          mAcc_No.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mRecipient_Phone.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (Acc_No.length() == 0) {
          mAcc_No.setError("This Field Is Required");
        } else if (Acc_No.length() < 10) {
          mAcc_No.setError("10 Digit NUBAN Account Number Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Recipient_Phone.length() == 0) {
          mRecipient_Phone.setError("This Field Is Required");

        } else if (Recipient_Phone.length() < 11) {
          mRecipient_Phone.setError("11 Digit Phone Number Required");

        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Pin Required");
        } else {
          Encryption encrypt = new Encryption();

          verify_uri = "method=getchargeandbankname&amount=" + Amount
              + "&nubanacc=" + encrypt.Base64Encoder(Acc_No)
              + "&bankname=" + Bank
              + "&mobile=" + Recipient_Phone;

          try {
            transfer_task_uri =
                "method=pidotobank&amount=" + Amount + "&bankname=" + Bank + "&mobile=" + encrypt
                    .Base64Encoder(pidoMobile) +
                    "&remark=" + URLEncoder.encode(Remark, "UTF-8")
                    + "&nubanacc=" + URLEncoder.encode(Acc_No, "UTF-8") + "&pin=" + encrypt
                    .Base64Encoder(Pin) + "&recipientmobile=" + Recipient_Phone;
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }

          if (connChecker.isAvailable()) {

            _task = new VerifyAcc_Task();
            _task.execute(verify_uri);

          } else {
            Toast.makeText(TransferToBank_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }

      }
    });

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent paramIntent) {
    // TODO Auto-generated method stub
    super.onActivityResult(requestCode, resultCode, paramIntent);
    if (resultCode == RESULT_OK) {
      String str = getPhoneNumber(paramIntent.getData());
      if (str.trim().length() > 0) {

        String result = str.replace(" ", "");
        if (result.contains("+234")) {
          String out = result.replace("+234", "0");
          mRecipient_Phone.setText(out.trim());
        } else if (result.contains("-")) {
          String out = result.replace("-", "");
          mRecipient_Phone.setText(out.trim());
        } else {
          mRecipient_Phone.setText(result);
        }
      }
    } else {
      Toast.makeText(this, "Phone Number Not Found...", Toast.LENGTH_SHORT).show();
    }
  }

  private class VerifyAcc_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(TransferToBank_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(TransferToBank_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      String charge;
      String extra = "";
      if (s.contains("|")) {
        Pd.dismissProgress();
        // if the result contain "|" sign means valid response
        StringTokenizer tokens = new StringTokenizer(s, "|");
        charge = tokens.nextToken();
        Acc_Name = tokens.nextToken();

        if (Acc_Name.equals("200")) {
          Acc_Name = "Unavailable at the moment";
        }

        // display a dialog for the user to verify the acc

        Verify_Dialog(Acc_Name, charge, Acc_No, Bank, Amount, Recipient_Phone);

      } else {
        // an error occured on the service
        Pd.dismissProgress();

        new Dialog(TransferToBank_Activity.this, s); // insert response into dialog
      }
    }
  }

  private class TransferToBank_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;
    DatabaseHandler dbHandler = new DatabaseHandler(TransferToBank_Activity.this);

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(TransferToBank_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(TransferToBank_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(TransferToBank_Activity.this, result); // insert response into dialog

      mAcc_No.setText("");
      mAmount.setText("");
      mRecipient_Phone.setText("");
      mRemark.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

package com.paycom.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class CardlessToken_Status_Activity extends BaseActivity {

  private EditText mPaycode;
  private String Paycode;
  private Button Btn_Status;
  private String safeURl, pidoMobile;

  private ConnectionChecker connChecker = new ConnectionChecker(CardlessToken_Status_Activity.this);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        CardlessToken_Status_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    setContentView(R.layout.activity_cardless_token__status);

    mPaycode = (EditText) findViewById(R.id.check_token_transref);
    Btn_Status = (Button) findViewById(R.id.btn_token_status);
    Btn_Status.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Paycode = mPaycode.getText().toString();

        if (TextUtils.isEmpty(mPaycode.getText())) {
          mPaycode.setError("This Field Is Required");
        } else {

          try {
            safeURl = "/tokenStatus?mobileNO=" + pidoMobile.replaceFirst("0", "234") + "&payCode="
                + URLEncoder.encode(Paycode, "UTF-8");

            if (connChecker.isAvailable()) {

              new checkStatus_Task().execute();
            } else {

              Toast.makeText(CardlessToken_Status_Activity.this, "No Connection To The Internet",
                  Toast.LENGTH_LONG).show();
            }

          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }

        }
      }
    });

  }

  private class checkStatus_Task extends AsyncTask<Void, Void, String> {

    Progressdialog progressdialog;
    String response;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      progressdialog = new Progressdialog(CardlessToken_Status_Activity.this, "Please Wait..");
      progressdialog.showProgress();
    }

    @Override
    protected String doInBackground(Void... params) {
      try {

        response = new ConnectionToService().POST_GET_URL(safeURl, "GET");

      } catch (Exception e) {
        e.printStackTrace();

        response = "Unable to connect to the service at the moment.Please try again later";
      }

      return response;
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      progressdialog.dismissProgress();

      new Dialog(CardlessToken_Status_Activity.this, s); // insert response into dialog

      mPaycode.setText("");
    }
  }
}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Created by FOLASHELE on 8/14/2015.
 */
public class Dstv_Activity extends BaseActivity {

  private EditText mCustomerId, mPin;
  private String CustomerId, Pin, pidoMobile;
  private Button Dstv_btn;
  private Spinner bouquet;
  private String Amount;
  private String Selected_choice;
  private static final String Tag = "dstv";
  private String dstv_trans_charge;
  private ConnectionChecker connCheck = new ConnectionChecker(Dstv_Activity.this);
  private Dstv_Task _task;
  private Dstv_verify_Task _Task;

  private void dstv_Confirmation_dialog(String dstv_trans_charge, String amount) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.dstv_confirmation, null);

    TextView mId = (TextView) v.findViewById(R.id.dstv_cus_id_txt);
    TextView bouquet = (TextView) v.findViewById(R.id.dstv_bouquet_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.dstv_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.dstv_trans_txt);
    TextView mTotal = (TextView) v.findViewById(R.id.dstv_total_txt);

    mId.setText(CustomerId);
    bouquet.setText(Selected_choice);
    mAmount_txt_view.setText("₦" + amount);
    mTrans_charge.setText("₦" + dstv_trans_charge);

    int total = Integer.parseInt(dstv_trans_charge.trim()) + Integer.parseInt(amount.trim());
    mTotal.setText("₦" + total);

    Encryption encrypt = new Encryption();
    final String dstv_uri = "method=billspayment&customerid=" + encrypt.Base64Encoder(CustomerId)
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&product=" + Selected_choice;

    AlertDialog.Builder builder = new AlertDialog.Builder(Dstv_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _task = new Dstv_Task();
          _task.execute(dstv_uri);
        } else {
          Toast.makeText(Dstv_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.dstv);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Dstv_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.dstv_cus_id);
    mPin = (EditText) findViewById(R.id.dstv_pin);

    bouquet = (Spinner) findViewById(R.id.dstv_spinner);
    bouquet.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Selected_choice = parent.getSelectedItem().toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    Dstv_btn = (Button) findViewById(R.id.dstv_btn);

    Dstv_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomerId = mCustomerId.getText().toString();
        Pin = mPin.getText().toString();

        if (CustomerId.length() == 0 && Pin.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (CustomerId.length() < 10) {
          mCustomerId.setError("Incomplete customer Id");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          String dstv_acc_uri;
          try {
            dstv_acc_uri =
                "method=billscharges&productname=" + URLEncoder.encode(Selected_choice, "UTF-8")
                    .toString() + "&mobileno=" + pidoMobile;
            _Task = new Dstv_verify_Task();
            _Task.execute(dstv_acc_uri);

          } catch (UnsupportedEncodingException e) {

            e.printStackTrace();
          }

        }
      }
    });

  }

  private class Dstv_verify_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(Dstv_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Dstv_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      if (s.contains("|")) {

        try {
          Pd.dismissProgress();
          // if the result contain "|" sign means valid response
          StringTokenizer tokens = new StringTokenizer(s, "|");
          dstv_trans_charge = tokens.nextToken();
          //   customerName = tokens.nextToken();
          Amount = tokens.nextToken();

          // display a dialog for the user to verify the acc
          dstv_Confirmation_dialog(dstv_trans_charge, Amount);

                  /*  if (customerName.equals("200")) {
                        customerName = "Unavailable at the moment";
                    }*/

        } catch (NoSuchElementException NE) {
          Pd.dismissProgress();
          new Dialog().error_Dialog(Dstv_Activity.this, "Please Check Your Id Again", "FAILED");
        }

      } else {
        // an error occured on the service
        Pd.dismissProgress();

        new Dialog(Dstv_Activity.this, s); // insert response into dialog
      }
    }
  }

  private class Dstv_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Dstv_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Dstv_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      //Log.d(Tag,"Result @ postExecute of airtimetop is: "+result);
      Pd.dismissProgress(); // close progress dialog after thread is done
      new Dialog(Dstv_Activity.this, result);

      mCustomerId.setText("");
      mPin.setText("");

    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }
}


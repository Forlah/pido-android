package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.util.StringTokenizer;

/**
 * Created by FOLASHELE on 8/10/2015.
 */
public class TransferToPido_Activity extends BaseActivity {

  private EditText mRecipientNo, mAmount, mPin;
  private String RecipientNo, Amount, Pin;
  private Button Transfer_to_Pido_btn;
  private ImageView PickUpContact;
  private String P2P_charge_uri;
  private String P2P_transfer_uri, pidoMobile;
  private static final String Tag = "P2P";
  private ConnectionChecker connChecker = new ConnectionChecker(TransferToPido_Activity.this);
  private VerifyP2P_Task _task;
  private P2P_Transfer_Task _Task;

  private void P2P_Confirmation_dialog(String name, String charge, String amount) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.transfer_to_pido_confirmation, null);

    TextView mAccount_name = (TextView) v.findViewById(R.id.pido_recipient_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.pido_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.pido_trans_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.pido_total_txt);

    mAccount_name.setText(name);
    mAmount_txt_view.setText("₦" + amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = Integer.parseInt(charge) + Integer.parseInt(amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(TransferToPido_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        _Task = new P2P_Transfer_Task();
        _Task.execute(P2P_transfer_uri);

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  private String getPhoneNumber(Uri paramUri) {
    String id = "";
    String no = "";
    Cursor cursor = getContentResolver().query(paramUri, null, null, null, null);

    while (cursor.moveToNext()) {
      id = cursor.getString(cursor.getColumnIndex("_id"));
      if ("1".equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("has_phone_number")))) {
        Cursor cursorNo = getContentResolver()
            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + id,
                null, null);
        while (cursorNo.moveToNext()) {
          if (cursorNo.getInt(cursorNo.getColumnIndex("data2")) == 2) {
            no = no.concat(cursorNo.getString(cursorNo.getColumnIndex("data1")));
            break;
          }
        }
        cursorNo.close();
      }
    }
    cursor.close();
    return no;
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent paramIntent) {
    // TODO Auto-generated method stub
    super.onActivityResult(requestCode, resultCode, paramIntent);
    if (resultCode == RESULT_OK) {
      String str = getPhoneNumber(paramIntent.getData());
      if (str.trim().length() > 0) {

        String result = str.replace(" ", "");
        if (result.contains("+234")) {
          String out = result.replace("+234", "0");
          mRecipientNo.setText(out.trim());
        } else if (result.contains("-")) {
          String out = result.replace("-", "");
          mRecipientNo.setText(out.trim());
        } else {
          mRecipientNo.setText(result);
        }
      }
    } else {
      // no found number was selected from phone contact
    }
  }

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.pido_to_pido);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        TransferToPido_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    PickUpContact = (ImageView) findViewById(R.id.trans_p2p_Contact);

    PickUpContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent localIntent = new Intent("android.intent.action.PICK",
            ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(localIntent, 1);
      }
    });

    mRecipientNo = (EditText) findViewById(R.id.to_pido_no);
    mAmount = (EditText) findViewById(R.id.to_pido_amount);
    mPin = (EditText) findViewById(R.id.to_pido_pin);

    String cReceipient = getIntent()
        .getStringExtra("From Contact Activity to perform PiDO transfer");
    if (cReceipient != " ") {
      mRecipientNo.setText(cReceipient);
    }

    Transfer_to_Pido_btn = (Button) findViewById(R.id.btn_to_pido);
    Transfer_to_Pido_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        RecipientNo = mRecipientNo.getText().toString();
        Amount = mAmount.getText().toString();
        Pin = mPin.getText().toString();

        if (RecipientNo.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mRecipientNo.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (RecipientNo.length() == 0) {
          mRecipientNo.setError("This Field Is Required");

        } else if (RecipientNo.length() < 11) {
          mRecipientNo.setError("11 Digit Phone Number Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Pin Required");
        } else {
          Encryption encrypt = new Encryption();
          P2P_charge_uri = "method=chargesp2p&amount=" + Amount + "&msisdn=" + RecipientNo;

          Log.d(Tag, "P2P charge uri is : " + P2P_charge_uri);
          P2P_transfer_uri = "method=p2p&phone=" + encrypt.Base64Encoder(pidoMobile) +
              "&amount=" + Amount + "&pin=" + encrypt.Base64Encoder(Pin) + "&recipientmobile="
              + encrypt.Base64Encoder(RecipientNo);

          if (connChecker.isAvailable()) {
            _task = new VerifyP2P_Task();
            _task.execute(P2P_charge_uri);
          } else {
            Toast.makeText(TransferToPido_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }

      }
    });

  }

  private class VerifyP2P_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(TransferToPido_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(TransferToPido_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      String charge, Pido_Acc_Name;
      if (s.contains("|")) {
        Pd.dismissProgress();
        // if the result contain "|" sign means valid response
        StringTokenizer tokens = new StringTokenizer(s, "|");
        charge = tokens.nextToken();
        Pido_Acc_Name = tokens.nextToken().trim().toString();

        if (Pido_Acc_Name.equals("200")) {
          Pido_Acc_Name = "Unavailable at the moment";
        }

        // display a dialog for the user to verify the transaction
        P2P_Confirmation_dialog(Pido_Acc_Name, charge, Amount);

      } else {
        Pd.dismissProgress();
        // an error occured on the service
        // display the dialog with the errors
        new Dialog(TransferToPido_Activity.this, "Error Occurred");
      }
    }
  }

  private class P2P_Transfer_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(TransferToPido_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(TransferToPido_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(TransferToPido_Activity.this, result); // insert response into dialog
      mRecipientNo.setText("");
      mAmount.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

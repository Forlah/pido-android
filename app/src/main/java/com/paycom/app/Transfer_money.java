package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class Transfer_money extends BaseActivity {

  private String actions[] = {"Transfer to bank", "Transfer to PiDO"};

  RecyclerView mTrans;
  TransferMoney_RecyclerAdapter mTransfer_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.transfer_money);

    mTransfer_Adapter = new TransferMoney_RecyclerAdapter(getApplicationContext(), actions);
    mTrans = (RecyclerView) findViewById(R.id.transfer_money_list);
    mTrans.setHasFixedSize(true);
    mTrans.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    mTrans.setLayoutManager(mLayoutManager);
    mTrans.setAdapter(mTransfer_Adapter);

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.

    //noinspection SimplifiableIfStatement

    return super.onOptionsItemSelected(item);

  }

  private class TransferMoney_RecyclerAdapter extends
      RecyclerView.Adapter<TransferMoney_RecyclerAdapter.DataObjectHolder> {

    String[] bills;
    Context mContext;

    private TransferMoney_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      bills = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView trans_name;
      ImageView trans_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        trans_name = (TextView) itemView.findViewById(R.id.nameTxt);
        trans_Image = (ImageView) itemView.findViewById(R.id.iconTag);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.transfer_row_adapter, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
      final int _position = position;
      holder.trans_name.setText(bills[position]);

      if (bills[position].equals("Transfer to PiDO")) {

        holder.trans_Image.setImageResource(R.mipmap.logo_large_wwide);
      } else {
        holder.trans_Image.setImageResource(R.mipmap.tobank);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if (bills[_position].equals("Transfer to PiDO")) {
            Intent i = new Intent(Transfer_money.this, TransferToPido_Activity.class);
            startActivity(i);
          } else {

            Intent i = new Intent(Transfer_money.this, TransferToBank_Activity.class);
            startActivity(i);
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return bills.length;
    }

  }

}





package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

public class Gotv_Activity extends BaseActivity {

  private EditText mCustomerId, mPin;
  private String CustomerId, Pin, pidoMobile;
  private Button Gotv_btn;
  private Spinner options;
  private String Selected_option, customerName, amount;
  private String gotv_trans_charge;
  private ConnectionChecker connCheck = new ConnectionChecker(Gotv_Activity.this);
  private Gotv_verify_Task _Task;
  private Gotv_Task _task;

  private void gotv_Confirmation_dialog(String charge, String Amount) {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.gotv_confirmation, null);

    //  TextView mName = (TextView) v.findViewById(R.id.gotv_cus_name_txt);
    TextView mId = (TextView) v.findViewById(R.id.gotv_cus_id_txt);
    TextView option = (TextView) v.findViewById(R.id.gotv_option_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.gotv_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.gotv_trans_txt);
    TextView mTotal = (TextView) v.findViewById(R.id.gotv_total_txt);

    //  mName.setText(cusName);
    mId.setText(CustomerId);
    option.setText(Selected_option);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + charge);

    int total = Integer.parseInt(charge.trim()) + Integer.parseInt(Amount.trim());
    mTotal.setText("₦" + total);

    Encryption encrypt = new Encryption();
    final String gotv_uri = "method=billspayment&customerid=" + encrypt.Base64Encoder(CustomerId)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&product=" + Selected_option;

    AlertDialog.Builder builder = new AlertDialog.Builder(Gotv_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _task = new Gotv_Task();
          _task.execute(gotv_uri);

        } else {
          Toast.makeText(Gotv_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_gotv);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Gotv_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.gotv_cus_id);
    mPin = (EditText) findViewById(R.id.gotv_pin);

    options = (Spinner) findViewById(R.id.gotv_spinner);
    options.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Selected_option = parent.getSelectedItem().toString();

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    Gotv_btn = (Button) findViewById(R.id.gotv_btn);
    Gotv_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        CustomerId = mCustomerId.getText().toString();
        Pin = mPin.getText().toString();

        if (CustomerId.length() == 0 && Pin.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (CustomerId.length() < 10) {
          mCustomerId.setError("Incomplete Customer Id");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError(" 4 Digit Number Required");
        } else {

          String gotv_acc_uri = null;
          try {
            gotv_acc_uri =
                "method=billscharges&productname=" + URLEncoder.encode(Selected_option, "UTF-8")
                    + "&mobileno=" + pidoMobile;
            _Task = new Gotv_verify_Task();
            _Task.execute(gotv_acc_uri);
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }

        }
      }
    });
  }

  private class Gotv_verify_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(Gotv_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Gotv_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      if (s.contains("|")) {

        try {
          Pd.dismissProgress();
          // if the result contain "|" sign means valid response
          StringTokenizer tokens = new StringTokenizer(s, "|");
          gotv_trans_charge = tokens.nextToken();
          //   customerName = tokens.nextToken();
          amount = tokens.nextToken();

          // display a dialog for the user to verify the acc
          gotv_Confirmation_dialog(gotv_trans_charge, amount);

                  /*  if (customerName.equals("200")) {
                        customerName = "Unavailable at the moment";
                    }*/

        } catch (NoSuchElementException NE) {
          Pd.dismissProgress();
          new Dialog().error_Dialog(Gotv_Activity.this, "Please Check Your Id Again", "FAILED");
        }

      } else {
        // an error occured on the service
        Pd.dismissProgress();

        new Dialog(Gotv_Activity.this, s); // insert response into dialog
      }
    }
  }

  private class Gotv_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Gotv_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Gotv_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      //Log.d(Tag,"Result @ postExecute of airtimetop is: "+result);
      Pd.dismissProgress(); // close progress dialog after thread is done
      new Dialog(Gotv_Activity.this, result);
      mCustomerId.setText("");
      mPin.setText("");

    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

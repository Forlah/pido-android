package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.StringTokenizer;

/**
 * Created by sp_developer on 2/24/16.
 */
public class KanoElectricity_Activity extends BaseActivity {

  private EditText mMeterNo, mAmount, mPin;
  private String MeterNo, Amount, Pin, pidoMobile, PaymentType;
  private Spinner mPaymentType;
  private Button Kano_Btn;
  private String ProductId;
  private Kano_Task _task;
  private GetCustomerName_Task _Task;
  String kano_url = null;
  private ConnectionChecker connCheck = new ConnectionChecker(KanoElectricity_Activity.this);

  private void Kano_Confirmation_dialog(String name, String charge, String minPurchase,
      String arrears, String tariffCode, String tariffRate, String bUnit) {

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.kano_confirmation, null);

    TextView mCustomer_name = (TextView) v.findViewById(R.id.kano_cus_name_txt);
    TextView _mMeterNo_txt = (TextView) v.findViewById(R.id.kano_meter_no_txt);
    TextView _type_txt = (TextView) v.findViewById(R.id.kano_type);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.kano_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.kano_trans_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.kano_total_txt);
    TextView mMinPurchase = (TextView) v.findViewById(R.id.kano_min_purchase);
    TextView mArrears = (TextView) v.findViewById(R.id.kano_arrears);
    TextView mTariffCode = (TextView) v.findViewById(R.id.kano_tariff_code);
    TextView mTariffRate = (TextView) v.findViewById(R.id.kano_tariff_rate);

    Encryption encrypt = new Encryption();

    try {
      kano_url = "method=kedcopayment&customerid=" + MeterNo
          + "&amount=" + Amount + "&businessUnit=" + URLEncoder.encode(bUnit, "UTF-8")
          + "&metertype=" + PaymentType
          + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
          + "&pin=" + encrypt.Base64Encoder(Pin);
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    mCustomer_name.setText(name);
    _mMeterNo_txt.setText(MeterNo);
    _type_txt.setText(PaymentType);
    mMinPurchase.setText(minPurchase);
    mArrears.setText(arrears);
    mTariffCode.setText(tariffCode);
    mTariffRate.setText(tariffRate);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = Integer.parseInt(charge) + Integer.parseInt(Amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(KanoElectricity_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {
          _task = new Kano_Task();
          _task.execute(kano_url);
        } else {
          Toast.makeText(KanoElectricity_Activity.this, "No Connection To The Internet",
              Toast.LENGTH_LONG).show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_kano);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        KanoElectricity_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mMeterNo = (EditText) findViewById(R.id.kano_meter);
    mAmount = (EditText) findViewById(R.id.kano_amount);
    mPin = (EditText) findViewById(R.id.kano_pin);

    mPaymentType = (Spinner) findViewById(R.id.kano_spinner);
    mPaymentType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        PaymentType = parent.getSelectedItem().toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    Kano_Btn = (Button) findViewById(R.id.btn_kano);

    Kano_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        MeterNo = mMeterNo.getText().toString();
        Amount = mAmount.getText().toString();
        Pin = mPin.getText().toString();

        if (MeterNo.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mMeterNo.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (MeterNo.length() == 0) {
          mMeterNo.setError("This Field Is Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          Encryption encrypt = new Encryption();
          String phcn_charge_uri =
              "method=kedcoenquiry&meterType=" + PaymentType + "&customerid=" + MeterNo;
          //Log.d(Tag, "P2P charge uri is : " + P2P_charge_uri);
          if (connCheck.isAvailable()) {

            _Task = new GetCustomerName_Task();
            _Task.execute(phcn_charge_uri);
          } else {
            Toast.makeText(KanoElectricity_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }
      }

    });

  }

  private class GetCustomerName_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(KanoElectricity_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        System.out.println("kaduna url = " + params[0]);
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(KanoElectricity_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      String charge, Customer_Name, minPurchace, customerArrear, tariffcode, tariffRate, businessUnit;

      System.out.println("server kano response = " + s);

      if (s.contains("|")) {
        Pd.dismissProgress();
        // if the result contain "|" sign means valid response
        StringTokenizer tokens = new StringTokenizer(s, "|");
        charge = tokens.nextToken().trim().toString();
        Customer_Name = tokens.nextToken().trim().toString();
        minPurchace = tokens.nextToken().trim().toString();
        customerArrear = tokens.nextToken().trim().toString();
        tariffcode = tokens.nextToken().trim().toString();
        tariffRate = tokens.nextToken().trim().toString();
        businessUnit = tokens.nextToken().trim().toString();

        if (Customer_Name.equals("200")) {
          Customer_Name = "Unavailable at the moment";
        }

        // display a dialog for the user to verify the transaction
        Kano_Confirmation_dialog(Customer_Name, charge, minPurchace, customerArrear, tariffcode,
            tariffRate, businessUnit);

      }

//            else if(s.startsWith("Name enquiry currently not available")){
//
//                Pd.dismissProgress();
//                new Dialog().error_Dialog(KanoElectricity_Activity.this, "Customer ID Not Available", "Failed");
//            }

      else {
        Pd.dismissProgress();
        // an error occured on the service
        // display the dialog with the errors
        new Dialog(KanoElectricity_Activity.this, s);
      }
    }
  }

  private class Kano_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(KanoElectricity_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(KanoElectricity_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(KanoElectricity_Activity.this, result); // insert response into dialog
      mMeterNo.setText("");
      mAmount.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

/**
 * Created by FOLASHELE on 8/7/2015.
 */
public class AirTime_Topup extends BaseActivity {

  private Spinner airTime, denominationSpinner;
  private EditText mPhoneNo, mPin;
  private String PhoneNo, Pin;
  private Button Send_btn;
  private String uri = " ";
  private String network_Name = " ";
  String amount_value = " ";
  ArrayAdapter<String> adapter;
  private static final String Tag = "airtime";
  private String pidoMobile;
  private ImageView imgPickUpContact;
  private ConnectionChecker connCheck = new ConnectionChecker(AirTime_Topup.this);
  private AirtimeTopup_Task _task;

  // used for updating the dependent spinner i.e denominations
  private void UpdateSpinner(String[] spinner_items) {

    adapter = new ArrayAdapter<>(AirTime_Topup.this, android.R.layout.simple_spinner_item,
        spinner_items);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    adapter.notifyDataSetChanged();
    denominationSpinner.setAdapter(adapter);

  }

  private void Airtimetopup_Confirmation_Dialog() {
    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.airtime_confirmation, null);

    TextView mRecptNo = (TextView) v.findViewById(R.id.airtime_recp_txt);
    TextView mNetwork_txt_view = (TextView) v.findViewById(R.id.airtime_ntwrk_txt);
    TextView airtime_amount = (TextView) v.findViewById(R.id.airtime_amn_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.airtime_total_txt);

    mRecptNo.setText(PhoneNo);
    mNetwork_txt_view.setText(network_Name);
    airtime_amount.setText("₦" + amount_value);

    int mTotal = Integer.parseInt(amount_value);
    mtotal.setText("₦" + mTotal);

    Encryption encrypt = new Encryption();

    uri = "method=airtime&msisdn=" + encrypt.Base64Encoder(pidoMobile) + "&pin=" + encrypt
        .Base64Encoder(Pin)
        + "&network=" + network_Name + "&amount=" + amount_value
        + "&recipientmobile=" + encrypt.Base64Encoder(PhoneNo);

    Log.d(Tag, "Airtime Topup uri is: " + uri);

    AlertDialog.Builder builder = new AlertDialog.Builder(AirTime_Topup.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connCheck.isAvailable()) {

          _task = new AirtimeTopup_Task();
          _task.execute(uri);
        } else {
          Toast.makeText(AirTime_Topup.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }

      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  private String getPhoneNumber(Uri paramUri) {
    String id = "";
    String no = "";
    Cursor cursor = getContentResolver().query(paramUri, null, null, null, null);

    while (cursor.moveToNext()) {
      id = cursor.getString(cursor.getColumnIndex("_id"));
      if ("1".equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("has_phone_number")))) {
        Cursor cursorNo = getContentResolver()
            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + id,
                null, null);
        while (cursorNo.moveToNext()) {
          if (cursorNo.getInt(cursorNo.getColumnIndex("data2")) == 2) {
            no = no.concat(cursorNo.getString(cursorNo.getColumnIndex("data1")));
            break;
          }
        }
        cursorNo.close();
      }
    }
    cursor.close();
    return no;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.airtime_topup);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(AirTime_Topup.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    String cReceipientNo = getIntent().getStringExtra(
        "From Contact Activity to perform Airtime topup");// retrieve data from the intent

    airTime = (Spinner) findViewById(R.id.airtime_spinner);
    airTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        network_Name = parent.getSelectedItem().toString();

        if (network_Name.equals("GLO")) {
          String[] items = {"100", "150", "200", "300", "500", "1000", "3000"};
          UpdateSpinner(items);

        } else if (network_Name.equals("9Mobile")) {
          String[] items = {"50", "100", "200", "500", "1000"};

          UpdateSpinner(items);

        } else if (network_Name.equals("MTN")) {
          String[] items = {"100", "200", "400", "750", "1500", "3000"};
          UpdateSpinner(items);
        } else {
          String[] items = {"100", "200", "500", "1000"};
          UpdateSpinner(items);

        }

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }

    });

    denominationSpinner = (Spinner) findViewById(R.id.denomination_spinner);

    denominationSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        amount_value = parent.getSelectedItem().toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    imgPickUpContact = (ImageView) findViewById(R.id.PickUpContact);

    imgPickUpContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent localIntent = new Intent("android.intent.action.PICK",
            ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(localIntent, 1);
      }
    });

    mPhoneNo = (EditText) findViewById(R.id.airtime_recipient_no);
    mPin = (EditText) findViewById(R.id.airtime_pin);

    if (cReceipientNo != " ") {
      mPhoneNo.setText(cReceipientNo);
    }

    Send_btn = (Button) findViewById(R.id.send_airtime);
    Send_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        PhoneNo = mPhoneNo.getText().toString();
        Pin = mPin.getText().toString();

        if (PhoneNo.length() == 0 && Pin.length() == 0) {
          mPhoneNo.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (PhoneNo.length() == 0) {
          mPhoneNo.setError("This Field Is Required");

        } else if (PhoneNo.length() < 11) {
          mPhoneNo.setError("11 Digits Phone Number Required");

        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");

        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Pin Required");
        } else {
          // dialog for airtime topup transaction details
          Airtimetopup_Confirmation_Dialog();

        }

      }
    });

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent paramIntent) {
    // TODO Auto-generated method stub
    super.onActivityResult(requestCode, resultCode, paramIntent);
    if (resultCode == RESULT_OK) {
      String str = getPhoneNumber(paramIntent.getData());
      if (str.trim().length() > 0) {

        String result = str.replace(" ", "");
        if (result.contains("+234")) {
          String out = result.replace("+234", "0");
          mPhoneNo.setText(out.trim());

        } else if (result.contains("-")) {
          String out = result.replace("-", "");
          mPhoneNo.setText(out.trim());
        } else {
          mPhoneNo.setText(result);
        }
      }
    } else {
      Toast.makeText(this, "Phone Number Not Found...", Toast.LENGTH_SHORT).show();
    }
  }

  private class AirtimeTopup_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(AirTime_Topup.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(AirTime_Topup.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      //Log.d(Tag,"Result @ postExecute of airtimetop is: "+result);
      Pd.dismissProgress();
      new Dialog(AirTime_Topup.this, result);
      mPhoneNo.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }
}

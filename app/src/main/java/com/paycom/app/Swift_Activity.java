package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class Swift_Activity extends BaseActivity {

  private EditText mCustomerId, mAmount, mPin;
  private String CustomerId, Amount, Pin, pidoMobile;
  private Button Swift_Btn;
  private static final int charge = 100;
  private static final int ProductID = 37;
  private Swift_Task _task;
  private ConnectionChecker connChecker = new ConnectionChecker(Swift_Activity.this);

  private void swift_Confirmation_dialog() {

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.swift_confirmation, null);

    TextView mCustomer_id = (TextView) v.findViewById(R.id.swift_cus_id_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.swift_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.swift_trans_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.swift_total_txt);

    Encryption encrypt = new Encryption();

    final String swift_uri = "method=billspayment&amount=" + Amount
        + "&customerid=" + encrypt.Base64Encoder(CustomerId)
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&productid=" + ProductID + "&charge=" + charge;

    mCustomer_id.setText(CustomerId);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = charge + Integer.parseInt(Amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(Swift_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {
        if (connChecker.isAvailable()) {

          _task = new Swift_Task();
          _task.execute(swift_uri);
        } else {
          Toast.makeText(Swift_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_swift);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Swift_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.swift_cus_id);
    mAmount = (EditText) findViewById(R.id.swift_amount);
    mPin = (EditText) findViewById(R.id.swift_pin);
    Swift_Btn = (Button) findViewById(R.id.swift_btn);

    Swift_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        CustomerId = mCustomerId.getText().toString();
        Amount = mAmount.getText().toString();
        Pin = mPin.getText().toString();

        if (CustomerId.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          swift_Confirmation_dialog();
        }
      }
    });

  }

  private class Swift_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Swift_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Swift_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(Swift_Activity.this, result); // insert response into dialog
      mCustomerId.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

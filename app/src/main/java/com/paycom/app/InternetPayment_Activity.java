package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class InternetPayment_Activity extends BaseActivity {

  String[] Bills = {"IPNX", "Swift"};

  RecyclerView mInternetPay;
  InternetPay_RecyclerAdapter mInternetPay_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_internet_payment);

    mInternetPay_Adapter = new InternetPay_RecyclerAdapter(getApplicationContext(), Bills);
    mInternetPay = (RecyclerView) findViewById(R.id.internet_pay_list);
    mInternetPay.setHasFixedSize(true);
    mInternetPay.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    // mRecyclerView.setLayoutManager(mLayoutManager);
    mInternetPay.setLayoutManager(mLayoutManager);
    mInternetPay.setAdapter(mInternetPay_Adapter);
  }

  private class InternetPay_RecyclerAdapter extends
      RecyclerView.Adapter<InternetPay_RecyclerAdapter.DataObjectHolder> {

    String[] bills;
    Context mContext;

    private InternetPay_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      bills = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView _name;
      ImageView _Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;

        _name = (TextView) itemView.findViewById(R.id.internet_type);
        _Image = (ImageView) itemView.findViewById(R.id.internet_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.internet_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
      // final int _position = position;
      holder._name.setText(bills[position]);

      if (bills[position].equals("IPNX")) {

        // holder.bill_name.setText(bills[position]);
        holder._Image.setImageResource(R.mipmap.ipnx);
      } else {
        holder._Image.setImageResource(R.mipmap.swift);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if ("IPNX".equals(bills[position])) {

            Intent i = new Intent(InternetPayment_Activity.this, Ipnx_Activity.class);
            startActivity(i);

          } else {
            Intent i = new Intent(InternetPayment_Activity.this, Swift_Activity.class);
            startActivity(i);
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return bills.length;
    }

  }

}

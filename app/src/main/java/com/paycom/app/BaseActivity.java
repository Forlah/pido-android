package com.paycom.app;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;

/**
 * Created by sp_developer on 11/28/15.
 */

/* This class is used as a super class for all activities in order to mitigate the need
   for overriding the onPause state . The onPause state is to cause all activities to automatically shutdown
   after a certain period
    */

public class BaseActivity extends AppCompatActivity {

  private static BaseActivity lastPausedActivity = null;

  @Override
  protected void onPause() {
    super.onPause();

    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {
        setPausedActivity();
      }
    }, 200000);

  }

  public void setPausedActivity() {
    lastPausedActivity = this;
  }

  @Override
  protected void onResume() {

    super.onResume();
    if (this == lastPausedActivity) {
      Intent intent = new Intent(this, MainActivity.class);
      intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
      startActivity(intent);
      finish();

    }
    lastPausedActivity = null;
  }

}

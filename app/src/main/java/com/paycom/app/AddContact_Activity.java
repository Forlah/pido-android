package com.paycom.app;

import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;
import com.paycom.DBhelper.DatabaseHandler;
import com.paycom.model.Contact;

public class AddContact_Activity extends BaseActivity {

  private EditText mFirstname, mSurname, mPhoneNO, mAccountNo;
  private String Firstname, Surname, PhoneNO, AccountNO, Bank;
  private Spinner mBank;
  private Button AddContact_btn;
  private String firstname, surname, phone, acc, bank;
  private int ID;
  private ImageView imgPickUpContact;
  ArrayAdapter<String> adapter;

  private String[] naija_banks = {"Access", "Citi", "Diamond", "Ecobank", "Enterprise",
      "FCMB", "Fidelity", "FirstBank", "GTB",
      "Heritage", "KeyStone", "MainStreet",
      "Skye", "Stanbic", "Standard", "Sterling",
      "UBA", "Union", "Unity", "Wema", "Zenith"};

  private String getPhoneNumber(Uri paramUri) {
    String id = "";
    String no = "";
    Cursor cursor = getContentResolver().query(paramUri, null, null, null, null);

    while (cursor.moveToNext()) {
      id = cursor.getString(cursor.getColumnIndex("_id"));
      if ("1".equalsIgnoreCase(cursor.getString(cursor.getColumnIndex("has_phone_number")))) {
        Cursor cursorNo = getContentResolver()
            .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, "contact_id = " + id,
                null, null);
        while (cursorNo.moveToNext()) {
          if (cursorNo.getInt(cursorNo.getColumnIndex("data2")) == 2) {
            no = no.concat(cursorNo.getString(cursorNo.getColumnIndex("data1")));
            break;
          }
        }
        cursorNo.close();
      }
    }
    cursor.close();
    return no;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_add_contact);

    mFirstname = (EditText) findViewById(R.id.add_contact_name);
    mSurname = (EditText) findViewById(R.id.add_contact_surname);
    mPhoneNO = (EditText) findViewById(R.id.add_contact_phoneNO);
    mAccountNo = (EditText) findViewById(R.id.add_contact_accNO);
    AddContact_btn = (Button) findViewById(R.id.add_contact_button);
    mBank = (Spinner) findViewById(R.id.add_contact_spinner);
    imgPickUpContact = (ImageView) findViewById(R.id.PickUpContact);

    adapter = new ArrayAdapter<>(AddContact_Activity.this, android.R.layout.simple_spinner_item,
        naija_banks);
    adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
    adapter.notifyDataSetChanged();
    mBank.setAdapter(adapter);

    ID = getIntent().getIntExtra("contact row id", 0);
    firstname = getIntent().getStringExtra("Edit firstname");
    surname = getIntent().getStringExtra("Edit surname");
    phone = getIntent().getStringExtra("Edit phone number");
    acc = getIntent().getStringExtra("Edit acc no");
    bank = getIntent().getStringExtra("Edit bank");

    if (ID != 0 && firstname != null && surname != null && phone != null && acc != null
        && bank != null) {

      setTitle("Edit Contact");
      AddContact_btn.setText("Update Contact");

      mFirstname.setText(firstname);
      mSurname.setText(surname);
      mPhoneNO.setText(phone);
      mAccountNo.setText(acc);
      mBank.setSelection(adapter.getPosition(bank));

    }

    imgPickUpContact.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View view) {
        Intent localIntent = new Intent("android.intent.action.PICK",
            ContactsContract.Contacts.CONTENT_URI);
        startActivityForResult(localIntent, 1);
      }
    });

    mBank.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Bank = parent.getSelectedItem().toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    AddContact_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Firstname = mFirstname.getText().toString();
        Surname = mSurname.getText().toString();
        PhoneNO = mPhoneNO.getText().toString();
        AccountNO = mAccountNo.getText().toString();

        if (Firstname.length() == 0 && Surname.length() == 0 && PhoneNO.length() == 0
            && AccountNO.length() == 0) {
          mFirstname.setError("This Field is Required");
          mSurname.setError("This Field is Required");
          mPhoneNO.setError("This Field is Required");
          mAccountNo.setError("This Field is Required");
        } else if (Firstname.length() == 0) {
          mFirstname.setError("This Field is Required");
        } else if (Surname.length() == 0) {
          mSurname.setError("This Field is Required");
        } else if (PhoneNO.length() == 0) {
          mPhoneNO.setError("This Field is Required");
        } else if (PhoneNO.length() < 11) {
          mPhoneNO.setError("11 Digit Number Required");
        } else if (AccountNO.length() == 0) {
          mAccountNo.setError("This Field is Required");
        } else if (AccountNO.length() < 10) {
          mAccountNo.setError("10 Digit Account Number Required");
        } else {
          DatabaseHandler dbHandler = new DatabaseHandler(AddContact_Activity.this);
          if (AddContact_btn.getText().toString().equalsIgnoreCase("Update Contact")) {

            Contact contact = new Contact(Firstname, Surname, Bank, PhoneNO, AccountNO);
            dbHandler.updateContact(contact, ID);

            Intent i = new Intent(AddContact_Activity.this, Contact_Activity.class);
            startActivity(i);
            finish();
          } else {
            Contact contact = new Contact(Firstname, Surname, Bank, PhoneNO, AccountNO);

            dbHandler.addContact(contact);

            mFirstname.setText("");
            mSurname.setText("");
            mPhoneNO.setText("");
            mAccountNo.setText("");

            Intent i = new Intent(AddContact_Activity.this, Contact_Activity.class);
            startActivity(i);
            finish();
          }
        }

      }
    });

  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent paramIntent) {
    // TODO Auto-generated method stub
    super.onActivityResult(requestCode, resultCode, paramIntent);
    if (resultCode == RESULT_OK) {
      String str = getPhoneNumber(paramIntent.getData());
      if (str.trim().length() > 0) {

        String result = str.replace(" ", "");
        if (result.contains("+234")) {
          String out = result.replace("+234", "0");
          mPhoneNO.setText(out.trim());
        } else if (result.contains("-")) {
          String out = result.replace("-", "");
          mPhoneNO.setText(out.trim());
        } else {
          mPhoneNO.setText(result);
        }
      }
    } else {
      Toast.makeText(this, "Phone Number Not Found...", Toast.LENGTH_SHORT).show();
    }
  }

}

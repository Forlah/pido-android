package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class Ipnx_Activity extends BaseActivity {

  private EditText mCustomerId, mPin;
  private String CustomerId, Pin, Plan, pidoMobile;
  private Button IPNX_Btn;
  private Spinner mOption;
  private static final int charge = 100;
  private int Amount, ProductID;
  private ConnectionChecker connChecker = new ConnectionChecker(Ipnx_Activity.this);
  private Ipnx_Task _task;

  private void Ipnx_Confirmation_dialog() {

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.ipnx_confirmation, null);

    TextView mCustomer_id = (TextView) v.findViewById(R.id.ipnx_cus_id_txt);
    TextView _mPlan = (TextView) v.findViewById(R.id.ipnx_plan_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.ipnx_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.ipnx_trans_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.ipnx_total_txt);

    Encryption encrypt = new Encryption();

    final String ipnx_uri = "method=billspayment&amount=" + Amount
        + "&customerid=" + encrypt.Base64Encoder(CustomerId)
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&productid=" + ProductID + "&charge=" + charge;

    mCustomer_id.setText(CustomerId);
    _mPlan.setText(Plan);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = charge + Amount;
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(Ipnx_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        if (connChecker.isAvailable()) {

          _task = new Ipnx_Task();
          _task.execute(ipnx_uri);
        } else {
          Toast.makeText(Ipnx_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_ipnx);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(Ipnx_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mCustomerId = (EditText) findViewById(R.id.ipnx_cus_id);
    mPin = (EditText) findViewById(R.id.ipnx_pin);
    mOption = (Spinner) findViewById(R.id.ipnx_spinner);
    mOption.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Plan = parent.getSelectedItem().toString();
        if (Plan.equals("Family IPNX Plus")) {

          Amount = 9450;
          ProductID = 40;
        } else if (Plan.equals("Home Office IPNX")) {

          Amount = 15750;
          ProductID = 41;

        } else if (Plan.equals("Professional IPNX")) {

          Amount = 21000;
          ProductID = 42;

        } else if (Plan.equals("Small Business Plus")) {
          Amount = 47250;
          ProductID = 44;
        } else if (Plan.equals("Small Business")) {
          Amount = 28350;
          ProductID = 43;
        } else {
          Amount = 6300;
          ProductID = 39;
        }

      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });
    IPNX_Btn = (Button) findViewById(R.id.btn_ipnx);

    IPNX_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        CustomerId = mCustomerId.getText().toString();
        Pin = mPin.getText().toString();

        if (CustomerId.length() == 0 && Pin.length() == 0) {
          mCustomerId.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (CustomerId.length() == 0) {
          mCustomerId.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          Ipnx_Confirmation_dialog();
        }
      }

    });

  }

  private class Ipnx_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Ipnx_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Ipnx_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(Ipnx_Activity.this, result); // insert response into dialog
      mCustomerId.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

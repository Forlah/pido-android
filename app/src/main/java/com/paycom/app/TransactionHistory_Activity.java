package com.paycom.app;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.adapters.HistoryRecyclerAdapter;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.util.ArrayList;

public class TransactionHistory_Activity extends BaseActivity {

  private RecyclerView mRecyclerView;

  private Button Send_Btn;
  private EditText mPIN;
  private String PIN, pidoMobile;

  private ConnectionChecker connCheck = new ConnectionChecker(TransactionHistory_Activity.this);

  private getTransactionHistory_Task _task;

  HistoryRecyclerAdapter historyAdapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        TransactionHistory_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    setContentView(R.layout.activity_transaction_history);
    mRecyclerView = (RecyclerView) findViewById(R.id.history);

    mRecyclerView.setHasFixedSize(true);

    mRecyclerView.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(TransactionHistory_Activity.this);
    // mRecyclerView.setLayoutManager(mLayoutManager);
    mRecyclerView.setLayoutManager(mLayoutManager);

    mPIN = (EditText) findViewById(R.id.history_pin);
    Send_Btn = (Button) findViewById(R.id.history_btn);

    Send_Btn.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {

        PIN = mPIN.getText().toString();
        if (PIN.length() == 0) {
          mPIN.setError("4 Digit PIN required");
        } else {
          Encryption encrypt = new Encryption();

          String history_uri =
              "method=transactionhistory&MobileNumber=" + pidoMobile.substring(1) + "&UserPassword="
                  + encrypt.Base64Encoder(PIN);

          if (connCheck.isAvailable()) {

            _task = new getTransactionHistory_Task();
            _task.execute(history_uri);
          } else {
            Toast.makeText(TransactionHistory_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }

          // Dismiss keyboard
          InputMethodManager imm = (InputMethodManager) getSystemService(
              Context.INPUT_METHOD_SERVICE);
          imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

        }

      }
    });

  }

//    private class History_Adapter  extends ArrayAdapter<String> {
//        ArrayList<String> Items;
//        // Context mContext;
//        public History_Adapter( ArrayList<String> items ){
//            super(TransactionHistory_Activity.this, 0, items);
//            Items = items;
//            //  mContext = c;
//        }
//
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//            String resultString = "" ;
//            //if there is no view available , inflate one
//            if(convertView == null){
//                convertView = getLayoutInflater().inflate(R.layout.history_, null);
//            }
//            resultString = Items.get(position);
//
//            System.out.println(Items.get(position));
//
//            if(resultString.trim().contains("|")) {
//
//                try{
//
//                    StringTokenizer tokens = new StringTokenizer(resultString.trim(),"|");
//
//                    String acc = tokens.nextToken().trim();
//                    String amnt = tokens.nextToken().trim();
//                    String date = tokens.nextToken().trim();
//                    String Trans_type = tokens.nextToken().trim();
//                    String type = tokens.nextToken().trim();
//                    String Description = tokens.nextToken().trim();
//                    String Trans_Id = tokens.nextToken().trim();
//
//
//                    TextView mAcc = (TextView) convertView.findViewById(R.id.acc);
//                    mAcc.setText(acc);
//
//                    TextView mAmnt = (TextView) convertView.findViewById(R.id.amount);
//                    mAmnt.setText(amnt);
//
//                    TextView mDate = (TextView) convertView.findViewById(R.id.date);
//                    mDate.setText(date);
//
//                    TextView mTrans = (TextView) convertView.findViewById(R.id.trans_type);
//                    mTrans.setText(Trans_type);
//
//                    TextView mType = (TextView) convertView.findViewById(R.id.type);
//                    mType.setText(type);
//
//                    TextView mDesc = (TextView) convertView.findViewById(R.id.history_desc);
//                    mDesc.setText(Description);
//
//                    TextView mTransId = (TextView) convertView.findViewById(R.id.trans_id);
//                    mTransId.setText(Trans_Id);
//
//
//
//                } catch (NoSuchElementException ne){
//
//                }
//
//            }
//
//
//
//            return convertView;
//        }
//    }

  private class getTransactionHistory_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;
    String output;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(TransactionHistory_Activity.this, "Please wait a moment");
      Pd.showProgress_Title();

    }

    @Override
    protected String doInBackground(String... params) {
      try {
        output = new ConnectionToService(params[0])
            .download_secure_URL(TransactionHistory_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "History currently not available..";
      }
    }

    @Override
    protected void onPostExecute(String strings) {
      super.onPostExecute(strings);
      System.out.println("History Response = " + strings);
      String[] outarray;
      ArrayList<String> arrayList = new ArrayList<>();

      if (strings.trim().contains(",")) {
        Pd.dismissProgress();
        outarray = strings.split(",");

        for (int i = 0; i < outarray.length; i++) {
          arrayList.add(i, outarray[i]);
        }

        historyAdapter = new HistoryRecyclerAdapter(TransactionHistory_Activity.this, arrayList);
        mRecyclerView.setAdapter(historyAdapter);

      } else {
        Pd.dismissProgress();

        new Dialog().error_Dialog(TransactionHistory_Activity.this, strings, "Failed");

      }

    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

  }

}

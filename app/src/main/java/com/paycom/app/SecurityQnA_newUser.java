package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.model.Data;
import com.paycom.model.DataLab;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;

public class SecurityQnA_newUser extends AppCompatActivity {

  private EditText msQuestion, msAnswer, msAnswer_confirmation;
  private String sQuestion, sAnswer, sAnswer_confirmation;
  private Button continue_btn;
  private String surname, firstname, Mobile_no, Password, IMEI, Token, Email, Gender;
  private static final String App_Version = "3.0";
  private ConnectionChecker connChecker = new ConnectionChecker(SecurityQnA_newUser.this);
  private getToken_Task _task;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_security_qn_a_new_user);

    msQuestion = (EditText) findViewById(R.id.sQuest_new);
    msAnswer = (EditText) findViewById(R.id.sAnswer_txt);
    msAnswer_confirmation = (EditText) findViewById(R.id.sAnswer_confrmtn);

    continue_btn = (Button) findViewById(R.id.continue_btn_QnA_new);
    continue_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sQuestion = msQuestion.getText().toString();
        sAnswer = msAnswer.getText().toString();
        sAnswer_confirmation = msAnswer_confirmation.getText().toString();

        if (sQuestion.length() == 0 && sAnswer.length() == 0
            && sAnswer_confirmation.length() == 0) {
          msQuestion.setError("This Field Is Required");
          msAnswer.setError("This Field Is Required");
          msAnswer_confirmation.setError("This Field Is Required");
        } else if (sQuestion.length() == 0) {
          msQuestion.setError("This Field Is Required");
        } else if (sAnswer.length() == 0) {
          msAnswer.setError("This Field Is Required");
        } else if (sAnswer_confirmation.length() == 0) {
          msAnswer_confirmation.setError("This Field Is Required");
        } else if (sAnswer_confirmation.equals(sAnswer) == false) {
          msAnswer_confirmation.setError("Answers Do Not Match");
        } else {
          Data c = DataLab.get(SecurityQnA_newUser.this).getDetails();
          surname = c.getSurname();
          firstname = c.getFirstname();
          Mobile_no = c.getMobile_no();
          Password = c.getPassword();
          IMEI = c.getImei();
          Token = c.getToken();
          Email = c.getEmail();
          Gender = c.getGender();

          Encryption encrypt = new Encryption();

          String reg_user_uri =
              "method=register1&firstname=" + firstname.trim() + "&surname=" + surname.trim()
                  + "&msisdn=" + encrypt.Base64Encoder(Mobile_no)
                  + "&email=" + Email + "&password=" + encrypt.Base64Encoder(Password.trim())
                  + "&version=" + App_Version
                  + "&device=" + "android" + "&haveaccount=" + "yes" + "&autcode=" + encrypt
                  .Base64Encoder(IMEI)
                  + "&securityquestion=" + encrypt.Base64Encoder(sQuestion.trim())
                  + "&securityanswer=" + encrypt.Base64Encoder(sAnswer.trim());

          if (connChecker.isAvailable()) {

            _task = new getToken_Task();
            _task.execute(reg_user_uri);
          } else {
            Toast.makeText(SecurityQnA_newUser.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }

      }
    });
  }

  private class getToken_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;
    String output;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(SecurityQnA_newUser.this, "Please wait a moment");
      Pd.showProgress_Title();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(SecurityQnA_newUser.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.startsWith("5000")) {

        Pd.dismissProgress(); // dismiss progress dialog
        new Dialog(SecurityQnA_newUser.this,
            "This phone number has been registered by a subscriber");
        finish(); // kill this activity

      } else if (result.startsWith("6000")) {

        Pd.dismissProgress(); // dismiss progress dialog

        // display a success dialog before proceeding
        AlertDialog.Builder builder = new AlertDialog.Builder(SecurityQnA_newUser.this);
        builder.setTitle("RESPONSE");
        builder.setMessage("Registration Successful. Your token will be sent shortly..");
        builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
          @Override
          public void onClick(DialogInterface dialog, int which) {
            Intent i = new Intent(SecurityQnA_newUser.this, Token_Activity.class);
            startActivity(i);
            finish(); // kill this activity
          }
        });
        builder.show();

      } else if (result.startsWith("7000")) {
        Pd.dismissProgress(); // dismiss progress dialog
        new Dialog(SecurityQnA_newUser.this, "Server Internal Error");
        finish(); // kill this activity
      } else {

        Pd.dismissProgress();
        new Dialog(SecurityQnA_newUser.this, result);

      }
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

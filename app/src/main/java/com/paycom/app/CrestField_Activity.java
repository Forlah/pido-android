package com.paycom.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.Sch_Fees;

public class CrestField_Activity extends BaseActivity {

  private EditText mPin;
  private String Pin, pidoMobile;
  private Button CrestField_Btn;
  private ConnectionChecker connChecker = new ConnectionChecker(CrestField_Activity.this);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_crest_field);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        CrestField_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mPin = (EditText) findViewById(R.id.crestfield_pin);

    CrestField_Btn = (Button) findViewById(R.id.btn_crestfield);
    CrestField_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Pin = mPin.getText().toString();
        if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          String uri =
              "method=schoolformpayment&commandtype=" + "crestfield" + "&productid=" + "134"
                  + "&schoolid=" + " " +
                  "&customerno=" + pidoMobile + "&mobileno=" + pidoMobile + "&pin=" + Pin;

          if (connChecker.isAvailable()) {

            new Sch_Fees(CrestField_Activity.this, uri);
          } else {
            Toast.makeText(CrestField_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
          mPin.setText("");
        }
      }
    });

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();

  }

}

package com.paycom.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONObject;

public class Cancel_Token_Activity extends BaseActivity {

  private EditText mTransRef;
  private String TransRef;
  private Button CancelToken_Btn;
  private String safeURl;

  private ConnectionChecker connChecker = new ConnectionChecker(Cancel_Token_Activity.this);

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_cancel__token);

    mTransRef = (EditText) findViewById(R.id.cancel_token_transref);

    CancelToken_Btn = (Button) findViewById(R.id.btn_cancel_token);
    CancelToken_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        TransRef = mTransRef.getText().toString().trim();

        if (TextUtils.isEmpty(mTransRef.getText())) {
          mTransRef.setError("This Field Is Required");
        } else {

          try {
            safeURl = "/cancelToken?transRef=" + URLEncoder.encode(TransRef, "UTF-8");

            if (connChecker.isAvailable()) {

              new CancelTask().execute();
            } else {

              Toast.makeText(Cancel_Token_Activity.this, "No Connection To The Internet",
                  Toast.LENGTH_LONG).show();
            }

          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }
        }
      }
    });
  }

  private class CancelTask extends AsyncTask<Void, Void, String> {

    Progressdialog progressdialog;
    String response;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      progressdialog = new Progressdialog(Cancel_Token_Activity.this, "Please Wait..");
      progressdialog.showProgress();
    }

    @Override
    protected String doInBackground(Void... params) {
      try {

        response = new ConnectionToService().POST_GET_URL(safeURl, "POST");

      } catch (Exception e) {
        e.printStackTrace();

        response = "Unable to connect to the service at the moment.Please try again later";
      }

      return response;
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);

      progressdialog.dismissProgress();

      try {

        JSONObject jsonObject = new JSONObject(s);
        String code = jsonObject.get("code").toString();
        String desc = jsonObject.get("description").toString();

        if (code.equals("00")) {

          new Dialog(Cancel_Token_Activity.this, desc); // insert response into dialog

          mTransRef.setText("");

        }

      } catch (Exception e) {
        e.printStackTrace();

        new Dialog(Cancel_Token_Activity.this,
            "Operation Not Successful"); // insert response into dialog

        mTransRef.setText("");
      }

    }
  }
}

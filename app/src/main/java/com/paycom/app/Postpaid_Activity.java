package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Encryption;
import com.paycom.utitlities.Progressdialog;
import java.util.StringTokenizer;

public class Postpaid_Activity extends BaseActivity {

  private EditText mMeterNo, mAmount, mPin;
  private String MeterNo, Amount, Pin, pidoMobile;
  private Button PostPaid_Btn;
  private static final String ProductId = "33";
  private PostPaid_Task _task;
  private GetCustomerName_Task _Task;

  private void PostPaid_Confirmation_dialog(String name, String charge) {

    // since the transaction detials of all will be equal , inflate this for all PHCN transaction details dialog

    LayoutInflater inflater = getLayoutInflater();
    View v = inflater.inflate(R.layout.eko_confirmation, null);

    TextView mCustomer_name = (TextView) v.findViewById(R.id.eko_cus_name_txt);
    TextView _mMeterNo_txt = (TextView) v.findViewById(R.id.eko_meter_no_txt);
    TextView mAmount_txt_view = (TextView) v.findViewById(R.id.eko_amount_txt);
    TextView mTrans_charge = (TextView) v.findViewById(R.id.eko_trans_txt);
    TextView mtotal = (TextView) v.findViewById(R.id.eko_total_txt);

    Encryption encrypt = new Encryption();

    final String postpaid_url = "method=payphcn&meterno=" + MeterNo
        + "&amount=" + Amount
        + "&mobile=" + encrypt.Base64Encoder(pidoMobile)
        + "&pin=" + encrypt.Base64Encoder(Pin)
        + "&productid=" + ProductId + "&charge=" + charge;

    mCustomer_name.setText(name);
    mAmount_txt_view.setText(Amount);
    mAmount_txt_view.setText("₦" + Amount);
    mTrans_charge.setText("₦" + charge);

    int mTotal = Integer.parseInt(charge) + Integer.parseInt(Amount);
    mtotal.setText("₦" + mTotal);

    AlertDialog.Builder builder = new AlertDialog.Builder(Postpaid_Activity.this);
    builder.setView(v);
    builder.setPositiveButton("Proceed?", new DialogInterface.OnClickListener() {
      @Override
      public void onClick(DialogInterface dialog, int which) {

        _task = new PostPaid_Task();
        _task.execute(postpaid_url);
      }
    });
    builder.setNegativeButton("Dismiss", null);
    builder.show();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_postpaid);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        Postpaid_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mMeterNo = (EditText) findViewById(R.id.postpaid_meter_no);
    mAmount = (EditText) findViewById(R.id.postpaid_amount);
    mPin = (EditText) findViewById(R.id.postpaid_pin);

    PostPaid_Btn = (Button) findViewById(R.id.btn_postpaid);

    PostPaid_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        MeterNo = mMeterNo.getText().toString();
        Amount = mAmount.getText().toString();
        Pin = mPin.getText().toString();

        if (MeterNo.length() == 0 && Amount.length() == 0 && Pin.length() == 0) {
          mMeterNo.setError("This Field Is Required");
          mAmount.setError("This Field Is Required");
          mPin.setError("This Field Is Required");
        } else if (MeterNo.length() == 0) {
          mMeterNo.setError("This Field Is Required");
        } else if (Amount.length() == 0) {
          mAmount.setError("This Field Is Required");
        } else if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          Encryption encrypt = new Encryption();
          String phcn_charge_uri = "method=phcnenquiry&meterno=" + MeterNo;
          //Log.d(Tag, "P2P charge uri is : " + P2P_charge_uri);
          _Task = new GetCustomerName_Task();
          _Task.execute(phcn_charge_uri);
        }
      }

    });

  }

  private class GetCustomerName_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      Pd = new Progressdialog(Postpaid_Activity.this, "Please Wait..");
      Pd.showProgress();
    }

    @Override
    protected String doInBackground(String... params) {
      String output;

      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Postpaid_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String s) {
      super.onPostExecute(s);
      String charge, Customer_Name;
      if (s.contains("|")) {
        Pd.dismissProgress();
        // if the result contain "|" sign means valid response
        StringTokenizer tokens = new StringTokenizer(s, "|");
        charge = tokens.nextToken().trim().toString();
        Customer_Name = tokens.nextToken().trim().toString();

        if (Customer_Name.equals("200")) {
          Customer_Name = "Unavailable at the moment";
        }

        // display a dialog for the user to verify the transaction
        PostPaid_Confirmation_dialog(Customer_Name, charge);

      } else {
        Pd.dismissProgress();
        // an error occured on the service
        // display the dialog with the errors
        new Dialog(Postpaid_Activity.this, "Connection Failed.Please Try again later");
      }
    }
  }

  private class PostPaid_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Postpaid_Activity.this, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Postpaid_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      Pd.dismissProgress(); // dismiss progress dialog

      new Dialog(Postpaid_Activity.this, result); // insert response into dialog
      mMeterNo.setText("");
      mAmount.setText("");
      mPin.setText("");
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}

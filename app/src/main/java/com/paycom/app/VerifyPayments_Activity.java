package com.paycom.app;

import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.Constants;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;
import org.json.JSONObject;

public class VerifyPayments_Activity extends BaseActivity {

  private EditText mBookId;
  private Button VerifyBtn;
  private String BookId;

  private RelativeLayout detailsRelative;
  private ConnectionChecker connChecker = new ConnectionChecker(VerifyPayments_Activity.this);
  private String safeURl;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_verify_payments);

    mBookId = (EditText) findViewById(R.id.plateno_verify_payments);
    detailsRelative = (RelativeLayout) findViewById(R.id.rel_1);
    VerifyBtn = (Button) findViewById(R.id.verify_payments_btn);

    VerifyBtn.setEnabled(false);
    VerifyBtn.setAlpha(0.5f);

    mBookId.addTextChangedListener(new TextWatcher() {
      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {

      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {

      }

      @Override
      public void afterTextChanged(Editable s) {

        if (mBookId.getText().toString().trim().equals("") || mBookId.getText().length() < 0) {

          VerifyBtn.setEnabled(false);
          VerifyBtn.setAlpha(0.5f);

        } else if (mBookId.getText().toString().toLowerCase().startsWith("BK".toLowerCase())
            == false) {

          VerifyBtn.setEnabled(false);
          VerifyBtn.setAlpha(0.5f);
          mBookId.setError("Invalid Booking ID");

        } else {
          VerifyBtn.setEnabled(true);
          VerifyBtn.setAlpha(1f);
        }

      }
    });

    VerifyBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        BookId = mBookId.getText().toString();

        try {

          safeURl = Constants.verifyPaymentURL + "BookID=" + URLEncoder
              .encode(BookId.toUpperCase(), "UTF-8");

          if (connChecker.isAvailable()) {

            new Payment_VerificationTask().execute();

          } else {

            Toast.makeText(VerifyPayments_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }

        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
        }
      }
    });

  }

  private class Payment_VerificationTask extends AsyncTask<Void, Void, JSONObject> {

    Progressdialog progressdialog;
    String response;
    JSONObject jsonObject = null;

    TextView bookingDateTxt, RefIdTxt, statusTxt;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();

      progressdialog = new Progressdialog(VerifyPayments_Activity.this, "Please Wait..");
      progressdialog.showProgress();

      detailsRelative.setVisibility(View.GONE);

    }

    @Override
    protected JSONObject doInBackground(Void... params) {

      try {

        BufferedReader reader;
        // Only display the first 500 characters of the retrieved
        // web page content.
        StringBuilder sb;

        URL url = new URL(safeURl);
        HttpURLConnection conn = (HttpURLConnection) url.openConnection();
        conn.setRequestMethod("GET");
        conn.setDoOutput(false);
        conn.setConnectTimeout(3000); /* milliseconds */
        conn.connect();

        // read the output from the server
        reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
        sb = new StringBuilder();

        String line;
        while ((line = reader.readLine()) != null) {
          sb.append(line);
        }

        reader.close();

        // create a JSON object hierarchy from the result
        jsonObject = new JSONObject(sb.toString());

      } catch (Exception e) {
        e.printStackTrace();
        response = "Unable to connect to the service at the moment.Please try again later";
      }

      return jsonObject;

    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
      super.onPostExecute(jsonObject);

      progressdialog.dismissProgress();

      try {

        String bookingDate = jsonObject.get("bookDate").toString();
        String status = jsonObject.get("payStatus").toString();
        String RefId = jsonObject.get("itemID").toString();

        bookingDateTxt = (TextView) findViewById(R.id.booking_date);
        statusTxt = (TextView) findViewById(R.id.payment_status);

        if (status.startsWith("false")) {
          status = "PENDING";
        } else if (status.startsWith("true")) {
          status = "PAID";
        }

        detailsRelative.setVisibility(View.VISIBLE);
        bookingDateTxt.setText(bookingDate);
        statusTxt.setText(status);

      } catch (Exception e) {

        new Dialog(VerifyPayments_Activity.this, response); // insert response into dialog
      }

    }
  }

}

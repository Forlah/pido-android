package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

public class PHCN_Activity extends BaseActivity {

  private String actions[] = {"Eko PHCN", "Kaduna Electricity", "Kano Electricity", "Postpaid"};

  RecyclerView mPHCN;
  PHCN_RecyclerAdapter mPhcn_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_phcn);

    mPhcn_Adapter = new PHCN_RecyclerAdapter(getApplicationContext(), actions);
    mPHCN = (RecyclerView) findViewById(R.id.phcn_list);
    mPHCN.setHasFixedSize(true);
    mPHCN.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    mPHCN.setLayoutManager(mLayoutManager);
    mPHCN.setAdapter(mPhcn_Adapter);
  }

  private class PHCN_RecyclerAdapter extends
      RecyclerView.Adapter<PHCN_RecyclerAdapter.DataObjectHolder> {

    String[] types;
    Context mContext;

    private PHCN_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      types = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView phcn_name;
      ImageView phcn_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        phcn_name = (TextView) itemView.findViewById(R.id.phcn_name);
        phcn_Image = (ImageView) itemView.findViewById(R.id.phcn_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.phcn_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, int position) {
      final int _position = position;

      holder.phcn_name.setText(types[position]);

      if (types[position].equals("Eko PHCN")) {

        holder.phcn_Image.setImageResource(R.mipmap.edc);
      } else if (types[position].equals("Kaduna Electricity")) {
        holder.phcn_Image.setImageResource(R.mipmap.kaedc);
      } else if (types[position].equals("Kano Electricity")) {

        holder.phcn_Image.setImageResource(R.mipmap.kedco);
      } else {

      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if (types[_position].equals("Eko PHCN")) {

            Intent i = new Intent(PHCN_Activity.this, Eko_Activity.class);
            startActivity(i);

          } else if (types[_position].equals("Kaduna Electricity")) {

            Intent i = new Intent(PHCN_Activity.this, KadunaElectricity_Activity.class);
            startActivity(i);

          } else if (types[_position].equals("Kano Electricity")) {

            Intent i = new Intent(PHCN_Activity.this, KanoElectricity_Activity.class);
            startActivity(i);

          } else {

            Intent i = new Intent(PHCN_Activity.this, Postpaid_Activity.class);
            startActivity(i);
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return types.length;
    }

  }

}

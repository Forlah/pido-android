package com.paycom.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListPopupWindow;
import android.widget.TextView;
import com.paycom.utitlities.Dialog;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FundWallet_Activity extends AppCompatActivity {

  private EditText mMonth, mYear, mCardNo, mCVV, mFirstname, mLastname, mEmail, mAmount, mPidoMobile;
  private String Month, Year, CardNo, CVV, Firstname, Lastname, Email, Amount, PidoMobile;
  private TextView cvv_help;

  private ListPopupWindow lpw, year_lpw;

  private String[] yearlist = new String[21];

  private Button PayBtn;

  public static boolean emailValidator(final String mailAddress) {

    Pattern pattern;
    Matcher matcher;

    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    pattern = Pattern.compile(EMAIL_PATTERN);
    matcher = pattern.matcher(mailAddress);
    return matcher.matches();

  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_fund_wallet);

    mMonth = (EditText) findViewById(R.id.mm);
    mYear = (EditText) findViewById(R.id.yy);
    mCardNo = (EditText) findViewById(R.id.card_number);
    mCVV = (EditText) findViewById(R.id.cvv);
    mFirstname = (EditText) findViewById(R.id.firstname);
    mLastname = (EditText) findViewById(R.id.lastname);
    mEmail = (EditText) findViewById(R.id.email);
    mAmount = (EditText) findViewById(R.id.amount);
    mPidoMobile = (EditText) findViewById(R.id.pido_mobile);
    PayBtn = (Button) findViewById(R.id.fund_wallet_btn);
    cvv_help = (TextView) findViewById(R.id.cvv_help);

    SimpleDateFormat y = new SimpleDateFormat("yyyy");
    String todaysDate = y.format(new Date());

    final String[] list = new String[]{"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11",
        "12"};

    for (int i = 0; i < 21; i++) {

      int date = Integer.parseInt(todaysDate) + i;
      yearlist[i] = String.valueOf(date); // populate year list
    }

    lpw = new ListPopupWindow(FundWallet_Activity.this);
    lpw.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, list));
    lpw.setAnchorView(mMonth);
    lpw.setModal(true);
    lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        String item = list[position];

        if (item.length() == 1) {

          item = "0" + item;
        }

        mMonth.setText(item);
        lpw.dismiss();
      }
    });

    year_lpw = new ListPopupWindow(FundWallet_Activity.this);
    year_lpw.setAdapter(new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, yearlist));
    year_lpw.setAnchorView(mYear);
    year_lpw.setModal(true);
    year_lpw.setOnItemClickListener(new AdapterView.OnItemClickListener() {
      @Override
      public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        String item = yearlist[position];
        mYear.setText(item);
        year_lpw.dismiss();
      }
    });

    mMonth.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        lpw.show();
      }
    });

    mYear.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        year_lpw.show();
      }
    });

    cvv_help.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        AlertDialog.Builder builder = new AlertDialog.Builder(FundWallet_Activity.this);
        builder.setTitle("What Is CVV?");
        builder.setView(R.layout.cvchelp_layout);
        builder.setCancelable(true);
        AlertDialog dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimationSlide;
        dialog.show();

//                if (Build.VERSION.SDK_INT> Build.VERSION_CODES.LOLLIPOP){
//                    AlertDialog.Builder builder=new AlertDialog.Builder(FundWallet_Activity.this);
//                    builder.setTitle("What Is CVV?");
//                    builder.setView(R.layout.cvchelp_layout);
//                    builder.setCancelable(true);
//                    AlertDialog dialog=builder.create();
//                    dialog.getWindow().getAttributes().windowAnimations=R.style.DialogAnimationSlide;
//                    dialog.show();
//                }
//                else {
//                    new AlertDialog.Builder(FundWallet_Activity.this)
//                            .setIcon(R.mipmap.cvvimage)
//                            .setMessage("Cvc Help")
//                            .setCancelable(true).show();
//                }

      }
    });

    PayBtn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        CardNo = mCardNo.getText().toString().trim();
        Month = mMonth.getText().toString().trim();
        Year = mYear.getText().toString().trim();
        CVV = mCVV.getText().toString().trim();
        Firstname = mFirstname.getText().toString().trim();
        Lastname = mLastname.getText().toString().trim();
        Email = mEmail.getText().toString().trim();
        Amount = mAmount.getText().toString().trim();
        PidoMobile = mPidoMobile.getText().toString().trim();

        if (TextUtils.isEmpty(mCardNo.getText()) && TextUtils.isEmpty(mMonth.getText()) &&
            TextUtils.isEmpty(mYear.getText()) && TextUtils.isEmpty(mCVV.getText()) &&
            TextUtils.isEmpty(mFirstname.getText()) && TextUtils.isEmpty(mLastname.getText()) &&
            TextUtils.isEmpty(mEmail.getText()) && TextUtils.isEmpty(mAmount.getText()) && TextUtils
            .isEmpty(mPidoMobile.getText())) {

          new Dialog()
              .show_alert(FundWallet_Activity.this, "Please provide information for all fields",
                  "Alert");
        } else if (CardNo.length() == 0) {
          mCardNo.setError("Please Enter Your Card Number");
        } else if (CardNo.length() < 16) {
          mCardNo.setError("Incomplete Card Number");
        } else if (Month.length() == 0 || Year.length() == 0) {

          new Dialog().show_alert(FundWallet_Activity.this, "Card Expiry Date Required", "Alert");

        } else if (CVV.length() == 0) {

          mCVV.setError("CVV Required ");
        } else if (mCVV.length() < 3) {

          mCVV.setError("Incomplete CVV.. ");
        } else if (Firstname.length() == 0) {

          mFirstname.setError("This Field Is Required");
        } else if (Lastname.length() == 0) {

          mLastname.setError("This Field Is Required");
        } else if (Email.length() == 0) {

          mEmail.setError("This Field Is Required");

        } else if (!emailValidator(Email)) {

          mEmail.setError("Invalid Email Address");

        } else if (Amount.length() == 0) {

          mAmount.setError("This Field Is Required..");
        } else if (Integer.parseInt(Amount) < 500) {

          mAmount.setError("Amount Must Be At Least 500 Naira");
        } else if (PidoMobile.length() == 0) {

          mPidoMobile.setError("This Field Is Required..");

        } else if (PidoMobile.length() < 11) {

          mPidoMobile.setError("11 Digit Pido Mobile Number Required");

        } else {

          Intent intent = new Intent(FundWallet_Activity.this, FundWalletWeb_Activity.class);
          intent.putExtra("KEY_CardNO", CardNo);
          intent.putExtra("KEY_Month", Month);
          intent.putExtra("KEY_Year", Year);

          intent.putExtra("KEY_CVV", CVV);
          intent.putExtra("KEY_Firstname", Firstname);
          intent.putExtra("KEY_Lastname", Lastname);

          intent.putExtra("KEY_Email", Email);
          intent.putExtra("KEY_Amount", Amount);
          intent.putExtra("KEY_PidoMobile", PidoMobile);

          startActivity(intent);

          FundWallet_Activity.this
              .overridePendingTransition(R.anim.activity_in, R.anim.activity_out);

        }

      }
    });
  }
}

package com.paycom.app;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import com.paycom.utitlities.Constants;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import org.json.JSONException;
import org.json.JSONObject;

public class FundWalletWeb_Activity extends AppCompatActivity {

  private String Firstname;
  private String Lastname;
  private String Phonenumber;
  private String email;
  private String Cardno;
  private String cvv;
  private String expiry_year;
  private String expiry_month;
  private String amount;
  private static String medium = "mobile";

  private static String URL = Constants.fundWalletURL;
  private String encodedURl;
  private ProgressBar mProgressBar;

  private String toJson() {

    JSONObject object = new JSONObject();
    try {
      object.put("firstname", Firstname);
      object.put("lastname", Lastname);
      if (Phonenumber.length() == 11) {
        object.put("phonenumber", Phonenumber.replaceFirst("0", "+234"));
      } else if (Phonenumber.length() == 6) {
        object.put("phonenumber", "+234" + Phonenumber);
      }

      object.put("email", email);
      object.put("card_no", Cardno);
      object.put("cvv", cvv);

      object.put("expiry_year", expiry_year);
      object.put("expiry_month", expiry_month);
      object.put("amount", amount);

      object.put("medium", medium);

    } catch (JSONException e) {
      e.printStackTrace();
    }

    return object.toString();
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    Cardno = getIntent().getStringExtra("KEY_CardNO");
    expiry_month = getIntent().getStringExtra("KEY_Month");
    expiry_year = getIntent().getStringExtra("KEY_Year");

    cvv = getIntent().getStringExtra("KEY_CVV");
    Firstname = getIntent().getStringExtra("KEY_Firstname");
    Lastname = getIntent().getStringExtra("KEY_Lastname");

    email = getIntent().getStringExtra("KEY_Email");
    amount = getIntent().getStringExtra("KEY_Amount");
    Phonenumber = getIntent().getStringExtra("KEY_PidoMobile");

    setContentView(R.layout.activity_fund_wallet_web);

    try {
      encodedURl = URL + URLEncoder.encode(toJson(), "UTF-8");
    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    mProgressBar = (ProgressBar) findViewById(R.id.pb);

    final WebView webView = (WebView) findViewById(R.id.webview);
    if (Build.VERSION.SDK_INT >= 21) {
      CookieManager.getInstance().setAcceptThirdPartyCookies(webView, true);
    } else {
      CookieManager.getInstance().setAcceptCookie(true);
    }

    WebSettings webSettings = webView.getSettings();
    webSettings.setJavaScriptEnabled(true);
    //webView.loadDataWithBaseURL(URL, "my text","text/plain", "UTF-8", null);

    webView.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        // Do something on page loading started
        // Visible the progressbar
        mProgressBar.setVisibility(View.VISIBLE);
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        // Do something when page loading finished

        //mProgressBar.setVisibility(View.GONE);
      }

    });

    webView.setWebChromeClient(new WebChromeClient() {
      /*
          public void onProgressChanged (WebView view, int newProgress)
              Tell the host application the current progress of loading a page.

          Parameters
              view : The WebView that initiated the callback.
              newProgress : Current page loading progress, represented by an integer
                  between 0 and 100.
      */
      public void onProgressChanged(WebView view, int newProgress) {
        // Update the progress bar with page loading progress
        mProgressBar.setProgress(newProgress);

        if (newProgress == 100) {
          // Hide the progressbar
          mProgressBar.setVisibility(View.GONE);
        }
      }
    });

    webView.loadUrl(encodedURl);

  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {

    switch (item.getItemId()) {

      case android.R.id.home:
        FundWalletWeb_Activity.this.finish();
        return true;
    }
    return super.onOptionsItemSelected(item);
  }
}

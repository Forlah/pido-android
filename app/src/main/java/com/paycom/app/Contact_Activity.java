package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import com.paycom.DBhelper.DatabaseHandler;
import com.paycom.model.Contact;
import java.util.ArrayList;

public class Contact_Activity extends BaseActivity {

  private ArrayList<Contact> contacts;
  private ListView mlistview;
  Contacts_Adapter contactsAdapter;
  DatabaseHandler db;
  String[] itemOptions = {"Perform Airtime Topup", "Transfer To PiDO", "Transfer To Bank",
      "Edit Contact"};

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // get instance of DatabaseHandler class
    db = new DatabaseHandler(Contact_Activity.this);
    contacts = db.getAllContacts();

    if (contacts.size() == 0) {
      setContentView(R.layout.no_contacts_activity);

      FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
      fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent i = new Intent(Contact_Activity.this, AddContact_Activity.class);
          startActivity(i);
          finish();
        }
      });
    } else {
      setContentView(R.layout.activity_contact);

      mlistview = (ListView) findViewById(R.id.contact_list);
      FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
      fab.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {
          Intent i = new Intent(Contact_Activity.this, AddContact_Activity.class);
          startActivity(i);
          finish();
        }
      });

      contactsAdapter = new Contacts_Adapter(contacts);
      mlistview.setAdapter(contactsAdapter);
      // contactsAdapter.notifyDataSetChanged();
      mlistview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

          final int Position = position;

          //   Toast.makeText(Contact_Activity.this,"Position selected is "+position,Toast.LENGTH_LONG).show();
          AlertDialog.Builder builder = new AlertDialog.Builder(Contact_Activity.this);
          builder.setItems(itemOptions, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              if (itemOptions[which].equals("Perform Airtime Topup")) {
                String no = contacts.get(Position).getPhonenumber();
                Intent i = new Intent(Contact_Activity.this, AirTime_Topup.class);
                i.putExtra("From Contact Activity to perform Airtime topup", no);
                startActivity(i);
                finish();
              } else if (itemOptions[which].equals("Transfer To PiDO")) {
                String no = contacts.get(Position).getPhonenumber();
                Intent i = new Intent(Contact_Activity.this, TransferToPido_Activity.class);
                i.putExtra("From Contact Activity to perform PiDO transfer", no);
                startActivity(i);
                finish();

              } else if (itemOptions[which].equals("Transfer To Bank")) {
                String no = contacts.get(Position).getPhonenumber();
                String acc_no = contacts.get(Position).getAccountnumber();
                String bank_name = contacts.get(Position).getBank();
                Intent i = new Intent(Contact_Activity.this, TransferToBank_Activity.class);
                i.putExtra("no perform bank transfer", no);
                i.putExtra("acc_no  to perform bank transfer", acc_no);
                i.putExtra("bank_name from contact", bank_name);
                startActivity(i);
                finish();

              } else {

                int db_id = contacts.get(Position).getId();
                String firstname = contacts.get(Position).getFirstname();
                String surname = contacts.get(Position).getSurname();
                String no = contacts.get(Position).getPhonenumber();
                String acc_no = contacts.get(Position).getAccountnumber();
                String bank_name = contacts.get(Position).getBank();

                System.out.println("Bank name along extras is " + bank_name);

                Intent i = new Intent(Contact_Activity.this, AddContact_Activity.class);
                i.putExtra("contact row id", db_id);
                i.putExtra("Edit firstname", firstname);
                i.putExtra("Edit surname", surname);
                i.putExtra("Edit phone number", no);
                i.putExtra("Edit acc no", acc_no);
                i.putExtra("Edit bank", bank_name);

                startActivity(i);
                finish();

              }

            }
          });

          builder.show();
        }
      });

    }

  }

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    // Inflate the menu; this adds items to the action bar if it is present.
    // getMenuInflater().inflate(R.menu.menu_contact, menu);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    // Handle action bar item clicks here. The action bar will
    // automatically handle clicks on the Home/Up button, so long
    // as you specify a parent activity in AndroidManifest.xml.
    int id = item.getItemId();

    //noinspection SimplifiableIfStatement
    if (id == R.id.add_contact) {
      //return true;
      Intent i = new Intent(Contact_Activity.this, AddContact_Activity.class);
      startActivity(i);
      finish();
    }

    return super.onOptionsItemSelected(item);
  }

  private class Contacts_Adapter extends ArrayAdapter<Contact> {

    ArrayList<Contact> Items;
    // Context mContext;

    public Contacts_Adapter(ArrayList<Contact> items) {
      super(Contact_Activity.this, 0, items);
      Items = items;
      //  mContext = c;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
      //if there is no view available , inflate one
      if (convertView == null) {
        convertView = getLayoutInflater().inflate(R.layout.add_contact_row, null);
      }

      // Contact c = getItem(position);
      Contact c = Items.get(position);

      //Configure Fullname text
      TextView mFullname = (TextView) convertView.findViewById(R.id.contact_fullname);
      mFullname.setText(c.getSurname() + " " + c.getFirstname());

      // configure bank name
      TextView mBank = (TextView) convertView.findViewById(R.id.contact_bank);
      mBank.setText(c.getBank());

      // configgure Account number
      TextView mAcc = (TextView) convertView.findViewById(R.id.contact_acc_no);
      mAcc.setText(c.getAccountnumber());

      return convertView;
    }
  }

}

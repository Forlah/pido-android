package com.paycom.app;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.CardView;
import android.view.View;

public class TimaasVerification_Activity extends BaseActivity {

  private CardView mVerifyBooking, mVerifyPayments;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_timaas_verification_);

    mVerifyBooking = (CardView) findViewById(R.id.verify_bookings_cardview);
    mVerifyBooking.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Intent intent = new Intent(TimaasVerification_Activity.this, VerifyBookings_Activity.class);
        startActivity(intent);

      }
    });

    mVerifyPayments = (CardView) findViewById(R.id.verify_payments_cardview);
    mVerifyPayments.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        Intent intent = new Intent(TimaasVerification_Activity.this, VerifyPayments_Activity.class);
        startActivity(intent);

      }
    });
  }
}

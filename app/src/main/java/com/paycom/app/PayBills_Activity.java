package com.paycom.app;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by FOLASHELE on 8/12/2015.
 */
public class PayBills_Activity extends BaseActivity {

  //String[] Bills = {"Pay TV","PHCN","Internet Payment","Araya","School Fees","Lottery And Betting","State Bill Payment"};
  String[] Bills = {"Pay TV", "PHCN", "Internet Payment", "Araya", "School Fees",
      "Lottery And Betting"};

  RecyclerView mPays;
  PayBills_RecyclerAdapter mPayBill_Adapter;
  LinearLayoutManager mLayoutManager = null;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);

    setContentView(R.layout.bill_payment);

    mPayBill_Adapter = new PayBills_RecyclerAdapter(getApplicationContext(), Bills);
    mPays = (RecyclerView) findViewById(R.id.pay_bill_list);
    mPays.setHasFixedSize(true);
    mPays.setItemAnimator(new DefaultItemAnimator());
    mLayoutManager = new LinearLayoutManager(getApplicationContext());
    // mRecyclerView.setLayoutManager(mLayoutManager);
    mPays.setLayoutManager(mLayoutManager);
    mPays.setAdapter(mPayBill_Adapter);

  }

  private class PayBills_RecyclerAdapter extends
      RecyclerView.Adapter<PayBills_RecyclerAdapter.DataObjectHolder> {

    String[] bills;
    Context mContext;

    private PayBills_RecyclerAdapter(Context context, String[] text) {

      mContext = context;
      bills = text;

    }

    public class DataObjectHolder extends RecyclerView.ViewHolder {

      TextView bill_name;
      ImageView bill_Image;
      View v = null;

      public DataObjectHolder(View itemView) {
        super(itemView);
        v = itemView;
        bill_name = (TextView) itemView.findViewById(R.id.bill_type);
        bill_Image = (ImageView) itemView.findViewById(R.id.bill_image);

      }

    }

    @Override
    public DataObjectHolder onCreateViewHolder(ViewGroup parent, int viewType) {
      View view = LayoutInflater.from(parent.getContext())
          .inflate(R.layout.bills_payment_row, parent, false);

      DataObjectHolder dataObjectHolder = new DataObjectHolder(view);

      return dataObjectHolder;
    }

    @Override
    public void onBindViewHolder(DataObjectHolder holder, final int position) {
      // final int _position = position;
      holder.bill_name.setText(bills[position]);

      if (bills[position].equals("Pay TV")) {

        // holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.pay_tv);
      } else if (bills[position].equals("PHCN")) {
        //  holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.phcn);

      } else if (bills[position].equals("Internet Payment")) {
        //  holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.internet);

      } else if (bills[position].equals("Araya")) {
        //  holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.araya);

      } else if (bills[position].equals("Lottery And Betting")) {
        // holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.lottery_betting);

      } else {
        //holder.bill_name.setText(bills[position]);
        holder.bill_Image.setImageResource(R.mipmap.sendmoney);
      }

      holder.v.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View v) {

          if ("Pay TV".equals(bills[position])) {

            Intent i = new Intent(PayBills_Activity.this, PayTv_Activity.class);
            startActivity(i);

          } else if (bills[position].equals("PHCN")) {

            Intent i = new Intent(PayBills_Activity.this, PHCN_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("Internet Payment")) {

            Intent i = new Intent(PayBills_Activity.this, InternetPayment_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("Araya")) {

            Intent i = new Intent(PayBills_Activity.this, Araya_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("School Fees")) {

            Intent i = new Intent(PayBills_Activity.this, SchoolFees_Activity.class);
            startActivity(i);
          } else if (bills[position].equals("Lottery And Betting")) {

            Intent i = new Intent(PayBills_Activity.this, LotteryBetting_Activity.class);
            startActivity(i);
          } else {
          }

        }
      });

    }

    @Override
    public int getItemCount() {
      return bills.length;
    }

  }

}

package com.paycom.app;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.model.User;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.util.ArrayList;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Created by FOLASHELE on 8/6/2015.
 */
public class ResetPassword_Activity extends AppCompatActivity {

  private EditText mSecurityQuestion, mSecurityAnswer, mNewPassword, mConfirmPassword;
  private String SecurityAnswer, NewPassword, ConfirmPassword, db_mobileNO, db_password, db_email, db_fullname, db_gender, db_imei, db_token;
  private Button Reset;
  private int db_id;
  private ConnectionChecker connChecker = new ConnectionChecker(ResetPassword_Activity.this);
  private static final String App_Version = "3.0";
  private SecurityQuestion_Task _Task;
  private TestNo_Task _task;
  Progressdialog Pd;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.reset_password);

    Reg_DatabaseHandler reg_Db = new Reg_DatabaseHandler(ResetPassword_Activity.this);
    ArrayList<User> user = reg_Db.getAll();

    db_mobileNO = user.get(0).getMobile_no();
    db_password = user.get(0).getPassword();
    db_imei = user.get(0).getImei();
    db_token = user.get(0).getToken();
    db_id = user.get(0).getId();
    db_fullname = user.get(0).getFullname();
    db_gender = user.get(0).getGender();
    db_email = user.get(0).getEmail();

    if (connChecker.isAvailable()) {
      String test_uri = "method=test&fone=" + db_mobileNO;
      _task = new TestNo_Task();
      _task.execute(test_uri);
    } else {
      Toast
          .makeText(ResetPassword_Activity.this, "No Connection To The Internet", Toast.LENGTH_LONG)
          .show();
    }

    mSecurityQuestion = (EditText) findViewById(R.id.security_qst);
    mSecurityAnswer = (EditText) findViewById(R.id.security_ans);
    mNewPassword = (EditText) findViewById(R.id.new_password);
    mConfirmPassword = (EditText) findViewById(R.id.confirm_password);

    Reset = (Button) findViewById(R.id.Reset_btn);
    Reset.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        SecurityAnswer = mSecurityAnswer.getText().toString();
        NewPassword = mNewPassword.getText().toString();
        ConfirmPassword = mConfirmPassword.getText().toString();

        if (SecurityAnswer.length() == 0 && NewPassword.length() == 0
            && ConfirmPassword.length() == 0) {
          mSecurityAnswer.setError("This Field Is Required");
          mNewPassword.setError("This Field Is Required");
          mConfirmPassword.setError("This Field Is Required");
        } else if (SecurityAnswer.length() == 0) {
          mSecurityAnswer.setError("This Field Is Required");
        } else if (NewPassword.length() == 0) {
          mNewPassword.setError("This Field Is Required");
        } else if (NewPassword.length() < 6) {
          mNewPassword.setError("Password must be a minimum of 6 characters");
        } else if (ConfirmPassword.length() == 0) {
          mConfirmPassword.setError("This Field Is Required");
        } else if (ConfirmPassword.equals(NewPassword) == false) {

          mConfirmPassword.setError(" Passwords Do Not Match");
        } else {

          String security_question_uri =
              "method=checksecurityquestion&msisdn=" + db_mobileNO + "&autcode=" + db_imei
                  + "&securityanswer=" + SecurityAnswer
                  + "&version=" + App_Version;

          if (connChecker.isAvailable()) {

            _Task = new SecurityQuestion_Task();
            _Task.execute(security_question_uri);
          } else {
            Toast.makeText(ResetPassword_Activity.this, "No Connection To The Internet",
                Toast.LENGTH_LONG).show();
          }
        }
      }
    });

  }

  private class TestNo_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(ResetPassword_Activity.this, "Please wait a moment");
      Pd.showProgress_Title();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(ResetPassword_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.contains("|")) {
        Pd.dismissProgress();
        String sQuestion;
        // if the result contain "|" sign means valid response
        try {
          StringTokenizer tokens = new StringTokenizer(result, "|");
          sQuestion = tokens.nextToken();
          mSecurityQuestion.setText(sQuestion);

        } catch (NoSuchElementException NE) {
          Pd.dismissProgress();
          Toast.makeText(ResetPassword_Activity.this,
              "Unexpected error occurred , Please try again ", Toast.LENGTH_SHORT).show();
        }

      } else {
        Pd.dismissProgress(); // dismiss progress dialog

        new Dialog(ResetPassword_Activity.this, result);
      }
    }
  }

  private class updateDB_task extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(ResetPassword_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.startsWith("1")) {   // password change was successful
        // update the local db
        Reg_DatabaseHandler rDb = new Reg_DatabaseHandler(ResetPassword_Activity.this);
        //  Toast.makeText(ResetPassword_Activity.this,"The delete return is "+s,Toast.LENGTH_LONG).show();
        System.out.println("Fullname: " + db_fullname + "\n" +
            "Mobile_No: " + db_mobileNO + "\n" +
            "New Password: " + NewPassword + "\n" +
            "IMEI: " + db_imei + "\n" +
            "Token: " + db_token + "\n" +
            "Email: " + db_email + "\n" +
            "Id: " + db_id + "\n" +
            "Gender: " + db_gender + "\n");

        long value = rDb
            .updateUser(db_id, db_fullname, db_mobileNO, NewPassword, db_imei, db_token, db_email,
                db_gender);

        Pd.dismissProgress(); // dismiss progress dialog
        if (value > 0) {

          AlertDialog.Builder builder = new AlertDialog.Builder(ResetPassword_Activity.this);
          builder.setTitle("Success");
          builder.setMessage("PiDO password change was successful..");
          builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
              Intent i = new Intent(ResetPassword_Activity.this, Login_Activity.class);
              startActivity(i);
              finish();
            }
          });
          builder.show();

        } else {
          System.out.println("Error occured inside the local db..");
          new Dialog(ResetPassword_Activity.this, "Error Occurred");

        }

      } else {

        Pd.dismissProgress();
        new Dialog(ResetPassword_Activity.this, result);

        mSecurityQuestion.setText("");
        mSecurityAnswer.setText("");
        mNewPassword.setText("");
        mConfirmPassword.setText("");
      }

    }
  }

  private class SecurityQuestion_Task extends AsyncTask<String, Void, String> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(ResetPassword_Activity.this, "Please wait a moment");
      Pd.showProgress_Title();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(ResetPassword_Activity.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.startsWith("8001") == true) {

        String update_uri = "method=forgetpassword&mobile=" + db_mobileNO
            + "&autcode=" + db_imei
            + "&version=" + App_Version
            + "&password=" + NewPassword
            + "&securityanswer=" + SecurityAnswer;

        // update service db
        new updateDB_task().execute(update_uri);

      } else if (result.startsWith("8002") == true) {
        Pd.dismissProgress(); // dismiss progress dialog

        new Dialog(ResetPassword_Activity.this, "Invalid Credentials");
      } else {

        Pd.dismissProgress(); // dismiss progress dialog

        new Dialog(ResetPassword_Activity.this, result);
      }
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }
}


package com.paycom.app;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.utitlities.Sch_Fees;

public class UnicalCES_Activity extends BaseActivity {

  private EditText mPin;
  private String Pin, pidoMobile;
  private Button UnicalCES_Btn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_unical_ces);

    Reg_DatabaseHandler rdb = new Reg_DatabaseHandler(
        UnicalCES_Activity.this); // instance of localDB
    pidoMobile = rdb.getMobileNo(); // pido user mobile no from db

    mPin = (EditText) findViewById(R.id.unical_ces_pin);

    UnicalCES_Btn = (Button) findViewById(R.id.btn_unical_ces);
    UnicalCES_Btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        Pin = mPin.getText().toString();
        if (Pin.length() == 0) {
          mPin.setError("This Field Is Required");
        } else if (Pin.length() < 4) {
          mPin.setError("4 Digit Number Required");
        } else {
          String uri =
              "method=schoolformpayment&commandtype=" + "otherschoool" + "&productid=" + " "
                  + "&schoolid=" + "2" +
                  "&customerno=" + pidoMobile + "&mobileno=" + pidoMobile + "&pin=" + Pin;

          new Sch_Fees(UnicalCES_Activity.this, uri);
          mPin.setText("");
        }
      }
    });

  }

}

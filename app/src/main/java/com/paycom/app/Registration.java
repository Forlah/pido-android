package com.paycom.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;
import com.paycom.model.DataLab;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.util.StringTokenizer;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by FOLASHELE on 8/5/2015.
 */
public class Registration extends AppCompatActivity {

  private Button mContinue;
  private EditText mName, mSurname, mEmail, mPhoneNumber, mPassword, mConfirmpassword;
  private Spinner mGender;
  private String Name, Surname, Email, PhoneNumber, Password, Gender, ConfirmPassword;
  private static final String Tag = "reg";
  private static final String App_Version = "3.0";
  Progressdialog Pd;
  String sQuestion, token;
  private ConnectionChecker connCheck;
  private checkUser_Task _task;
  private check_updateDB_task _Task;

  public static boolean emailValidator(final String mailAddress) {

    Pattern pattern;
    Matcher matcher;

    final String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
        + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    pattern = Pattern.compile(EMAIL_PATTERN);
    matcher = pattern.matcher(mailAddress);
    return matcher.matches();

  }

  public String getIMEI() {
    TelephonyManager mngr = (TelephonyManager) Registration.this
        .getSystemService(this.TELEPHONY_SERVICE);
    String imei = mngr.getDeviceId();
    return imei;
  }

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.register);

    mName = (EditText) findViewById(R.id.nameFIeld);

    mSurname = (EditText) findViewById(R.id.surnameFIeld);

    mPhoneNumber = (EditText) findViewById(R.id.phonenoField);

    mEmail = (EditText) findViewById(R.id.emailField);

    mPassword = (EditText) findViewById(R.id.passwordField);

    mConfirmpassword = (EditText) findViewById(R.id.confirmPasswordField);

    mGender = (Spinner) findViewById(R.id.gender_spinner);
    mGender.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

      @Override
      public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        Gender = parent.getItemAtPosition(position).toString();
      }

      @Override
      public void onNothingSelected(AdapterView<?> parent) {

      }
    });

    mContinue = (Button) findViewById(R.id.continue_button);

    mContinue.setOnClickListener(new View.OnClickListener() {

      @Override
      public void onClick(View v) {

        Name = mName.getText().toString();
        Surname = mSurname.getText().toString();
        PhoneNumber = mPhoneNumber.getText().toString();
        Email = mEmail.getText().toString();
        Password = mPassword.getText().toString();
        ConfirmPassword = mConfirmpassword.getText().toString();

        if (Name.length() == 0 && Surname.length() == 0 && PhoneNumber.length() == 0
            && Email.length() == 0 && Password.length() == 0 &&
            ConfirmPassword.length() == 0) {

          mName.setError("This Field Is Required");
          mSurname.setError("This Field Is Required");
          mPhoneNumber.setError("This Field Is Required");
          mEmail.setError("This Field Is Required");
          mPassword.setError("This Field Is Required");
          mConfirmpassword.setError("This Field Is Required");
        } else if (Name.length() == 0) {
          mName.setError("This Field Is Required");

        } else if (Surname.length() == 0) {
          mSurname.setError("This Field Is Required");
        } else if (PhoneNumber.length() == 0) {
          mPhoneNumber.setError("This Field Is Required");
        } else if (PhoneNumber.length() < 11) {
          mPhoneNumber.setError("Ensure 11 digit mobile no");

        } else if (Email.length() == 0) {
          mEmail.setError("This Field Is Required");

        } else if (!emailValidator(Email)) {
          mEmail.setError("Please verify email");
        } else if (Password.length() == 0) {
          mPassword.setError("This Field Is Required");

        } else if (Password.length() < 6) {
          mPassword.setError("Password must be a minimum of 6 characters");

        } else if (ConfirmPassword.length() == 0) {
          mConfirmpassword.setError("This Field Is Required");

        } else if (ConfirmPassword.equals(Password) == false) {
          mConfirmpassword.setError("Passwords Do Not Match");
        } else {
          String test_uri = "method=test&fone=" + PhoneNumber;
          connCheck = new ConnectionChecker(Registration.this);

          // _task = new checkUser_Task();
          //_task.execute(test_uri);
          if (connCheck.isAvailable()) {
            _task = new checkUser_Task();
            _task.execute(test_uri);
          } else {
            Toast.makeText(Registration.this, "No Connection To The Internet", Toast.LENGTH_LONG)
                .show();
          }
        }

      }
    });
  }

  private class checkUser_Task extends AsyncTask<String, Void, String> {

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(Registration.this, "Please wait a moment");
      Pd.showProgress_Title();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Registration.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }
    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (result.trim().contains("|")) {
        Pd.dismissProgress();

        // if the result contain "|" sign means valid response, the mobile no exists
        StringTokenizer tokens = new StringTokenizer(result, "|");
        sQuestion = tokens.nextToken();
        token = tokens.nextToken();

        String check_update_uri = "method=checkandupdate&mobile=" + PhoneNumber
            + "&autcode=" + getIMEI() + "&version=" + App_Version + "&password=" + Password.trim();

        if (connCheck.isAvailable()) {

          // perform update on the service db
          new check_updateDB_task().execute(check_update_uri);
        } else {
          Toast.makeText(Registration.this, "No Connection To The Internet", Toast.LENGTH_LONG)
              .show();
        }

      } else if (result.trim().contentEquals("ERROR")) {

        // save the credentials of the user in the singleton class before leaving this activity
        DataLab.get(Registration.this)
            .addDetails(Name, Surname, PhoneNumber, Password, getIMEI(), " ", Email,
                Gender); // no token for a new user

        Intent i = new Intent(Registration.this, SecurityQnA_newUser.class);
        startActivity(i);

        finish(); // kill this activity
      } else {
        Pd.dismissProgress(); // dismiss progress dialog

        //  new Dialog(Registration.this,"An Error Occurred, Please try again later..");
        new Dialog(Registration.this, result);

      }
    }
  }

  private class check_updateDB_task extends AsyncTask<String, Void, String> {

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(Registration.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);

      if (result.contains("1") == false) {

        Pd.dismissProgress(); // dismiss progress dialog
        new Dialog(Registration.this, result);

      } else {

        Pd.dismissProgress();

        // save the credentials of the user in the singleton class before leaving the activity
        DataLab.get(Registration.this)
            .addDetails(Name.trim(), Surname.trim(), PhoneNumber.trim(), Password.trim(), getIMEI(),
                token.trim(), Email.trim(), Gender);

        Intent i = new Intent(Registration.this, SecurityQnA.class);
        i.putExtra("PiDO subscriber Security Question", sQuestion);
        startActivity(i);

        finish(); // kill this activity

      }
    }

  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }

    if (_Task != null) {
      if (_Task.getStatus() != AsyncTask.Status.FINISHED) {
        _Task.cancel(true);
      }
      _Task = null;
    }
  }

}


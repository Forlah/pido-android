package com.paycom.app;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import com.paycom.DBhelper.Reg_DatabaseHandler;
import com.paycom.model.Data;
import com.paycom.model.DataLab;
import com.paycom.model.User;
import com.paycom.utitlities.ConnectionChecker;
import com.paycom.utitlities.ConnectionToService;
import com.paycom.utitlities.Dialog;
import com.paycom.utitlities.Progressdialog;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;

public class SecurityQnA extends AppCompatActivity {

  private EditText msQuestion, msAnswer;
  private String sQuestion, sAnswer;
  private static final String App_Version = "3.0";
  private String Surname, firstname, Mobile_no, Password, IMEI, Token, Email, Gender;
  private ConnectionChecker connChecker = new ConnectionChecker(SecurityQnA.this);
  private loginwith_security_Task _task;

  private Button Continue_btn;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_security_qn);

    msQuestion = (EditText) findViewById(R.id.sQuest);
    msAnswer = (EditText) findViewById(R.id.sAnswer);

    String Pido_SQuestion = getIntent()
        .getStringExtra("PiDO subscriber Security Question");// retrieve data from the intent

    msQuestion.setText(Pido_SQuestion);

    Continue_btn = (Button) findViewById(R.id.continue_btn_QnA);
    Continue_btn.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        sAnswer = msAnswer.getText().toString();

        if (sAnswer.length() == 0) {
          msAnswer.setError("This Field Is Required");
        } else {

          Data c = DataLab.get(SecurityQnA.this).getDetails();
          Surname = c.getSurname();
          firstname = c.getFirstname();
          Mobile_no = c.getMobile_no();
          Password = c.getPassword();
          IMEI = c.getImei();
          Token = c.getToken();
          Email = c.getEmail();
          Gender = c.getGender();

          String loginwith_securityquestion_url = null;
          try {

            loginwith_securityquestion_url = "method=loginwithsecurityquestion&msisdn=" + Mobile_no
                + "&password=" + Password + "&autcode=" + IMEI + "&device=" + "android"
                + "&securityanswer=" + URLEncoder.encode(sAnswer, "UTF-8")
                + "&version=" + App_Version;
          } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
          }

          if (connChecker.isAvailable()) {

            _task = new loginwith_security_Task();
            _task.execute(loginwith_securityquestion_url);
          } else {
            Toast.makeText(SecurityQnA.this, "No Connection To The Internet", Toast.LENGTH_LONG)
                .show();
          }

        }
      }
    });
  }

  private class loginwith_security_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(SecurityQnA.this, "Please wait a moment");
      Pd.showProgress_Title();
    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(SecurityQnA.this);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      if (result.contains("8001") == true) {

        User _user = new User(firstname, Surname, Mobile_no, Password, IMEI, Token, Email, Gender);

        Reg_DatabaseHandler db = new Reg_DatabaseHandler(SecurityQnA.this);
        db.addUser(_user);

        Pd.dismissProgress(); // dismiss progress dialog

        Intent i = new Intent(SecurityQnA.this, Login_Activity.class);
        startActivity(i);
        Toast.makeText(SecurityQnA.this, "Welcome", Toast.LENGTH_LONG).show();

        finish(); // kill this activity

      } else if (result.contains("8002") == true) {
        Pd.dismissProgress(); // dismiss progress dialog

        new Dialog(SecurityQnA.this, "Invalid Credentials");
      } else {
        Pd.dismissProgress(); // dismiss progress dialog

        new Dialog(SecurityQnA.this, result);
      }
    }
  }

  @Override
  protected void onDestroy() {
    super.onDestroy();
    if (_task != null) {

      if (_task.getStatus() != AsyncTask.Status.FINISHED) {
        _task.cancel(true);
      }
      _task = null;
    }
  }

}

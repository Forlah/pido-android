package com.paycom.utitlities;

import android.content.Context;
import android.os.AsyncTask;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Sch_Fees {

  Context context;

  public Sch_Fees(Context c, String _uri) {
    context = c;

    new SchoolFees_Task().execute(_uri);
  }

  private class SchoolFees_Task extends AsyncTask<String, Void, String> {

    Progressdialog Pd;

    @Override
    protected void onPreExecute() {
      super.onPreExecute();
      Pd = new Progressdialog(context, "Please Wait..");
      Pd.showProgress();

    }

    @Override
    protected String doInBackground(String... params) {
      String output = "";
      // params comes from the execute() call: params[0] is the url.
      try {
        ConnectionToService c = new ConnectionToService(params[0]);
        output = c.download_secure_URL(context);
        return output;
      } catch (Exception e) {
        e.printStackTrace();
        return "Unable to connect to the service at the moment.Please try again later";
      }

    }

    @Override
    protected void onPostExecute(String result) {
      super.onPostExecute(result);
      //Log.d(Tag,"Result @ postExecute of airtimetop is: "+result);
      Pd.dismissProgress();
      new Dialog(context, result);

    }
  }

}

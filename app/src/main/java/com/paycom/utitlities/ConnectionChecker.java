package com.paycom.utitlities;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

/**
 * Created by sp_developer on 11/28/15.
 */
public class ConnectionChecker {

  private Context context;

  public ConnectionChecker(Context c) {

    context = c;
  }

  public boolean isAvailable() {
    ConnectivityManager conMgr = (ConnectivityManager) context
        .getSystemService(Context.CONNECTIVITY_SERVICE);
    NetworkInfo networkInfo = conMgr.getActiveNetworkInfo();
    if (networkInfo != null && networkInfo.isConnected()) {
      return true;
    } else {
      return false;
    }
  }

}

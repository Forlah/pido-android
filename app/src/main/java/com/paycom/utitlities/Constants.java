package com.paycom.utitlities;

/**
 * Created by sp_developer on 11/18/16.
 */
public class Constants {

  private static final String BASE = "http://62.173.41.6:8080/AbiaTMaxMobile/webresources/abiaTIMASS/";
  public static final String verifyBookingURL = BASE + "verifyBook?";
  public static final String verifyPaymentURL = BASE + "verifyPayment?";

  // public static final String fundWalletURL = "http://10.10.10.11:8080/FlutterServices/accountRequest?sRequest=";
  public static final String fundWalletURL = "http://62.173.41.6:8080/FlutterServices/accountRequest?sRequest=";

}

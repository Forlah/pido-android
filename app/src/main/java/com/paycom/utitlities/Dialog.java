package com.paycom.utitlities;

import android.content.Context;
import android.support.v7.app.AlertDialog;
import com.paycom.app.R;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Dialog {

  Context mContext;
  AlertDialog.Builder builder;

  // Empty constructor for accessing class
  public Dialog() {

  }

  public Dialog(Context context, String msg) {
    mContext = context;
    builder = new AlertDialog.Builder(mContext);
    builder.setTitle("RESPONSE");
    builder.setMessage(msg);
    builder.setPositiveButton("OK", null);
    builder.show();
  }

  public void error_Dialog(Context context, String msg, String errorString) {
    builder = new AlertDialog.Builder(context);
    builder.setTitle(errorString);
    builder.setMessage(msg);
    builder.setIcon(R.mipmap.fail);
    builder.setPositiveButton("OK", null);
    builder.show();
  }

  public void show_alert(Context context, String msg, String title) {

    builder = new AlertDialog.Builder(context);
    builder.setTitle(title);
    builder.setMessage(msg);
    builder.setIcon(R.mipmap.fail);
    builder.setPositiveButton("OK", null);
    builder.show();
  }

}

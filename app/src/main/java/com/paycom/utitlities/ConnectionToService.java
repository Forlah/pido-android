package com.paycom.utitlities;

import android.content.Context;
import android.util.Log;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.security.KeyStore;
import java.security.cert.Certificate;
import java.security.cert.CertificateFactory;
import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.TrustManagerFactory;

/**
 * Created by sp_developer on 11/28/15.
 */
public class ConnectionToService {

  //public static final String Base_Url = "http://62.173.41.6:8081/paycommobilemoney_new/platform/pido2.ashx?";
  // public static final String Base_Url = "http://10.10.10.15:8081/paycommobilemoney_new/platform/pido2.ashx?";

  //public static final String Base_Url = "https://10.10.10.15:8443/platform/pido2.ashx?";
  public static final String Base_Url = "https://62.173.41.6:8443/platform/pido2.ashx?";

  public static final String BaseURL_ = "http://62.173.41.6:8080/PayATMCardless/webresources/ATMCardless";
  //public static final String BaseURL_ = "http://10.10.10.11:8080/PayATMCardless/webresources/ATMCardless";

  private String Url;
  private String Built_Url;
  private static final String DEBUG_TAG = "connection_To";
  public static final int CONNECTION_TIME_OUT = 8000; // just for kedc nameenquiry

  private static final int SOCKET_TIME_OUT = 5200;

  private static final String isSelfSigned = "True";
  //private static final String isSelfSigned = "false";

  public ConnectionToService(String _url) {
    Url = _url;
    Built_Url = Base_Url.trim() + Url.trim();

  }

  public ConnectionToService() {

  }

  // Given a URL, establishes an HttpUrlConnection and retrieves
// the web page content as a InputStream, which it returns as
// a string.
//    public String downloadUrl() throws IOException {
//        InputStream is = null;
//        // Only display the first 500 characters of the retrieved
//        // web page content.
//        int len = 5000;
//        StringBuilder sb = new StringBuilder();
//
//        try {
//            //String encoded_url = URLEncoder.encode(Built_Url, "UTF-8").toString();
//            URL url = new URL(Built_Url);
//            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
//            conn.setDoOutput(true);
//            conn.setRequestMethod("POST");
//            conn.setConnectTimeout(SOCKET_TIME_OUT); /* milliseconds */
//
//
//            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
//            // this is where we're adding post data to the request
//            wr.write(sb.toString());
//            wr.flush();
//            is = conn.getInputStream();
//         //   Log.d(DEBUG_TAG,"Output of conn.getInputStream = "+is);
//            wr.close();
//
//            //conn.setDoInput(true);
//            // Starts the query
//            conn.connect();
//            int response = conn.getResponseCode();
//         //   Log.d(DEBUG_TAG, "The response is: " + response);
//             //Convert the InputStream into a string
//            String contentAsString = readIt(is, len);
//           // Log.d(DEBUG_TAG, "The response output string is: " + contentAsString);
//
//            return contentAsString;
//
//            // Makes sure that the InputStream is closed after the app is
//            // finished using it.
//        } finally {
//            if (is != null) {
//                is.close();
//            }
//        }
//    }

  // Given a URL, establishes an HttpsUrlConnection  and retrieves
// the web page content as a InputStream, which it returns as
// a string.
  public String download_secure_URL(Context context) throws Exception {

    InputStream is = null;
    // Only display the first 500 characters of the retrieved
    // web page content.
    int len = 5000;
    StringBuilder sb = new StringBuilder();
    String contentAsString = "";

    try {

      switch (isSelfSigned) {

        case "True":

          // Because server has a self signed certificate, we read and cert file and create trust authority
          CertificateFactory cf = CertificateFactory.getInstance("X.509");
          InputStream caInput = new BufferedInputStream(context.getAssets().open("pserver.crt"));
          Certificate ca = cf.generateCertificate(caInput);

          // create a keystore containing our trusted CAs
          String keyStoreType = KeyStore.getDefaultType();
          KeyStore keyStore = KeyStore.getInstance(keyStoreType);
          keyStore.load(null, null);
          keyStore.setCertificateEntry("ca", ca);

          // Create a TrustManager that trusts the CAs in our keystore
          String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
          TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
          tmf.init(keyStore);

          //create an SSLcontext that uses our Trustmanager
          SSLContext context_ = SSLContext.getInstance("TLS");
          context_.init(null, tmf.getTrustManagers(), null);

          // Skip all host name verifications, i trust myself
          HostnameVerifier hostnameVerifier = new HostnameVerifier() {
            @Override
            public boolean verify(String hostname, SSLSession session) {
              return true;
            }
          };

          URL url = new URL(Built_Url);
          HttpsURLConnection conn = (HttpsURLConnection) url.openConnection();
          conn.setSSLSocketFactory(context_.getSocketFactory());
          conn.setHostnameVerifier(hostnameVerifier);
          conn.setDoOutput(true);
          conn.setRequestMethod("POST");
          conn.setConnectTimeout(SOCKET_TIME_OUT); /* milliseconds */

          OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
          // this is where we're adding post data to the request
          wr.write(sb.toString());
          wr.flush();
          is = conn.getInputStream();
          //   Log.d(DEBUG_TAG,"Output of conn.getInputStream = "+is);
          wr.close();

          //conn.setDoInput(true);
          // Starts the query
          conn.connect();
          int response = conn.getResponseCode();
          //   Log.d(DEBUG_TAG, "The response is: " + response);
          //Convert the InputStream into a string
          contentAsString = readIt(is, len);

          break;

        case "false":

          URL _url = new URL(Built_Url);

          HttpURLConnection _conn = (HttpURLConnection) _url.openConnection();
          _conn.setDoOutput(true);
          _conn.setRequestMethod("POST");
          _conn.setConnectTimeout(SOCKET_TIME_OUT); /* milliseconds */

          OutputStreamWriter _wr = new OutputStreamWriter(_conn.getOutputStream());
          // this is where we're adding post data to the request
          _wr.write(sb.toString());
          _wr.flush();
          is = _conn.getInputStream();
          //   Log.d(DEBUG_TAG,"Output of conn.getInputStream = "+is);
          _wr.close();

          //conn.setDoInput(true);
          // Starts the query
          _conn.connect();
          int _response = _conn.getResponseCode();
          //   Log.d(DEBUG_TAG, "The response is: " + response);
          //Convert the InputStream into a string
          contentAsString = readIt(is, len);

          break;

      }

    } finally {
      if (is != null) {
        is.close();
      }
    }

    return contentAsString;

  }

  // Reads an InputStream and converts it to a String.
  private String readIt(InputStream stream, int len) throws IOException {
    Reader reader = null;
    reader = new InputStreamReader(stream, "UTF-8");
    char[] buffer = new char[len];
    reader.read(buffer);
    return new String(buffer);
  }

  public String PostURL_with_header(String json_data) throws IOException {

    HttpURLConnection connection = null;

    try {

      URL url = new URL(BaseURL_ + "/generateToken");
      connection = (HttpURLConnection) url.openConnection();
      connection.setDoOutput(true);
      connection.setRequestMethod("POST");

      // set the sending type and receiving type to json and string respectively
      connection.setRequestProperty("Content-Type", "application/json");
      connection.setRequestProperty("Accept", "text/plain");

      connection.setConnectTimeout(SOCKET_TIME_OUT); // timeout in milli secs

      if (json_data != null) {

        // set the content length of the body
        connection.setRequestProperty("Content-length", json_data.getBytes().length + "");
        connection.setDoOutput(true);
        connection.setDoInput(true);
        connection.setUseCaches(false);

        // send the json as body of the request
        OutputStream outputStream = connection.getOutputStream();
        outputStream.write(json_data.getBytes("UTF-8"));
        outputStream.close();
      }

      // Connect to the server
      connection.connect();

      int status = connection.getResponseCode();
      // System.out.println("The response code = "+status);
      switch (status) {

        case 200:
        case 201:
          BufferedReader bufferedReader = new BufferedReader(
              new InputStreamReader(connection.getInputStream()));
          StringBuilder stringBuilder = new StringBuilder();
          String line;
          while ((line = bufferedReader.readLine()) != null) {

            stringBuilder.append(line + "\n");

          }
          bufferedReader.close();
          Log.d(DEBUG_TAG, "The response output string is: " + stringBuilder.toString());

          return stringBuilder.toString();
      }

    } finally {
      if (connection != null) {
        try {
          connection.disconnect();
        } catch (Exception ex) {
          Log.e("HTTP Client", "Error in http connection" + ex.toString());
        }
      }
    }

    return "Connection Timed Out";

  }

  public String POST_GET_URL(String safeurl, String method) throws IOException {

    String contentAsString;

    switch (method) {

      case "GET":

        try {

          BufferedReader reader;
          // Only display the first 500 characters of the retrieved
          // web page content.
          StringBuilder sb;

          URL url = new URL(BaseURL_ + safeurl);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setRequestMethod("GET");
          conn.setDoOutput(false);
          conn.setConnectTimeout(SOCKET_TIME_OUT); /* milliseconds */
          conn.connect();
          int response = conn.getResponseCode();

          // read the output from the server
          reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
          sb = new StringBuilder();

          String line;
          while ((line = reader.readLine()) != null) {
            sb.append(line);
          }

          reader.close();

          contentAsString = sb.toString();

          return contentAsString;

        } catch (Exception e) {
          e.printStackTrace();
        }

      case "POST":

        InputStream is = null;
        // Only display the first 500 characters of the retrieved
        // web page content.
        int len = 5000;
        StringBuilder sb = new StringBuilder();

        try {
          //String encoded_url = URLEncoder.encode(Built_Url, "UTF-8").toString();
          URL url = new URL(BaseURL_ + safeurl);
          HttpURLConnection conn = (HttpURLConnection) url.openConnection();
          conn.setDoOutput(true);
          conn.setRequestMethod("POST");
          conn.setConnectTimeout(SOCKET_TIME_OUT); /* milliseconds */

          OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
          // this is where we're adding post data to the request
          wr.write(sb.toString());
          wr.flush();
          is = conn.getInputStream();
          //   Log.d(DEBUG_TAG,"Output of conn.getInputStream = "+is);
          wr.close();

          //conn.setDoInput(true);
          // Starts the query
          conn.connect();
          int response = conn.getResponseCode();
          //   Log.d(DEBUG_TAG, "The response is: " + response);
          //Convert the InputStream into a string
          contentAsString = readIt(is, len);
          // Log.d(DEBUG_TAG, "The response output string is: " + contentAsString);

          return contentAsString;

          // Makes sure that the InputStream is closed after the app is
          // finished using it.
        } finally {
          if (is != null) {
            is.close();
          }
        }

    }

    return "Connection Timed Out";

  }

}

package com.paycom.utitlities;

import android.util.Log;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.Reader;
import java.net.HttpURLConnection;
import java.net.URL;
import org.json.JSONArray;

/**
 * Created by sp_developer on 11/16/16.
 */
public class ConnectionToService_TMAS {

  private static final String Tag = "connection_To_json";

//    String json_result = "[\n" +
//            "  {\n" +
//            "    \"offenceType\": \"salon car\",\n" +
//            "    \"listOffence\": [ \"parking permit\" ,\"one way driving\", \"missing faulty parts\", \"vehicle licence renewal\" ]\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"offenceType\": \"bus\",\n" +
//            "    \"listOffence\": [ \"parking permit 1\" ,\"one way driving 1\", \"missing faulty parts 1\", \"vehicle licence renewal 1\" ]\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"offenceType\": \"lorry\",\n" +
//            "    \"listOffence\": [ \"parking permit 2\" ,\"one way driving 2\", \"missing faulty parts 2\", \"vehicle licence renewal 2\" ]\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"offenceType\": \"truck\",\n" +
//            "    \"listOffence\": [ \"parking permit 3\" ,\"one way driving 3\", \"missing faulty parts 3\", \"vehicle licence renewal 3\" ]\n" +
//            "  },\n" +
//            "  {\n" +
//            "    \"offenceType\": \"others\",\n" +
//            "    \"listOffence\": [ \"parking permit 4\" ,\"one way driving 4\", \"missing faulty parts 4\", \"vehicle licence renewal 4\" ]\n" +
//            "  }\n" +
//            "]";

  public JSONArray getJSONfromUrl(String safeUrl) throws IOException {
    // JSONArray jsonArray = null;
    JSONArray _jsonArray = null;

    try {

      InputStream is = null;
      BufferedReader reader = null;
      // Only display the first 500 characters of the retrieved
      // web page content.
      StringBuilder sb;

      URL url = new URL(safeUrl);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setRequestMethod("GET");
      conn.setDoOutput(false);
      conn.setConnectTimeout(3000); /* milliseconds */
      conn.connect();
      int response = conn.getResponseCode();

      // read the output from the server
      reader = new BufferedReader(new InputStreamReader(conn.getInputStream()));
      sb = new StringBuilder();

      String line;
      while ((line = reader.readLine()) != null) {
        sb.append(line);
      }

      reader.close();

      // create a JSON object hierarchy from the result
      _jsonArray = new JSONArray(sb.toString());

      Log.d(Tag, "my json array = " + _jsonArray.toString());

    } catch (Exception e) {
      e.printStackTrace();
      Log.e(Tag, "Cannot process JSON results", e);
    }

    return _jsonArray;
  }

  public String downloadUrl(String safeUrl) throws IOException {
    InputStream is = null;
    // Only display the first 500 characters of the retrieved
    // web page content.
    int len = 5000;
    StringBuilder sb = new StringBuilder();

    try {
      URL url = new URL(safeUrl);
      HttpURLConnection conn = (HttpURLConnection) url.openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod("POST");
      conn.setConnectTimeout(2300); /* milliseconds */

      OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
      // this is where we're adding post data to the request
      wr.write(sb.toString());
      wr.flush();
      is = conn.getInputStream();
      // Log.d(Tag,"Output of conn.getInputStream = "+is);
      wr.close();

      //conn.setDoInput(true);
      // Starts the query
      conn.connect();
      int response = conn.getResponseCode();
      //Log.d(Tag, "The response is: " + response);
      //Convert the InputStream into a string
      String contentAsString = readIt(is, len);
      // Log.d(Tag, "The response output string is: " + contentAsString);

      return contentAsString;

      // Makes sure that the InputStream is closed after the app is
      // finished using it.
    } finally {
      if (is != null) {
        is.close();
      }
    }
  }

  // Reads an InputStream and converts it to a String.
  private String readIt(InputStream stream, int len) throws IOException {
    Reader reader = null;
    reader = new InputStreamReader(stream, "UTF-8");
    char[] buffer = new char[len];
    reader.read(buffer);
    return new String(buffer);
  }

}

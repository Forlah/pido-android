package com.paycom.utitlities;

import android.util.Base64;
import java.io.UnsupportedEncodingException;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Encryption {

  private static String text_string = "";

  public Encryption() {
  }

  /*  public  static String  toByte(String url){
        byte[] str  = url.getBytes();
        byte[] str2 = Base64.encode(str, Base64.DEFAULT);
                System.out.println(" hi " + str2.toString());
        return  str2.toString();
    }*/

  public static String Base64Encoder(String str) {
    text_string = str; // string to be encoded with Base64
    byte[] data = null; // string conversion to bytes
    String _string = null;
    String encoded_string = null;

    try {
      data = text_string.getBytes("UTF-8");
      _string = Base64.encodeToString(data, Base64.DEFAULT).toString();
      encoded_string = _string.replace("=", " ").replace("_", "/");
      // encoded_string =  _string;

    } catch (UnsupportedEncodingException e) {
      e.printStackTrace();
    }

    return encoded_string.trim();

  }

}

package com.paycom.utitlities;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Progressdialog {

  private Context context;
  private String message;
  private ProgressDialog pd;

  public Progressdialog(Context c, String str) {
    context = c;
    message = str;
  }

  public Progressdialog() {

  }

  public void showProgress() {
    pd = new ProgressDialog(context, AlertDialog.THEME_DEVICE_DEFAULT_LIGHT);
    pd.setMessage(message);
    pd.setTitle("Processing");
    pd.setCanceledOnTouchOutside(false);
    // pd.setProgressStyle(AlertDialog.THEME_HOLO_DARK);
    pd.show();
  }

  public void showProgress_Title() {
    pd = new ProgressDialog(context, AlertDialog.THEME_HOLO_LIGHT);
    pd.setMessage(message);
    pd.setTitle("Processing");
    pd.setCanceledOnTouchOutside(false);
    // pd.setProgressStyle(AlertDialog.THEME_HOLO_DARK);
    pd.show();
  }

  public void dismissProgress() {
    pd.dismiss();
  }

}

package com.paycom.DBhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import android.widget.Toast;
import com.paycom.model.Contact;
import java.util.ArrayList;

/**
 * Created by sp_developer on 11/28/15.
 */
public class DatabaseHandler extends SQLiteOpenHelper {

  public static final String DATABASE_NAME = "ContactsManager";
  public static final String DATABASE_TABLE_NAME = "Pidocontacts";
  public static final int DATABASE_VERSION = 1;
  private static final String Tag = "Database";
  private Context mContext;

  // contacts table column names
  public static final String KEY_ID = "id";
  public static final String KEY_SURNAME = "SURNAME";
  public static final String KEY_FIRSTNAME = "FIRST_NAME";
  public static final String KEY_PHONE_NO = "PHONE_NUMBER";
  public static final String KEY_BANK = "BANK";
  public static final String KEY_ACCOUNT_NO = "ACCOUNT_NUMBER";

  public DatabaseHandler(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    mContext = context;
  }

  @Override
  public void onCreate(SQLiteDatabase Db) {
    StringBuilder stringBuilder = new StringBuilder("CREATE TABLE ");
    stringBuilder.append(DATABASE_TABLE_NAME + " (" + KEY_ID);
    stringBuilder.append(" INTEGER PRIMARY KEY, ");
    stringBuilder.append(KEY_SURNAME + " TEXT, ");
    stringBuilder.append(KEY_FIRSTNAME + " TEXT, ");
    stringBuilder.append(KEY_PHONE_NO + " TEXT, ");
    stringBuilder.append(KEY_BANK + " TEXT, ");
    stringBuilder.append(KEY_ACCOUNT_NO + " TEXT );");

    Log.d(Tag, "the sql statement " + stringBuilder.toString());
    String CREATE_CONTACTS_TABLE = stringBuilder.toString();
    Db.execSQL(CREATE_CONTACTS_TABLE);

  }

  @Override
  public void onUpgrade(SQLiteDatabase Db, int oldVersion, int newVersion) {

    Db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_NAME);

    // create table again
    onCreate(Db);

  }

    /*
       All CRUD (Create, Read, Update, Delete) Operation
     */

  // Adding new contact
  public void addContact(Contact contact) {

    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();
    Log.d(Tag, "account no is  :" + contact.getAccountnumber());
    Log.d(Tag, "Phone number is :" + contact.getPhonenumber());
    values.put(KEY_FIRSTNAME, contact.getFirstname());
    values.put(KEY_SURNAME, contact.getSurname());
    values.put(KEY_BANK, contact.getBank());
    values.put(KEY_ACCOUNT_NO, contact.getAccountnumber());
    values.put(KEY_PHONE_NO, contact.getPhonenumber());

    // Inserting Row
    db.insert(DATABASE_TABLE_NAME, null, values);
    db.close();

    Toast.makeText(mContext, "Contact Added Successfully", Toast.LENGTH_LONG).show();

  }

    /*
      This is another addContact method but it collects both name and surname after a bank transaction
      has been completed successfully
     */

  public void addContactToDb(Contact c) {
    SQLiteDatabase db = this.getWritableDatabase();
    ContentValues values = new ContentValues();

    values.put(KEY_SURNAME, "");
    values.put(KEY_FIRSTNAME, c.getFirstname());
    values.put(KEY_BANK, c.getBank());
    values.put(KEY_ACCOUNT_NO, c.getAccountnumber());
    values.put(KEY_PHONE_NO, c.getPhonenumber());

    // Inserting Row
    db.insert(DATABASE_TABLE_NAME, null, values);
    db.close();
  }


  /*  // Getting a single contact
    public Contact getContact(int id){
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.query(DATABASE_TABLE_NAME, new String[]{KEY_ID, KEY_NAME,KEY_PHONE_NO},KEY_ID + "=?",
                new String[]{String.valueOf(id)},null,null,null,null);
        if(cursor != null){
            cursor.moveToNext();
        }

        Contact contact = new Contact(Integer.parseInt(cursor.getString(0)),cursor.getString(1), cursor.getString(2));

        return  contact;
    } */

  public ArrayList<Contact> getAllContacts() {
    ArrayList<Contact> contactList = new ArrayList<Contact>();
    // Select All Query
    String selectQuery = "SELECT * FROM " + DATABASE_TABLE_NAME;

    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToNext()) {
      do {
        Contact contact = new Contact();

        contact.setId(Integer.parseInt(cursor.getString(0)));
        contact.setSurname(cursor.getString(1));
        contact.setFirstname(cursor.getString(2));
        contact.setPhonenumber(cursor.getString(3));
        contact.setBank(cursor.getString(4));
        contact.setAccountnumber(cursor.getString(5));

        // Adding contact to list
        contactList.add(contact);

      } while (cursor.moveToNext());
    }
    return contactList;
  }

  // Updating single contact
  public int updateContact(Contact contact, int id) {
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues values = new ContentValues();
    values.put(KEY_FIRSTNAME, contact.getFirstname());
    values.put(KEY_SURNAME, contact.getSurname());
    values.put(KEY_BANK, contact.getBank());
    values.put(KEY_ACCOUNT_NO, contact.getAccountnumber());
    values.put(KEY_PHONE_NO, contact.getPhonenumber());

    // updating row
    return db.update(DATABASE_TABLE_NAME, values, KEY_ID + " = " + id, null);
  }

  // Deleting single contact
  public void deleteContact(Contact contact) {
    SQLiteDatabase db = this.getWritableDatabase();
    db.delete(DATABASE_TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(contact.getId())});
    db.close();
  }

  // Getting contact Count
  public int getContactsCount() {
    String countQuery = "SELECT * FROM " + DATABASE_TABLE_NAME;
    SQLiteDatabase db = DatabaseHandler.this.getReadableDatabase();
    Cursor cursor = db.rawQuery(countQuery, null);
    cursor.close();

    return cursor.getCount();
  }

  //checks if data already exists in the Db or not
  public boolean isExistingInDB(String fieldValue) {
    String[] selectionArgs = {fieldValue};
    boolean isEntryPresent = false;
    String queryString =
        "SELECT * FROM " + DATABASE_TABLE_NAME + " WHERE " + KEY_ACCOUNT_NO + " = ?";
    SQLiteDatabase db = DatabaseHandler.this.getReadableDatabase();
    Cursor cursor = db.rawQuery(queryString, selectionArgs);
    if (cursor.moveToFirst()) {
      isEntryPresent = true;
    }

    cursor.close();
    return isEntryPresent;
  }

}

package com.paycom.DBhelper;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;
import com.paycom.model.User;
import java.util.ArrayList;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Reg_DatabaseHandler extends SQLiteOpenHelper {

  public static final String DATABASE_NAME = "RegistrationManager";
  public static final String DATABASE_TABLE_NAME = "PidoUser";
  public static final int DATABASE_VERSION = 1;
  private static final String Tag = "Database";
  private Context mContext;

  // contacts table column names
  public static final String KEY_ID = "id";
  public static final String KEY_FULLNAME = "FULL_NAME";
  public static final String KEY_MOBILE_NO = "MOBILE";
  public static final String KEY_PASSWORD = "PASSWORD";
  public static final String KEY_IMEI = "IMEI";
  public static final String KEY_TOKEN = "TOKEN";
  public static final String KEY_GENDER = "GENDER";
  public static final String KEY_EMAIL = "EMAIL";

  public Reg_DatabaseHandler(Context context) {
    super(context, DATABASE_NAME, null, DATABASE_VERSION);
    mContext = context;
  }

  /*  public  String  CREATE_TABLE_NEW = "CREATE TABLE " + DATABASE_TABLE_NAME + " ( id  INTEGER AUTO INCREMENT PRIMARY KEY,"
                                        + KEY_FULLNAME + " TEXT, "
                                        + KEY_MOBILE_NO  + " TEXT, "+KEY_IMEI
                                        +" TEXT," + KEY_PASSWORD +" TEXT,"
                                        + KEY_TOKEN + " TEXT,"
                                        + KEY_GENDER + " TEXT,"
                                        + KEY_EMAIL + " TEXT" + ")" ; */

  @Override
  public void onCreate(SQLiteDatabase db) {
    String CREATE_TABLE_NEW = "CREATE TABLE " + DATABASE_TABLE_NAME + " ( id INTEGER PRIMARY KEY,"
        + KEY_FULLNAME + " TEXT, "
        + KEY_MOBILE_NO + " TEXT, "
        + KEY_PASSWORD + " TEXT, "
        + KEY_IMEI + " TEXT, "
        + KEY_TOKEN + " TEXT, "
        + KEY_GENDER + " TEXT, "
        + KEY_EMAIL + " TEXT" + ")";

    Log.d(Tag, "the sql statement " + CREATE_TABLE_NEW);
    // String CREATE_CONTACTS_TABLE = stringBuilder.toString();
    System.out.println(CREATE_TABLE_NEW);
    db.execSQL(CREATE_TABLE_NEW);

  }

  @Override
  public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    db.execSQL("DROP TABLE IF EXISTS " + DATABASE_TABLE_NAME);

    // create table again
    onCreate(db);
  }

  public long addUser(User user) {

    //SQLiteDatabase db = this.getWritableDatabase();
    SQLiteDatabase db = this.getWritableDatabase();

    ContentValues values = new ContentValues();
    values.put(KEY_FULLNAME, user.getFullname());
    values.put(KEY_MOBILE_NO, user.getMobile_no());
    values.put(KEY_PASSWORD, user.getPassword());
    values.put(KEY_IMEI, user.getImei());
    values.put(KEY_TOKEN, user.getToken());
    values.put(KEY_GENDER, user.getGender());
    values.put(KEY_EMAIL, user.getEmail());

    // Inserting Row
    long x = db.insert(DATABASE_TABLE_NAME, null, values);
    db.close();

    return x;

  }

  //checks if data already exists in the Db or not
  public boolean DbExists() {
    boolean out;
    String queryString = "SELECT * FROM " + DATABASE_TABLE_NAME;
    System.out.println(queryString);
    SQLiteDatabase db = Reg_DatabaseHandler.this.getReadableDatabase();
    Cursor cursor = db.rawQuery(queryString, null);
    System.out.println("The cursor count is  " + cursor.getCount());
    if (cursor.getCount() <= 0) {
      out = false;
      cursor.close();

    } else {
      out = true;
      cursor.close();
    }

    return out;
  }

  public String getMobileNo() {
    String phone = "";
    String queryString = "SELECT MOBILE FROM " + DATABASE_TABLE_NAME;
    System.out.println(queryString);
    SQLiteDatabase db = Reg_DatabaseHandler.this.getReadableDatabase();
    Cursor cursor = db.rawQuery(queryString, null);
    if (cursor.moveToNext()) {
      phone = cursor.getString(0);
      cursor.close();
    }

    return phone;
  }

  public ArrayList<User> getAll() {
    ArrayList<User> user = new ArrayList<>();
    // Select All Query
    String selectQuery = "SELECT * FROM " + DATABASE_TABLE_NAME;

    SQLiteDatabase db = this.getWritableDatabase();
    Cursor cursor = db.rawQuery(selectQuery, null);

    // looping through all rows and adding to list
    if (cursor.moveToNext()) {
      do {
        User _user = new User();

        _user.setId(Integer.parseInt(cursor.getString(0)));
        _user.setFullname(cursor.getString(1));
        _user.setMobile_no(cursor.getString(2));
        _user.setPassword(cursor.getString(3));
        _user.setImei(cursor.getString(4));
        _user.setToken(cursor.getString(5));
        _user.setGender(cursor.getString(6));
        _user.setEmail(cursor.getString(7));

        // Adding contact to list
        user.add(_user);

      } while (cursor.moveToNext());
    }
    //cursor.close();
    return user;
  }

  public long deleteUser(int id) {
    SQLiteDatabase db = this.getWritableDatabase();
    long x = db.delete(DATABASE_TABLE_NAME, KEY_ID + " = ?", new String[]{String.valueOf(id)});
    db.close();

    return x;
  }

  // Updating single user pido password
  public long updateUser(int id, String db_fullname, String db_mobileNO, String NewPassword,
      String db_imei, String db_token, String db_email, String db_gender) {
    long x = 0;
    try {
      SQLiteDatabase db = this.getWritableDatabase();
      ContentValues values = new ContentValues();
      values.put(KEY_FULLNAME, db_fullname);
      values.put(KEY_MOBILE_NO, db_mobileNO);
      values.put(KEY_PASSWORD, NewPassword);
      values.put(KEY_IMEI, db_imei);
      values.put(KEY_TOKEN, db_token);
      values.put(KEY_GENDER, db_gender);
      values.put(KEY_EMAIL, db_email);

      // updating row
      x = db.update(DATABASE_TABLE_NAME, values, KEY_ID + " = " + id, null);
    } catch (Exception ex) {
      ex.printStackTrace();
    }
    return x;
  }

}

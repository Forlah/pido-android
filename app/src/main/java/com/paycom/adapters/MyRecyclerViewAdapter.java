package com.paycom.adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.TextView;
import com.paycom.app.AirTime_Topup;
import com.paycom.app.CardlessWithdrawal_Activity;
import com.paycom.app.CashOut_Activity;
import com.paycom.app.ChangePin_Activity;
import com.paycom.app.Contact_Activity;
import com.paycom.app.FundWallet_Activity;
import com.paycom.app.PayBills_Activity;
import com.paycom.app.R;
import com.paycom.app.TransactionHistory_Activity;
import com.paycom.app.Transfer_money;

/**
 * Created by sp_developer on 11/28/15.
 */
public class MyRecyclerViewAdapter extends
    RecyclerView.Adapter<MyRecyclerViewAdapter.DataObjectHolder> {

  private static String LOG_TAG = "MyRecyclerViewAdapter";
  private String[] str;
  private int[] image;
  Context context;
  // private static MyClickListener myClickListener;
  public static AdapterView.OnItemClickListener mItemClickListener;

  // menu item row class
  public static class DataObjectHolder extends RecyclerView.ViewHolder {

    private TextView menu_type;
    private TextView label;
    private ImageView mImageview;

    public View mView = null;

    // class constructor
    public DataObjectHolder(View itemView) {
      super(itemView);
      mView = itemView;
      menu_type = (TextView) itemView.findViewById(R.id.title);
      label = (TextView) itemView.findViewById(R.id.desc);
      mImageview = (ImageView) itemView.findViewById(R.id.list_image);

    }

  }

  // pido menu recycler adapter constructor
  public MyRecyclerViewAdapter(String[] text, Context context, int[] image) {
    str = text;
    this.image = image;
    this.context = context;
  }

  @Override
  public DataObjectHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {
    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.menu_row_adapter, parent, false);

    DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
    return dataObjectHolder;
  }

  @Override
  public void onBindViewHolder(MyRecyclerViewAdapter.DataObjectHolder holder, final int position) {

    holder.menu_type.setText(str[position]);
    holder.mImageview.setImageResource(image[position]);

    if ("Airtime Topup".equals(str[position])) {

      holder.label.setText("Recharge MTN, Airtel, Etisalat and Glo airtime");

    } else if ("Transfer Money".equals(str[position])) {

      holder.label.setText("To Pido, to bank or other mobile money");

    } else if ("Cash Out".equals(str[position])) {

      holder.label.setText("Receive cash from a nearby agent");
    } else if ("Cardless Withdrawal".equals(str[position])) {

      holder.label.setText("Withdraw cash from a nearby ATM terminal");

    } else if ("Fund PiDO Wallet".equals(str[position])) {

      holder.label.setText("Fund your pido wallet via atm cards");
    } else if ("Pay Bills".equals(str[position])) {
      holder.label.setText("Pay for goods and services");
    } else if ("Change Pin".equals(str[position])) {
      holder.label.setText("Change your PiDO pin");
    } else if ("PiDO Contact".equals(str[position])) {
      holder.label.setText("Manage or send money to contacts");
    } else {
      holder.label.setText("Check all previous transactions");
    }

    holder.mView.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {
        if (str[position].equals("Airtime Topup")) {
          Intent i = new Intent(context, AirTime_Topup.class);
          context.startActivity(i);
        } else if (str[position].equals("Transfer Money")) {
          Intent i = new Intent(context, Transfer_money.class);
          context.startActivity(i);
        } else if (str[position].equals("Cash Out")) {

          Intent i = new Intent(context, CashOut_Activity.class);
          context.startActivity(i);
        } else if (str[position].equals("Cardless Withdrawal")) {

          Intent i = new Intent(context, CardlessWithdrawal_Activity.class);
          context.startActivity(i);
        } else if (str[position].equals("Fund PiDO Wallet")) {

          Intent i = new Intent(context, FundWallet_Activity.class);
          context.startActivity(i);
        } else if (str[position].equals("Pay Bills")) {

          Intent i = new Intent(context, PayBills_Activity.class);
          context.startActivity(i);
        } else if (str[position].equals("Change Pin")) {

          Intent i = new Intent(context, ChangePin_Activity.class);
          context.startActivity(i);
        } else if (str[position].equals("PiDO Contact")) {
          Intent i = new Intent(context, Contact_Activity.class);
          context.startActivity(i);
        } else {
          Intent i = new Intent(context, TransactionHistory_Activity.class);
          context.startActivity(i);
        }

      }
    });

  }

  @Override
  public int getItemCount() {
    return str.length;
  }
}

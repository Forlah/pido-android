package com.paycom.adapters;

import android.content.Context;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.paycom.app.R;
import com.paycom.model.Booking;
import java.util.ArrayList;

/**
 * Created by sp_developer on 1/13/17.
 */
public class VerifyBookings_RecyclerAdapter extends
    RecyclerView.Adapter<VerifyBookings_RecyclerAdapter.DataObjectHolder> {

  private ArrayList<Booking> Items;
  private Context context;

  public VerifyBookings_RecyclerAdapter(Context context, ArrayList<Booking> items) {
    Items = items;
    this.context = context;
  }

  public static class DataObjectHolder extends RecyclerView.ViewHolder {

    private TextView name, date, status, state;

    public View mView = null;

    public DataObjectHolder(View itemView) {
      super(itemView);

      mView = itemView;
      name = (TextView) itemView.findViewById(R.id.offname);

      date = (TextView) itemView.findViewById(R.id.date);

      status = (TextView) itemView.findViewById(R.id.status);

      state = (TextView) itemView.findViewById(R.id.state);

    }

  }

  @Override
  public VerifyBookings_RecyclerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {

    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.booking_row, parent, false);

    DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
    return dataObjectHolder;

  }

  @Override
  public void onBindViewHolder(VerifyBookings_RecyclerAdapter.DataObjectHolder holder,
      int position) {

    Booking result = Items.get(position);

    holder.name.setText(result.getName());
    holder.date.setText(result.getDate());
    holder.state.setText(result.getState());
    String status_txt = result.getStatus();
    if (status_txt.startsWith("No Payment") || status_txt.startsWith("UNPAID")) {
      holder.status.setTextColor(ContextCompat.getColor(context, R.color.red));
    } else {
      holder.status.setTextColor(ContextCompat.getColor(context, R.color.Green));
    }
    holder.status.setText(status_txt);

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }
}


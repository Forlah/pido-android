package com.paycom.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.paycom.app.R;
import java.math.BigDecimal;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.Locale;
import java.util.NoSuchElementException;
import java.util.StringTokenizer;

/**
 * Created by sp_developer on 4/16/16.
 */
public class HistoryRecyclerAdapter extends
    RecyclerView.Adapter<HistoryRecyclerAdapter.DataObjectHolder> {

  private ArrayList<String> Items;
  private Context context;

  public HistoryRecyclerAdapter(Context context, ArrayList<String> items) {
    Items = items;
    this.context = context;
  }

  public static class DataObjectHolder extends RecyclerView.ViewHolder {

    private TextView mAcc, mAmnt, mDate, mTrans, mType, mDesc, mTransId;

    public View mView = null;

    public DataObjectHolder(View itemView) {
      super(itemView);
      mView = itemView;

      mAcc = (TextView) itemView.findViewById(R.id.acc);

      mAmnt = (TextView) itemView.findViewById(R.id.amount);

      mDate = (TextView) itemView.findViewById(R.id.date);

      mTrans = (TextView) itemView.findViewById(R.id.trans_type);

      mType = (TextView) itemView.findViewById(R.id.type);

      mDesc = (TextView) itemView.findViewById(R.id.history_desc);

      mTransId = (TextView) itemView.findViewById(R.id.trans_id);

    }

  }

  @Override
  public HistoryRecyclerAdapter.DataObjectHolder onCreateViewHolder(ViewGroup parent,
      int viewType) {

    View view = LayoutInflater.from(parent.getContext())
        .inflate(R.layout.history_, parent, false);

    DataObjectHolder dataObjectHolder = new DataObjectHolder(view);
    return dataObjectHolder;

  }

  @Override
  public void onBindViewHolder(HistoryRecyclerAdapter.DataObjectHolder holder, int position) {

    String resultString = Items.get(position);

    if (resultString.trim().contains("|")) {

      try {

        StringTokenizer tokens = new StringTokenizer(resultString.trim(), "|");

        String acc = tokens.nextToken().trim();
        String amnt = tokens.nextToken().trim();
        String date = tokens.nextToken().trim();
        String Trans_type = tokens.nextToken().trim();
        String type = tokens.nextToken().trim();
        String Description = tokens.nextToken().trim();
        String Trans_Id = tokens.nextToken().trim();
        holder.mAcc.setText(acc);

//                if(amnt.startsWith("-")){
//                    String new_amnt = "- "+"₦"+amnt.substring(1);
//                    holder.mAmnt.setText(new_amnt);
//                }
//
//                else if(amnt.startsWith("-") == false){
//                    holder.mAmnt.setText("₦"+amnt);
//                }

        try {
          BigDecimal bd = new BigDecimal(amnt);
          holder.mAmnt.setText(NumberFormat.getCurrencyInstance(new Locale("en", "NG")).format(bd));

        } catch (Exception e) {
          e.printStackTrace();
          holder.mAmnt.setText("₦");
        }

        holder.mDate.setText(date);

        holder.mTrans.setText(Trans_type);

        holder.mType.setText(type);

        holder.mDesc.setText(Description);

        holder.mTransId.setText(Trans_Id);

      } catch (NoSuchElementException ne) {

      }

    }

  }

  @Override
  public int getItemCount() {
    return Items.size();
  }
}

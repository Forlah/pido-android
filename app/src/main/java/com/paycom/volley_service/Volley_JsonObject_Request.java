package com.paycom.volley_service;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonObjectRequest;
import org.json.JSONObject;

/**
 * Created by sp_developer on 2/23/17.
 */
public class Volley_JsonObject_Request extends JsonObjectRequest {

  private Priority mPriority;

  public Volley_JsonObject_Request(int method, String url, JSONObject jsonRequest,
      Response.Listener<JSONObject> listener,
      Response.ErrorListener errorListener) {

    super(method, url, jsonRequest, listener, errorListener);
  }

  public void setPriority(Priority priority) {
    mPriority = priority;
  }

  @Override
  public Priority getPriority() {
    return mPriority == null ? Priority.NORMAL : mPriority;
  }

}

package com.paycom.volley_service;

import com.android.volley.Response;
import com.android.volley.toolbox.StringRequest;

/**
 * Created by sp_developer on 2/21/17.
 */
public class Volley_StringRequest extends StringRequest {

  private Priority mPriority;

  public Volley_StringRequest(int method, String url, Response.Listener<String> listener,
      Response.ErrorListener errorListener) {

    super(method, url, listener, errorListener);
  }

  public void setPriority(Priority priority) {
    mPriority = priority;
  }

  @Override
  public Priority getPriority() {
    return mPriority == null ? Priority.NORMAL : mPriority;
  }
}

package com.paycom.volley_service;

import com.android.volley.Response;
import com.android.volley.toolbox.JsonArrayRequest;
import org.json.JSONArray;

/**
 * Created by sp_developer on 2/20/17.
 */
public class Volley_JsonArray_Request extends JsonArrayRequest {

  private Priority mPriority;

  public Volley_JsonArray_Request(int method, String url, JSONArray jsonRequest,
      Response.Listener<JSONArray> listener,
      Response.ErrorListener errorListener) {

    super(method, url, jsonRequest, listener, errorListener);
  }

  public void setPriority(Priority priority) {
    mPriority = priority;
  }

  @Override
  public Priority getPriority() {
    return mPriority == null ? Priority.NORMAL : mPriority;
  }
}

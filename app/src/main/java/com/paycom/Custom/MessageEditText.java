package com.paycom.Custom;

import android.content.Context;
import android.util.AttributeSet;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputConnection;
import android.widget.EditText;

/**
 * Created by sp_developer on 2/12/17.
 */
public class MessageEditText extends EditText {

  public MessageEditText(Context context) {
    super(context);
  }

  public MessageEditText(Context context, AttributeSet attrs) {
    super(context, attrs);
  }

  public MessageEditText(Context context, AttributeSet attrs, int defStyle) {
    super(context, attrs, defStyle);
  }

  @Override
  public InputConnection onCreateInputConnection(EditorInfo outAttrs) {
    InputConnection conn = super.onCreateInputConnection(outAttrs);
    outAttrs.imeOptions &= ~EditorInfo.IME_FLAG_NO_ENTER_ACTION;
    return conn;
  }
}

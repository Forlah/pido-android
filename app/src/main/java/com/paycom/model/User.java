package com.paycom.model;

/**
 * Created by sp_developer on 11/28/15.
 */
public class User {

  private int id;
  private String fullname;
  private String mobile_no;
  private String password;
  private String imei;
  private String token;
  private String email;
  private String gender;

  // empty constructor
  public User() {

  }

  public User(String _firstname, String _surname, String _mobile_no, String _password, String _imei,
      String _token,
      String _email, String _gender) {

    fullname = _surname + " " + _firstname;
    mobile_no = _mobile_no;
    password = _password;
    imei = _imei;
    token = _token;
    email = _email;
    gender = _gender;
  }

  public User(String _fullname, String _mobile_no, String _password, String _imei, String _token,
      String _email, String _gender) {
    fullname = _fullname;
    mobile_no = _mobile_no;
    password = _password;
    imei = _imei;
    token = _token;
    email = _email;
    gender = _gender;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String _email) {
    email = _email;
  }

  public String getFullname() {
    return fullname;
  }

  public void setFullname(String _fullname) {
    fullname = _fullname;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String _gender) {
    gender = _gender;
  }

  public int getId() {
    return id;
  }

  public void setId(int _id) {
    id = _id;
  }

  public String getImei() {
    return imei;
  }

  public void setImei(String _imei) {
    imei = _imei;
  }

  public String getMobile_no() {
    return mobile_no;
  }

  public void setMobile_no(String _mobile_no) {
    mobile_no = _mobile_no;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String _password) {
    password = _password;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String _token) {
    token = _token;
  }

}

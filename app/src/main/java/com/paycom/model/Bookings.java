package com.paycom.model;

/**
 * Created by sp_developer on 11/23/16.
 */
public class Bookings {

  private String offCategory;
  private String offName;
  private String amount;
  private String name;
  private String officerMobile;
  private String address;
  private String plateNo;
  private String mobileNo;
  private String revenueCode;
  private String codeID;

  public String getOffCategory() {
    return offCategory;
  }

  public void setOffCategory(String offCategory) {
    this.offCategory = offCategory;
  }

  public String getAddress() {
    return address;
  }

  public void setAddress(String address) {
    this.address = address;
  }

  public String getAmount() {
    return amount;
  }

  public void setAmount(String amount) {
    this.amount = amount;
  }

  public String getMobileNo() {
    return mobileNo;
  }

  public void setMobileNo(String mobileNo) {
    this.mobileNo = mobileNo;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getOfficerMobile() {
    return officerMobile;
  }

  public void setOfficerMobile(String officerMobile) {
    this.officerMobile = officerMobile;
  }

  public String getOffName() {
    return offName;
  }

  public void setOffName(String offName) {
    this.offName = offName;
  }

  public String getPlateNo() {
    return plateNo;
  }

  public String getCodeID() {
    return codeID;
  }

  public void setCodeID(String codeID) {
    this.codeID = codeID;
  }

  public String getRevenueCode() {
    return revenueCode;
  }

  public void setRevenueCode(String revenueCode) {
    this.revenueCode = revenueCode;
  }

  public void setPlateNo(String plateNo) {
    this.plateNo = plateNo;
  }

}

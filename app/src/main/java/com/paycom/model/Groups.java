package com.paycom.model;

import java.util.ArrayList;

/**
 * Created by FOLASHELE on 10/25/2015.
 */
public class Groups {

  private String name;
  private ArrayList<String> Items;

  public ArrayList<String> getChildren() {
    return children;
  }

  public void setChildren(ArrayList<String> children) {
    this.children = children;
  }

  private ArrayList<String> children;

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public ArrayList<String> getItems() {
    return Items;
  }

  public void setItems(ArrayList<String> items) {
    Items = items;
  }
}

package com.paycom.model;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Data {

  private String firstname;
  private String surname;
  private String gender;
  private String mobile_no;
  private String password;
  private String imei;
  private String token;
  private String email;

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String _firstname) {
    firstname = _firstname;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String _gender) {
    gender = _gender;
  }

  public String getImei() {
    return imei;
  }

  public void setImei(String _imei) {
    imei = _imei;
  }

  public String getMobile_no() {
    return mobile_no;
  }

  public void setMobile_no(String _mobile_no) {
    mobile_no = _mobile_no;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String _password) {
    password = _password;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String _surname) {
    surname = _surname;
  }

  public String getToken() {
    return token;
  }

  public void setToken(String _token) {
    token = _token;
  }

  public void setEmail(String _email) {
    email = _email;
  }

  public String getEmail() {
    return email;
  }

  public Data() {

  }

}

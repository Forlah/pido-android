package com.paycom.model;

/**
 * Created by sp_developer on 11/28/15.
 */
public class Contact {

  // private variables
  private int id;
  private String firstname;
  private String surname;
  private String bank;
  private String phonenumber;
  private String accountnumber;

  // empty constructor to create object
  public Contact() {

  }

  //constructor
  public Contact(int _id, String _firstname, String _surname, String _bank, String _phonenumber,
      String _accountnumber) {

    id = _id;
    firstname = _firstname;
    bank = _bank;
    surname = _surname;
    phonenumber = _phonenumber;
    accountnumber = _accountnumber;
  }

  // constructor without id
  public Contact(String _firstname, String _surname, String _bank, String _phonenumber,
      String _accountnumber) {
    firstname = _firstname;
    bank = _bank;
    surname = _surname;
    phonenumber = _phonenumber;
    accountnumber = _accountnumber;
  }

  // constructor with with fullname and surname together
  public Contact(String _names, String _bank, String _phonenumber, String _accountnumber) {
    firstname = _names;
    bank = _bank;
    phonenumber = _phonenumber;
    accountnumber = _accountnumber;
  }

  public int getId() {
    return id;
  }

  public void setId(int _id) {
    id = _id;
  }

  public String getFirstname() {
    return firstname;
  }

  public void setFirstname(String _firstname) {
    firstname = _firstname;
  }

  public String getSurname() {
    return surname;
  }

  public void setSurname(String _surname) {
    surname = _surname;
  }

  public String getBank() {
    return bank;
  }

  public void setBank(String _bank) {
    bank = _bank;
  }

  public String getPhonenumber() {
    return phonenumber;
  }

  public void setPhonenumber(String _phonenumber) {
    phonenumber = _phonenumber;
  }

  public String getAccountnumber() {
    return accountnumber;
  }

  public void setAccountnumber(String _accountnumber) {
    accountnumber = _accountnumber;
  }

}

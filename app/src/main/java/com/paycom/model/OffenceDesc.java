package com.paycom.model;

/**
 * Created by sp_developer on 11/22/16.
 */
public class OffenceDesc {

  public String getCategoryId() {
    return CategoryId;
  }

  public void setCategoryId(String categoryId) {
    CategoryId = categoryId;
  }

  public String getAmount() {
    return Amount;
  }

  public void setAmount(String amount) {
    Amount = amount;
  }

  public String getDesc() {
    return Desc;
  }

  public void setDesc(String desc) {
    Desc = desc;
  }

  private String CategoryId;
  private String Amount;
  private String Desc;
}

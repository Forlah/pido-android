package com.paycom.model;

import android.content.Context;

/**
 * Created by sp_developer on 11/28/15.
 */
public class DataLab {

  // This is a singleton class to store the list of places showing on the map
  // and it also helps to retrieve and add places to the list

  private static DataLab sDataLab;

  private Context mContext;
  private Data mData;

  private DataLab(Context context) {
    mContext = context;
    mData = new Data();
  }

  public static DataLab get(Context c) {
    if (sDataLab == null) {
      sDataLab = new DataLab(c.getApplicationContext());
    }
    return sDataLab;
  }

  public Data getDetails() {
    return mData;
  }

  public void addDetails(String _firstname, String _surname, String _mobile_no, String _password,
      String _imei, String _token,
      String _email, String _gender) {

    mData.setFirstname(_firstname);
    mData.setSurname(_surname);
    mData.setMobile_no(_mobile_no);
    mData.setPassword(_password);
    mData.setImei(_imei);
    mData.setToken(_token);
    mData.setEmail(_email);
    mData.setGender(_gender);

  }

}

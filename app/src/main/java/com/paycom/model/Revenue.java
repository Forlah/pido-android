package com.paycom.model;

/**
 * Created by sp_developer on 1/3/17.
 */
public class Revenue {

  String id;
  String revenueMessage;
  String revenueCode;

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public String getRevenueCode() {
    return revenueCode;
  }

  public void setRevenueCode(String revenueCode) {
    this.revenueCode = revenueCode;
  }

  public String getRevenueMessage() {
    return revenueMessage;
  }

  public void setRevenueMessage(String revenueMessage) {
    this.revenueMessage = revenueMessage;
  }

}
